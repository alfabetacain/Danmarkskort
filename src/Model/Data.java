package Model;

import java.util.List;

/**
 * Data interface which is used for classes containing positional information,
 * more specifically classes which contain a starting and ending point, so that
 * they can be viewed as a line
 *
 */
public interface Data
{

    /**
     * Returns the coordinate from which the line starts
     *
     * @return returns the Coordinate from which the line starts
     */
    public Coordinate getFrom();

    /**
     * Returns the coordinate to which the line goes
     *
     * @return returns the coordinate to which the line goes
     */
    public Coordinate getTo();

    /**
     * Returns the coordinate of the center of the line
     *
     * @return return the coordinate of the center of the line
     */
    public Coordinate getCenter();

    /**
     * Returns the type of the class
     *
     * @return returns the type of the class
     */
    public RoadType getType();

    /**
     * Returns the given name of the line
     *
     * @return returns the given name of the line
     */
    public String getRoadName();
    
    //The time it takes to drive the length of the road by the roads speedlimit.
    public double weight();
    
    public INode getFromNode();
    
    public INode getToNode();
    public double getRotation();
    
    public double getLength();
    
    public int getZipCode();

    public Coordinate getCoordFromNumber(int fromNumber);
    public boolean hasHouseNumber(int number);   
    public boolean isNode(int houseNumber);
    public INode getAddressAsNode(int houseNumber, int possibleNodeNumber);
    public int getCoordinateAsHouseNumber(Coordinate coord);
    public void add(Data d);
    public List<Data> getEdges();
}
