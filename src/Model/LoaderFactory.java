package Model;

public abstract class LoaderFactory 
{
    public static IMapData getLoader(LoaderType type)
    {
        switch (type)
        {
            case KRAKLOADER:
                return new MapData();
            case OSMLOADER:
                return new FastOSMLoader();
            case TESTER:
                return new TesterLoader();
            default:
                return new MapData();
        }
    }
}
