package View;

import Model.Coordinate;

/**
 * Class to hold a coordinate, address name and house number of an address.
 */
public class HighlightAddress {

    public final Coordinate coord;
    public final String name;
    public final int number;

    /**
     * Constructor to be used, if a house number was found in the search input,
     * or via the coordinate upon the clicking of the map.
     * @param c Address' coordinate.
     * @param s Address' name.
     * @param number The house number.
     */
    public HighlightAddress(Coordinate c, String s, int number) {
        coord = c;
        name = s;
        this.number = number;
    }

    /**
     * Constructor to be used, if no house number was found in the search input,
     * nor via the coordinate upon the clicking of the map.
     * @param c Address' coordinate.
     * @param s Address' name.
     */
    public HighlightAddress(Coordinate c, String s) {
        coord = c;
        name = s;
        number = -1;
    }

    public boolean isVisible(Coordinate viewCoordinate, double viewLength) {
        return (viewCoordinate.x < coord.x
                && viewCoordinate.x + viewLength > coord.x
                && viewCoordinate.y < coord.y
                && viewCoordinate.y + viewLength > coord.y);
    }

    @Override
    public String toString() {
        return (number > 0) ? number + " " + name : name;
    }
}