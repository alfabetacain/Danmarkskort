package View;

import Controller.ArrowKeyDirection;
import Model.Coordinate;
import Model.Data;
import Model.GeometricCalculator;
import Model.IModel;
import Model.Road;
import Model.RoadType;
import Model.Square;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;

/**
 * Class used to draw a map using edges.
 */
public class MapView extends JPanel {

    //private int moveFactor;
    private final IModel model;
    //the coordinate of the start of the view.
    private Coordinate viewCoordinate;
    //the length of what the user is currently viewing.
    private double viewLength;
    //how much the user is actually seeing of the map. Can be seen as a %
    private double zoomLvl;
    //The original square; case 1.0. for the zoomLvl to be relative to:
    private final double coveredSquare;
    //Amount of pixels for the mapview
    private int dimension = 600;
    private Square zoomBox;
    //the pct to zoom by.
    private final double zoomFactor, maxZoom, minZoom, minX, maxX, minY, maxY;
    //The start coordinate for the zoom animation.
    private Coordinate zoomAniCoordinate;
    //The saved start length of the Zoom animation.
    private double zoomAniLength;
    //The desired end coordinate for the zoom animation.
    private Coordinate zoomToCoord;
    //The desired end length for the zoom animation.
    private double zoomToLength;
    private boolean isAnimating, CSIZoom;
    private Square paintedSquare;
    private BufferedImage buffImg;
    private Coordinate buffImgCoordinate;
    private Set<RoadType> roadTypes;
    //Very light-gray color for the background
    private final Color backgroundColor = new Color(250, 250, 250);
    private final Color transparentBlue = new Color(0, 0, 255, 10);
    private HighlightAddress fromAddress;
    private HighlightAddress toAddress;
    //A slightly transparent bright red!!!
    private Color highlightColor = new Color(255, 0, 0, 200);
    //The calculated route between 2 points.
    private List<Data> route;
    //a bright yellow
    private final Color routeColor = new Color(254, 255, 63);
    private final Color highligtedTurn = new Color(50, 171, 255);
    private final Color highlightedTurnText = new Color(52, 116, 161);
    //the turn in the route that is highlighted. 
    private Data highlightRoad,beforeHighlight;
    //if the highlighted turn is the start point.
    private boolean highlightIsStart;
    private final File greenFlagFile = new File("data/greenflag-32.png");
    private final File redFlagFile = new File("data/redflag-32.png");
    //the different strokes used in drawing the map.
    private final BasicStroke routeStroke, routeStrokeSmall, ferryStroke;
    //the typical amount of frames used for a animation.
    private final int zoomAnimationFrames = 20;
    private final double startX,startY;

    /**
     * the Mapview is giving a model to get data from. It then draws the 
     * initial view by getting the coordinates for the coveredSquare.
     * The length of coveredSquare is set to 420000 as hack for a bit nicer
     * look of the view, that doesn't include bornholm in the beginning.
     */
    public MapView(IModel model) {
        super();
        setBackground(backgroundColor);
        this.model = model;
        double coveredSquareLength = model.getCoveredSquare().getLength();
        //not equal to coveredSquareLength to not show bornholm to start with.
        coveredSquare = 420000;
        this.viewLength = coveredSquare;
        startX = model.getCoveredSquare().getX();
        startY =  model.getCoveredSquare().getY();
        this.viewCoordinate = new Coordinate(startX,startY);
        buffImgCoordinate = viewCoordinate;
        zoomFactor = 0.10;
        maxZoom = 0.0015;
        minZoom = 1.5;
        zoomLvl = 1.0;
        //Set the limits for where the user can move around on the map view.
        minX = viewCoordinate.x - coveredSquareLength * 0.10;
        minY = viewCoordinate.y - coveredSquareLength * 0.10;
        maxX = viewCoordinate.x + coveredSquareLength * 1.10;
        maxY = viewCoordinate.y + coveredSquareLength * 1.10;
        
        routeStroke = new BasicStroke(20.0f, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND);
        routeStrokeSmall = new BasicStroke(5.0f, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_ROUND);
        ferryStroke = new BasicStroke(1.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 1.0f, new float[]{10.0f, 10.0f}, 0);

    }

    /**
     * Zooms in by making the reducing  the square made by the viewCoordinate 
     * and viewLength with the zoomfactor, relative to the zoomLvl. Then it 
     * paints a new BufferedImage. 
     * Will not zoom in if maximum zoom level has been reached.
     * 
     * A repaint() method call is required for the changes of this method to take
     * visual effect.
     */
    public void zoomIn() {
        if (zoomLvl < maxZoom) {
            return;
        }

        viewCoordinate = new Coordinate(viewCoordinate.x 
                + zoomFactor * zoomLvl * coveredSquare, 
                viewCoordinate.y + zoomFactor * zoomLvl * coveredSquare);
        viewLength -= 2 * zoomFactor * viewLength;
        
        buffImg = null;
    }

    /**
     * Zooms out by making the adding the viewCoordinate and viewLength with
     * the zoomfactor, relative to the zoomLvl. Then it paints a new 
     * BufferedImage. Will not zoom out if minimum zoom level has been reached.
     * 
     * If the zoom out would bring the mapView to move out of the 
     * minimum or maximum coordinates (to far away from the dataset, e.g. 
     * Denmark), then the coordinate to zoom out to will be adjusted so that
     * it is within these limits.
     * 
     * A repaint() method call is required for the changes of this method to take
     * visual effect.
     */
    public void zoomOut() {
        if ((viewLength + 2 * (zoomFactor * viewLength)) / coveredSquare
                > minZoom) {
            return;
        }

        viewLength += 2 * (zoomFactor * viewLength);

        double newX = viewCoordinate.x - (zoomFactor * zoomLvl * coveredSquare);
        double newY = viewCoordinate.y - (zoomFactor * zoomLvl * coveredSquare);

        //Consider how the view should be placed, if we are close to the borders
        if (viewCoordinate.x < minX) {
            newX = minX;
        } else if (viewCoordinate.x + viewLength > maxX) {
            newX = maxX - viewLength;
        }
        if (viewCoordinate.y < minY) {
            newY = minY;
        } else if (viewCoordinate.y + viewLength > maxY) {
            newY = maxY - viewLength;
        }
        
        viewCoordinate = new Coordinate(newX, newY);
        buffImg = null;
    }

    /**
     * Moves the MapView's view onto the designated x and y coordinates, with the
     * designated sideLength.
     * @param x x-value of the new view.
     * @param y y-value of the new view.
     * @param sideLength side length of the new view.
     */
    public void zoomToSquare(double x, double y, double sideLength) {
        y += sideLength;
        sideLength = (viewCoordinate.x + ((x + sideLength) / getDif())) 
                - (viewCoordinate.x + (x / getDif()));

        x = viewCoordinate.x + (x / getDif());
        y = viewLength + viewCoordinate.y - (y / getDif());
        removeZoomSquare();

        if (sideLength / coveredSquare < maxZoom) {
            System.out.println("Square to small");
            return;
        }

        //Values needed for zoom animation
        zoomAniCoordinate = viewCoordinate;
        zoomAniLength = viewLength;
        //New actual values, resulting from the zoom animation
        viewCoordinate = new Coordinate(x, y);
        viewLength = round(sideLength);
        
        zoomAnimation();
    }


    private double getDif() {
        return dimension / viewLength;
    }
    
    public boolean toggleCSIZoom() {
        CSIZoom = !CSIZoom;
        return CSIZoom;
    }

    private void removeZoomSquare() {
        zoomBox = null;
    }
    
    /**
     * Makes a ZoomAnimation. Before callign this method, first assign the 
     * current viewCoordinate and viewLength to zoomAniCoordinate and 
     * zoomAniLength respectively. Then change the viewLength and 
     * viewCoordinate to target length and coordinate and instead of calling
     * repaint, call zoomAnimation to bring you there, with an animation.
     * 
     * The animation is generated but taking the difference between the current
     * and target coordinate and length, and then changing the current by
     * the difference / zoomframes and repainting 'zoomframes' amount of times.
     * If the current and target length is the same, the animation is only 
     * in movement, without zoom, therefore the BuffImg isn't reset, so to use 
     * it. If the distance between the coordinates is small, a relatively
     * smaller amount of zoomframes is used in the animation.
     */
    private void zoomAnimation() {
        if (zoomAniCoordinate == null || zoomAniLength == 0)
            return;
        isAnimating = true;
        zoomToCoord = viewCoordinate;
        zoomToLength = viewLength;
        viewCoordinate = zoomAniCoordinate;
        double i = zoomAnimationFrames;
        double dist = GeometricCalculator.calculateDistanceBetweenCoordinates(viewCoordinate, zoomToCoord);
        if(dist < 1000) {
            i = (dist/1000)*i;
        }

        double factor = (zoomToLength - zoomAniLength) / i;
        double xfactor = (zoomToCoord.x - viewCoordinate.x) / i;
        double yfactor = (zoomToCoord.y - viewCoordinate.y) / i;

        while (i > 0) {
            zoomAniLength = zoomAniLength + factor;
            viewLength = zoomAniLength;

            viewCoordinate = new Coordinate(viewCoordinate.x + xfactor, viewCoordinate.y + yfactor);

            if (factor != 0) {
                buffImg = null;
            }
            
            /* This is a hack, it would be nicer to call it in the thread wtih
             * repaint(). */
            paintImmediately(new Rectangle(this.getSize()));
            i--;
        }
        zoomAniCoordinate = null;
        zoomAniLength = 0;
        
        viewLength = zoomToLength;
        isAnimating = false;
    }

    public void updateSquare(Square square) {
        this.zoomBox = square;
        if (paintedSquare == null) {
            paintedSquare = square;
        }
    }

    /**
     * Updates the dimension of the map.
     */
    public void updateDimension() {
        
        dimension = getWidth() < getHeight() ? getWidth() : getHeight();
        buffImg = null;
    }

    /**
     * Used for changing the shown map in mapview, by continuosly calling this
     * method when the mouse is moved. The map is then moved with a vector
     * equal to the new coor - the last.
     * 
     * If the drag would bring the mapView to move out of the 
     * minimum or maximum coordinates (too far away from the dataset, e.g. 
     * Denmark), then the coordinate to drag to will be adjusted so that
     * it is within these limits.
     */
    public void mouseDrag(Coordinate coor) {
        double x = round(viewCoordinate.x - (coor.x / getDif()));
        double y = round(viewCoordinate.y + (coor.y / getDif()));

        if (x < minX) {
            x = minX;
        } else if (x + viewLength > maxX) {
            x = maxX - viewLength;
        }
        
        if (y < minY) {
            y = minY;
        } else if (y + viewLength > maxY) {
            y = maxY - viewLength;
        }
        
        if (x >= minX && y >= minY && maxX > x + viewLength && maxY >= y + viewLength) {
            viewCoordinate = new Coordinate(x, y);
        }
    }

    /**
     * Resets the mapview to show the entire map.
     */
    public void resetView() {
        //needed for the zoom animation
        zoomAniCoordinate = viewCoordinate;
        zoomAniLength = viewLength;

        viewLength = coveredSquare;
        viewCoordinate = new Coordinate(startX, startY);
        zoomAnimation();
        buffImg = null;
    }

    /**
     * Method given a direction, moves the current view in designated direction,
     * assuming that such action does not make the view exceed the limits for
     * the affected dimension.
     * @param direction The direction for the view to be moved towards.
     */
    public void moveMap(ArrowKeyDirection direction) {
        switch (direction) {
            case UP:
                if (viewCoordinate.y + viewLength < maxY) {
                    viewCoordinate = new Coordinate(viewCoordinate.x, viewCoordinate.y + (zoomFactor * zoomLvl * coveredSquare));
                }
                break;
            case DOWN:
                if (viewCoordinate.y > minY) {
                    viewCoordinate = new Coordinate(viewCoordinate.x, viewCoordinate.y - (zoomFactor * zoomLvl * coveredSquare));
                }
                break;
            case LEFT:
                if (viewCoordinate.x > minX) {
                    viewCoordinate = new Coordinate(viewCoordinate.x - (zoomFactor * zoomLvl * coveredSquare), viewCoordinate.y);
                }
                break;
            case RIGHT:
                if (viewCoordinate.x + viewLength < maxX) {
                    viewCoordinate = new Coordinate(viewCoordinate.x + (zoomFactor * zoomLvl * coveredSquare), viewCoordinate.y);
                }
                break;
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        zoomLvl = viewLength / coveredSquare;
        
        if (zoomBox != null) {
            g.drawImage(buffImg, 0, 0, this);
            g.setColor(Color.BLUE);
            g.drawRect(round(zoomBox.getX()), round(zoomBox.getY()), round(zoomBox.getLength()), round(zoomBox.getLength()));
            g.setColor(transparentBlue);
            g.fillRect(round(zoomBox.getX()), round(zoomBox.getY()), round(zoomBox.getLength()), round(zoomBox.getLength()));

            return;
        }

        //let buffimg be null, when it is not used
        if (buffImg == null) {
            buffImgCoordinate = viewCoordinate;
            //switched from dimension, dimension
            paintBuffImg();
        } else {
           /* Updates the buffered image, essentially only if the viewCoordinate
            * was priorly changed, otherwise updates buffImg to be the same. */
            addToBuffImg();
        }
        g.drawImage(buffImg, 0, 0, this);

        
        if (highlightRoad != null) {
            paintHighligtTurn(g);
        }

        /* draw the triangle pointing at the highlighted Address and write the
         * addressName, only if is is within the view. */
        if (fromAddress != null && fromAddress.isVisible(viewCoordinate,viewLength)) {
            paintAddress(fromAddress, g, false);
        }
        if (toAddress != null && toAddress.isVisible(viewCoordinate,viewLength)) {
            paintAddress(toAddress, g, true);
        }
    }

    
    /**
     * Highlights a turn or point in the route. 
     * Paints the corner of the highlightRoad and the beforeHighlight,
     * so to show the turn leading to the selected road
     * If the highlightRoad is the first or last point in the array, a round
     * red start/end-dot is painted on the start or end point.
     * 
     * @param g = the graphics to paint on.
     */
    private void paintHighligtTurn(Graphics g) {

        Graphics2D g2 = (Graphics2D) g;

        Data edge = highlightRoad;
        g2.setStroke(new BasicStroke(5));

        Point from = coordinateToPoint(edge.getFrom());
        Point to = coordinateToPoint(edge.getTo());

        if (beforeHighlight == null && !highlightIsStart) { //Paint end
            g2.setColor(Color.RED);
            g2.fill(new Ellipse2D.Double(to.x - 7, to.y - 7, 14, 14));
        } else if (beforeHighlight == null && highlightIsStart) { //paint start
            g2.setColor(Color.RED);
            g2.fill(new Ellipse2D.Double(from.x - 7, from.y - 7, 14, 14));
        } else { // paint turn.

            g2.setColor(highligtedTurn);
            int length = 100;
            Coordinate previousCoord = getShorterEdge(length, beforeHighlight.getLength(), beforeHighlight.getTo(), beforeHighlight.getFrom());
            Coordinate nextCoord = getShorterEdge(length, edge.getLength(), edge.getFrom(), edge.getTo());

            drawEdge(edge.getFrom(), nextCoord, g2);
            drawEdge(previousCoord, edge.getFrom(), g2);

            g2.setColor(highlightedTurnText);
            g2.drawString(beforeHighlight.getRoadName(), coordinateToPoint(previousCoord).x, coordinateToPoint(previousCoord).y);
            g2.drawString(edge.getRoadName(), coordinateToPoint(nextCoord).x, coordinateToPoint(nextCoord).y);
        }
        g2.setColor(routeColor);
    }
    
    /**
     * Paints the road of a rectangle, defined by a coordinate and a width and
     * height on the BuffImg. Can be used both to paint an entire new buffImg
     * or a part of a new one.
     * 
     * @param image The BufferedImage to paint the data on.
     * @param coord The realworld coordinate of the rectangles 0,0
     * @param width of the rectangle
     * @param height of the rectangle
     */
    private void paintRoadsOnGraphics(BufferedImage image, Coordinate coord, double width, double height) {

        Graphics2D g2 = (Graphics2D) image.getGraphics();

        g2.setBackground(backgroundColor);
        roadTypes = new HashSet<>();
        //standard road types to be drawn
        roadTypes.add(RoadType.COAST);
        roadTypes.add(RoadType.POLITICALBORDER);
        roadTypes.add(RoadType.RIVER);
        roadTypes.add(RoadType.HIGHWAY);
        roadTypes.add(RoadType.EXPRESSWAY);
        roadTypes.add(RoadType.MAINROAD);
        roadTypes.add(RoadType.FERRYROUTE);

        if (zoomLvl < 0.3) {
            roadTypes.add(RoadType.REGULARROAD);
        }
        if (zoomLvl < 0.04) {
            roadTypes.add(RoadType.SUBROAD);
        }
        if (zoomLvl < 0.01) {
            roadTypes.add(RoadType.PATH);
        }
        if (route != null) {
            Iterator it = route.iterator();
            g2.setColor(routeColor);
            if (zoomLvl < 0.02) {
                g2.setStroke(routeStroke);
            } else {
                g2.setStroke(routeStrokeSmall);
            }

            while (it.hasNext()) {
                Data edge = (Data) it.next();
                drawEdge(edge, g2);
            }
        }


        List<Data> data = model.getAreaData(coord, width, height, roadTypes);

        for (Data E : data) {
            if (zoomLvl < 0.02 && (E.getType() == RoadType.HIGHWAY
                    || E.getType() == RoadType.EXPRESSWAY
                    || E.getType() == RoadType.MAINROAD)) {
                g2.setStroke(new BasicStroke(2));
            } else {
                g2.setStroke(new BasicStroke(1));
            }

            switch (E.getType()) {
                case HIGHWAY:
                    g2.setColor(new Color(200, 0, 0));
                    break;
                case EXPRESSWAY:
                    g2.setColor(new Color(200, 50, 50));
                    break;
                case MAINROAD:
                    g2.setColor(new Color(200, 100, 100));
                    break;
                case REGULARROAD:
                    g2.setColor(Color.DARK_GRAY);
                    break;
                case SUBROAD:
                    g2.setColor(Color.gray);
                    break;
                case PATH:
                    g2.setColor(Color.green);
                    break;
                case COAST:
                    g2.setColor(Color.black);
                    break;
                case FERRYROUTE:
                    g2.setColor(Color.BLUE);
                    g2.setStroke(ferryStroke);
                    break;
                default:
                    g2.setColor(Color.black);
            }
            Point from = coordinateToPoint(E.getFrom());
            Point to = coordinateToPoint(E.getTo());
            g2.draw(new Line2D.Double(from.x,
                    from.y,
                    to.x,
                    to.y));
        }
    }

    /**
     * Paints a new BufferedImage.
     */
    private void paintBuffImg() {
        buffImg = new BufferedImage(dimension, dimension, BufferedImage.TYPE_INT_ARGB);

        paintRoadsOnGraphics(buffImg, viewCoordinate, round(viewLength), round(viewLength));
    }

    /**
     * the method used to add content the buffImg, instead of painting a new 
     * one. Functions by making a new buffered image, then it finds the relative
     * placement of the old buffimg on the new one. That leaves some empty 
     * space on the new buffered image, which is then filled by calling 
     * paintRoadsOnGraphics on those areas.
     */
    private void addToBuffImg() {

        double xdiff = viewCoordinate.x - buffImgCoordinate.x;
        double ydiff = viewCoordinate.y - buffImgCoordinate.y;

        BufferedImage newBuffImg = new BufferedImage(dimension, dimension, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = (Graphics2D) newBuffImg.getGraphics();

        g.setBackground(backgroundColor);
        g.clearRect(0, 0, dimension, dimension);


        Coordinate drawAreaCoordinate;

        if (xdiff > 0) {
            drawAreaCoordinate = new Coordinate(buffImgCoordinate.x + viewLength, viewCoordinate.y);
            paintRoadsOnGraphics(newBuffImg, drawAreaCoordinate, xdiff, round(viewLength));
        } else if (xdiff < 0) {
            paintRoadsOnGraphics(newBuffImg, viewCoordinate, Math.abs(xdiff), round(viewLength));

        }
        // -- hvis du tager denne del ud nedenunder så virker den del oven over men ikke ellers
        if (ydiff < 0) {
            paintRoadsOnGraphics(newBuffImg, viewCoordinate, round(viewLength), Math.abs(ydiff));
        } else if (ydiff > 0) {
            drawAreaCoordinate = new Coordinate(viewCoordinate.x, buffImgCoordinate.y + viewLength);
            paintRoadsOnGraphics(newBuffImg, drawAreaCoordinate, round(viewLength), ydiff);
        }

        g.drawImage(buffImg, round(-xdiff * dimension / viewLength), round(ydiff * dimension / viewLength), this);


        buffImgCoordinate = viewCoordinate;

        buffImg = newBuffImg;
    }

    private int round(double number) {
        return (int) Math.round(number);
    }

    /**
     * gets a realworld coordinate from a point on the map.
     * 
     * @param point = the point the map
     * @return the corresponding realworld coordinate.
     */
    public Coordinate pointToCoordinate(Point point) {
        double x = viewCoordinate.x + (point.x / getDif());
        double y = viewLength + viewCoordinate.y - (point.y / getDif());
        return new Coordinate(x, y);
    }

    /**
     * gets a point on the map from a realworld coordinate.
     * 
     * @param coord = the realworld coordinate
     * @return the corresponding point the map.
     */
    private Point coordinateToPoint(Coordinate coord) {
        double dif = getDif();
        double x = (coord.x - viewCoordinate.x) * getDif();
        double y = ((viewLength) - coord.y + viewCoordinate.y) * dif;

        return new Point(round(x), round(y));
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(dimension, dimension);
    }

    public void centerOnHighlightedAddress() {
        if (toAddress == null && fromAddress == null) {
            return;
        }
        if (toAddress == null) {
            centerOnHighlightedAddress(fromAddress);
            return;
        }
        if (fromAddress == null) {
            centerOnHighlightedAddress(toAddress);
            return;
        }

        showRoute();
    }

    private void centerOnHighlightedAddress(HighlightAddress address) {

        if (address == null) {
            return;
        }

        if (CSIZoom) {
            resetView();
        }
        
        zoomAniCoordinate = viewCoordinate;
        zoomAniLength = viewLength;

        Coordinate highlight = address.coord;
        viewLength = round(coveredSquare * 0.02);
        double half = viewLength / 2;
        viewCoordinate = new Coordinate(highlight.x - half, highlight.y - half);

        zoomAnimation();
        buffImg = null;
    }

    private void centerOnPoint(Coordinate coordinate) {

        zoomAniCoordinate = viewCoordinate;
        zoomAniLength = viewLength;
        viewLength = 1000;
        viewCoordinate = new Coordinate(round(coordinate.x - viewLength / 2), round(coordinate.y - viewLength / 2));
        zoomAnimation();
    }

    private void paintAddress(HighlightAddress address, Graphics g, boolean isEndAddress) {
        g.setColor(highlightColor);
        double dif = getDif();
        
        double xdif = getDif();
        int xpoint = round(address.coord.x - viewCoordinate.x);
        int ypoint = round((viewLength) - address.coord.y + viewCoordinate.y);

        if(isEndAddress)
            g.drawImage(getFlagImage(isEndAddress), round(xpoint * xdif), round(ypoint * dif)-32, null);
        else
            g.drawImage(getFlagImage(isEndAddress), round(xpoint * xdif), round(ypoint * dif)-32, null);
        
        //Place name of road slightly below flag
        FontMetrics fm = g.getFontMetrics();
        Rectangle2D rect = fm.getStringBounds(address.toString(), g);

        g.setColor(new Color(0, 0, 0, 140)); //bg box color
        g.fillRect(round(xpoint * dif)+2,
                   round(ypoint * dif)+11 - fm.getAscent(),
                   (int) rect.getWidth(),
                   (int) rect.getHeight());

        g.setColor(new Color(250, 250, 250)); //Text color
        g.drawString(address.toString(), round(xpoint * dif)+2, round(ypoint * dif)+10);
        //g.drawChars(address.toString().toCharArray(), 0, address.toString().length(), round(xpoint * dif)+2, round(ypoint * dif)+10);
    }
    
    private BufferedImage getFlagImage(boolean isEndAddress)
    {
        try {
            if(isEndAddress)
                return ImageIO.read(redFlagFile);
            else
                return ImageIO.read(greenFlagFile);
        } catch (IOException ex) {
            //Do nothing
        }
        return null;
    }

    private void showRoute() {
        if (toAddress == null || fromAddress == null) {
            return;
        }

        if (CSIZoom || zoomLvl < 0.2) {
            resetView();
        }

        Square toAndFrom = new Square(coordinateToPoint(fromAddress.coord), coordinateToPoint(toAddress.coord));

        double l = toAndFrom.getLength();

        zoomToSquare(toAndFrom.getX() - l * 0.25, toAndFrom.getY() - l * 0.25, l * 1.5);

    }

    //Used to highlight a road or turn in the shown route.
    //get is the next road after the turn.
    public void highlightTurn(Data next, Data before) {

        if (next instanceof Road) {
            highlightRoad = next.getEdges().get(0);
        } else {
            highlightRoad = next;
        }

        if (before instanceof Road) {
            beforeHighlight = before.getEdges().get(before.getEdges().size() - 1);
        } else {
            beforeHighlight = before;
        }
        centerOnPoint(next.getFrom());
    }

    /**
     * Used to highlight and center on an address.
     * 
     * @param endOrStart the edge to be highlighted.
     * @param start = if the data is the start of the route.
     */
    public void highlightTurn(Data endOrStart, boolean start) {
        //Hvis ankomst og Road klasse = fejl
        beforeHighlight = null;
        highlightIsStart = start;
        if (start) {
            if (endOrStart instanceof Road) {
                highlightRoad = endOrStart.getEdges().get(0);
            } else {
                highlightRoad = endOrStart;
            }

            centerOnPoint(highlightRoad.getFrom());
        } else {

            if (endOrStart instanceof Road) {
                highlightRoad = endOrStart.getEdges().get(endOrStart.getEdges().size() - 1);
            } else {
                highlightRoad = endOrStart;
            }

            centerOnPoint(highlightRoad.getTo());

        }


    }

    /**
     * a help method to draw an edge on a graphics object. 
     * 
     * @param edge the edge to be drawn
     * @param g2 the graphics2D object to draw on
     */
    private void drawEdge(Data edge, Graphics2D g2) {
       /* Recursively go through sub-edges of a road, if it is a road that we
        * cycle through. If not a road, draw the sub-edge */
        if (edge instanceof Road) {
            for (Data e : edge.getEdges()) {
                drawEdge(e, g2);
            }
            return;
        }
        Point from = coordinateToPoint(edge.getFrom());
        Point to = coordinateToPoint(edge.getTo());
        g2.draw(new Line2D.Double(from.x,
                from.y,
                to.x,
                to.y));
    }

    /**
     * Draws a line between two coordinates. especially useful for
     * @param fromCoord
     * @param toCoord
     * @param g2
     */
    private void drawEdge(Coordinate fromCoord, Coordinate toCoord, Graphics2D g2) {

        Point from = coordinateToPoint(fromCoord);
        Point to = coordinateToPoint(toCoord);
        g2.draw(new Line2D.Double(from.x,
                from.y,
                to.x,
                to.y));
    }

    private Coordinate getShorterEdge(int length, double oldLength, Coordinate from, Coordinate to) {
        if (length < oldLength) {
            double factor = length / oldLength;
            return new Coordinate(from.x + (to.x - from.x) * factor, from.y + (to.y - from.y) * factor);
        } else {
            return to;
        }
    }

    public void setToAddress(Coordinate c, String s, int i) {
        toAddress = new HighlightAddress(c, s, i);
    }

    public void clearAddresses() {
        toAddress = null;
        fromAddress = null;
        route = null;
        highlightRoad = null;
        buffImg = null;
        resetView();
    }

    public void setFromAddress(Coordinate c, String s, int i) {
        fromAddress = new HighlightAddress(c, s, i);
    }

    public boolean hasTo() {
        return toAddress != null;
    }

    public boolean hasFrom() {
        return fromAddress != null;
    }

    public String getFromName() {
        return fromAddress.name;
    }
    
    public Coordinate getFromCoordinate()
    {
        return fromAddress.coord;
    }
    
    public Coordinate getToCoordinate() {
        return toAddress.coord;
    }

    public String getToName() {
        return toAddress.name;
    }
    
    public int getFromHouseNumber() {
        return fromAddress.number;
    }
    
    public int getToHouseNumber() {
        return toAddress.number;
    }

    /**
     * Changes the route to be drawn in the mapview.
     * 
     * @param newRoute the new route ;)
     */
    public void changeRoute(List<Data> newRoute) {
        route = newRoute;
        highlightRoad = null;
        buffImg = null;
    }
}
