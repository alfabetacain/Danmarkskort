package Model;

import View.HighlightAddress;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * IModel defines methods for extracting positional data
 *
 */
public interface IModel
{
    /**
     * Returns the data which lies within the given area of a given type. The lower left point
     * of the defined square is assumed to be the given coordinate
     *
     * @param startingPoint the starting point of the wanted square
     * @param length the length of the square
     * @param roadType the type of road to get
     * @return all Data which are inside the square
     */
    public List<Data> getAreaData(Coordinate startingPoint, double width,double height, Set<RoadType> roadType);

    /**
     * Returns the name of the line segment closest to the given coordinate
     *
     * @param point the point from which to find a line segment
     * @return the name of the line segment
     */
    public String getRoadName(Coordinate point);

    /**
     * Returns the square which the model covers
     *
     * @return the square which the model covers
     */
    public Square getCoveredSquare();

    public Coordinate getAddressCoordinate(String s);
    
    public List<Data> getPathBetweenPoints(String start, String end, boolean isWalking, int fromNumber, int toNumber);
    public List<Data> getPathBetweenPoints(Coordinate from, Coordinate to, boolean isWalking);
    
    
    public Data getClosestRoad(Coordinate coordinates);

    public Data getEdgeFromAddress(String s);
    public Set<String> getAllRoadNames();
    public Collection<String> getAllCityNames();
    
    public String getCityFromZip(int zip);

    public HighlightAddress getClosestAddress(Coordinate realWorldCoordinate, boolean isFrom);
    
    public String getRoadAndCityName(Data d);
}
