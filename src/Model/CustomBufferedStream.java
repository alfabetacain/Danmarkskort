/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomBufferedStream extends BufferedInputStream {

    public CustomBufferedStream(InputStream in, int size) {
        super(in, size);
    }
    
    @Override
    public int available() throws IOException {
        if (in == null)
            throw new IOException( "Stream closed" );
        final int n = count - pos;
        return n > 0 ? n : in.available();
    }
    
}
