
package Model;

import View.LoadBar;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/*
 * The OSM loader that used the serialized data created before running the 
 * program by misc/OSMSerializer.java
 * 
 */
public class FastOSMLoader implements IMapData {

    private final LoadBar loadBar;
    private Map<RoadType, List<Data>> map;
    private final int stop;
    private int loadProgress;

    public FastOSMLoader() {
        loadBar = LoadBar.getInstanceOf();
        loadProgress = 0;
        stop = 60;
        map = null;
        try {
            Timer timer = new Timer();
            timer.schedule(new UpdateLoader(), 0, 1000);
            System.out.println("Starting load");
            FileInputStream fileIn = new FileInputStream("data/serialEdges.ser");
            ObjectInputStream in = new ObjectInputStream(new CustomBufferedStream(fileIn, 128 * 1024));
            map = (EnumMap<RoadType, List<Data>>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to read serialized data");
        }
    }

    @Override
    public Map<RoadType, List<Data>> getData() {
        return map;
    }

    private class UpdateLoader extends TimerTask {

        @Override
        public void run() {
            loadProgress += 2;
            loadBar.setValue(loadProgress *100/ stop);
        }
    }
}
