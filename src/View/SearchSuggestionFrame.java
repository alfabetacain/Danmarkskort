package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

/**
 * A class acting as a JFrame, to be opened when the spell checker of the program
 * returns a number of suggestions higher than just one.
 */
public class SearchSuggestionFrame extends JFrame {
    private Font textFont = new Font("Meiryo", 0, 12);
    private Font headerFont = new Font("Meiryo UI", 1, 13);
    private Color lightGray = new Color(245, 245, 245);
    private Dimension buttonDimension = new Dimension(180, 20);
    private Dimension preferredDimensionSingle = new Dimension(400, 200);
    private Dimension preferredDimensionDouble = new Dimension(400, 400);
    private JList listForSearchSugg;
    private JList listForNavFrom;
    private JList listForNavTo;
    private SuggestionFrameType frameType;
    
    /**
     * Called in different circumstances, depending on which text fields
     * require a text input of which suggestions exist for
     * @param frameLabelText the headline text, defining which field the
     * suggestions are for
     * @param suggestions the suggested addresses, as alternatives to user input
     */
    public SearchSuggestionFrame(String frameLabelText, String[] suggestions,
            ActionListener confirmButtonListener, WindowAdapter windowAdapter, SuggestionFrameType frameType)
    {
        this.frameType = frameType;
        
        //Frame functionality setup
        setPreferredSize(preferredDimensionSingle);
        
        //Add window focus listener, to keep this frame focused
        addWindowFocusListener(windowAdapter);
        
        //Adding panel-divided contents
        add(createFrameLabel(frameLabelText), BorderLayout.NORTH);
        add(createPanelWithSuggestions(suggestions, 0), BorderLayout.CENTER);
        add(createPanelWithConfirmButton(confirmButtonListener), BorderLayout.SOUTH);
        
        //Final set up of the frame
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    /**
     * Called when both the navigate from and to text fields have suggestions
     * for altering the user input, to provide a valid navigation search
     * @param suggestionsForNavTo suggestions for the navigate to text field
     * @param suggestionsForNavFrom suggestions for the navigate from text field
     */
    public SearchSuggestionFrame(String[] suggestionsForNavFrom, String[] suggestionsForNavTo,
            ActionListener confirmButtonListener, WindowAdapter windowAdapter, SuggestionFrameType frameType)
    {
        this.frameType = frameType;
        
        //Frame functionality setup
        setPreferredSize(preferredDimensionDouble);
        
        //Add window focus listener, to keep this frame focused
        addWindowFocusListener(windowAdapter);
        
        //Adding panel-divided contents
        JPanel suggestionPanel = new JPanel();
        suggestionPanel.setLayout(new GridLayout(2, 1));
        JPanel firstPanel = new JPanel();
        firstPanel.add(createFrameLabel("Navigér fra - mente du:"));
        firstPanel.add(createPanelWithSuggestions(suggestionsForNavFrom, 1));
        suggestionPanel.add(firstPanel);
        JPanel secondPanel = new JPanel();
        secondPanel.add(createFrameLabel("Navigér til - mente du:"));
        secondPanel.add(createPanelWithSuggestions(suggestionsForNavTo, 2));
        suggestionPanel.add(secondPanel);
        add(suggestionPanel, BorderLayout.CENTER);
        add(createPanelWithConfirmButton(confirmButtonListener), BorderLayout.SOUTH);
        
        //Final set up of the frame
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    private JPanel createPanelWithSuggestions(String[] suggestionsA, int whichSuggestionList)
    {
        JPanel panel = new JPanel();
        JList list = createList(suggestionsA);
        list.setVisibleRowCount(5);
        list.setFixedCellWidth(340);
        JScrollPane scrollPane;
        if(getFrameType() == SuggestionFrameType.SEARCH) {
            listForSearchSugg = list;
            scrollPane = new JScrollPane(listForSearchSugg);
        } else if(getFrameType() == SuggestionFrameType.NAVIGATEFROM) {
            listForNavFrom = list;
            scrollPane = new JScrollPane(listForNavFrom);
        } else if(getFrameType() == SuggestionFrameType.NAVIGATETO) {
            listForNavTo = list;
            scrollPane = new JScrollPane(listForNavTo);
        } else {
            if(whichSuggestionList == 1) {
                listForNavFrom = list;
                scrollPane = new JScrollPane(listForNavFrom);
            } else { //if(whichSuggestionList == 2)
                listForNavTo = list;
                scrollPane = new JScrollPane(listForNavTo);
            }
        }
        
        scrollPane.setPreferredSize(new Dimension(360, 109));
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        panel.add(scrollPane);
        return panel;
    }
    
    private JPanel createPanelWithConfirmButton(ActionListener confirmButtonListener)
    {
        JPanel panel = new JPanel();
        JButton button = createButton("Bekræft", confirmButtonListener);
        panel.add(button);
        return panel;
    }
    
    private JLabel createFrameLabel(String frameLabelText)
    {
        JLabel frameLabel = new JLabel(frameLabelText);
        frameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        frameLabel.setFont(headerFont);
        return frameLabel;
    }
    
    private JList createList(String[] suggestions)
    {
        JList listVar = new JList(suggestions);
        listVar.setBorder(BorderFactory.createLineBorder(lightGray, 1));
        listVar.setFont(textFont);
        listVar.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        return listVar;
    }
    
    private JButton createButton(String buttonName, ActionListener listener)
    {
        JButton button = new JButton(buttonName);
        button.setBorder(BorderFactory.createLineBorder(lightGray, 1));
        button.setFont(textFont);
        button.setPreferredSize(buttonDimension);
        button.addActionListener(listener);
        return button;
    }
    
    public String getSingleSearchSuggestion()
    {
        return (String) listForSearchSugg.getSelectedValue();
    }
    
    public String getSuggestionFrom()
    {
        return (String) listForNavFrom.getSelectedValue();
    }
    
    public String getSuggestionTo()
    {
        return (String) listForNavTo.getSelectedValue();
    }
    
    public SuggestionFrameType getFrameType()
    {
        return frameType;
    }
    
    public void tearDown()
    {
        removeAll();
        dispose();
    }
}