package Model;

import java.io.Serializable;
import java.util.List;

public class OSMEdgeData implements Data, Serializable{

    private static final long serialVersionUID = 1;
    private final String roadName;
    private final RoadType type;
    private final NodeWithAddress from;
    private final NodeWithAddress to;
    
    private OSMEdgeData(OSMEdgeDataBuilder builder)
    {
        from = builder.from;
        to = builder.to;
        this.roadName = builder.roadName;
        this.type = builder.type;
    }

    @Override
    public RoadType getType() {
        return type;
    }

    @Override
    public String getRoadName() {
        return roadName;
    }
    
    @Override
    public double weight()
    {
        return getLength() / 1000 / type.getSpeedLimit();
    }

    @Override
    public boolean hasHouseNumber(int number) {
        for (int house : from.getHouseNumber())
            if (house == number) return true;
        for (int house : to.getHouseNumber())
            if (house == number) return true;
        return false;
    }

    @Override
    public INode getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        for (int house : from.getHouseNumber())
            if (house == houseNumber) return from;
        for (int house : to.getHouseNumber())
            if (house == houseNumber) return to;
        return from;
    }

    @Override
    public Coordinate getFrom() {
        return from.getCoordinate();
    }

    @Override
    public Coordinate getTo() {
        return to.getCoordinate();
    }

    @Override
    public Coordinate getCenter() {
        double centerX, centerY;
        if (from.getCoordinate().x > to.getCoordinate().x) {
                centerX = (from.getCoordinate().x - to.getCoordinate().x) * 0.5 + to.getCoordinate().x;
            } else {
                centerX = (to.getCoordinate().x - from.getCoordinate().x) * 0.5 + from.getCoordinate().x;
            }
            if (from.getCoordinate().y > to.getCoordinate().y) {
                centerY = (from.getCoordinate().y - to.getCoordinate().y) * 0.5 + to.getCoordinate().y;
            } else {
                centerY = (to.getCoordinate().y - from.getCoordinate().y) * 0.5 + from.getCoordinate().y;
            }
            return new Coordinate(centerX, centerY);
    }

    @Override
    public INode getFromNode() {
        return from;
    }

    @Override
    public INode getToNode() {
        return to;
    }

    @Override
    public double getLength() {
        return GeometricCalculator.calculateDistanceBetweenCoordinates(
                from.getCoordinate(), to.getCoordinate());
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add(Data d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        NodeWithAddress closest = from;
        if (GeometricCalculator.calculateDistanceBetweenCoordinates(
                from.getCoordinate(), coord) > 
                GeometricCalculator.
                        calculateDistanceBetweenCoordinates(
                                to.getCoordinate(), coord))
            closest = to;
        return closest.getHouseNumber().length > 0 ? closest.getHouseNumber()[0] : -1;
    }
    
    public static class OSMEdgeDataBuilder
    {
        private final NodeWithAddress from;
        private final NodeWithAddress to;
        private String roadName;
        private RoadType type;
        
        public OSMEdgeDataBuilder(NodeWithAddress from, NodeWithAddress to)
        {
            this.from = from;
            this.to = to;
        }
        public OSMEdgeDataBuilder roadName(String roadName)
        {
            this.roadName = roadName;
            return this;
        }
        
        public OSMEdgeDataBuilder type(RoadType type)
        {
            this.type = type;
            return this;
        }
        
        public OSMEdgeData build()
        {
            return new OSMEdgeData(this);
        }
    }
    
    @Override
    public double getRotation() {
       return 0;
    }
    
    @Override
    public int getZipCode() {
        return from.getZip();
                }
    
    @Override
    public Coordinate getCoordFromNumber(int fromNumber){
        return getCenter();
    }
}
