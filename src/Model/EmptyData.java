package Model;

import java.util.List;

/**
 * Class used to model an empty object which implements the data interface
 *
 */
public class EmptyData implements Data
{

    @Override
    public Coordinate getFrom()
    {
        return new Coordinate(Double.NaN, Double.NaN);
    }

    @Override
    public Coordinate getTo()
    {
        return new Coordinate(Double.NaN, Double.NaN);
    }

    @Override
    public Coordinate getCenter()
    {
        return new Coordinate(Double.NaN, Double.NaN);
    }

    @Override
    public RoadType getType()
    {
        return RoadType.UNKNOWN;
    }

    @Override
    public String getRoadName()
    {
        return "Ukendt vej";
    }

    @Override
    public double weight()
    {
        return Double.NaN;
    }

    @Override
    public Node getFromNode() {
        return new Node(-1, -1,-1);
    }

    @Override
    public Node getToNode() {
        return getFromNode();
    }

    @Override
    public double getLength() {
        return Double.NaN;
    }
    
    @Override
    public double getRotation() {
       return 0;
    }
    
    @Override
    public int getZipCode() {
        return 0;
                }
    
    public Coordinate getCoordFromNumber(int fromNumber){
        return getCenter();
    }

    @Override
    public boolean hasHouseNumber(int number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void add(Data d)
    {
        
    }

    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
