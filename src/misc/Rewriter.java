package misc;

import Model.Node;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javanet.staxutils.IndentingXMLStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author christian
 */
public class Rewriter extends DefaultHandler {

    private Set<Node> notRoadNodes;
    private final IndentingXMLStreamWriter writer;
    private boolean secondRound = false;
    private List<Integer> ndList;

    public Rewriter(Set<Node> notRoadNodes) {
        this.notRoadNodes = notRoadNodes;
        ndList = new ArrayList<>();
        XMLOutputFactory writerFactory = XMLOutputFactory.newFactory();
        try {
            writer = new IndentingXMLStreamWriter(writerFactory.createXMLStreamWriter(new FileWriter("data/OSMUTM-Address-Final-4Real.osm")));
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Writer failed to be initialized");
        }
    }

    @Override
    public void startDocument() throws SAXException {

        try {
            if (!secondRound) {
                writer.writeStartDocument("1.0");
                writer.writeStartElement("OpenStreetMaps");

                for (Node node : notRoadNodes) {
                    writer.writeEmptyElement("node");
                    writer.writeAttribute("lat", "" + node.getCoordinate().y);
                    writer.writeAttribute("lon", "" + node.getCoordinate().x);
                    writer.writeAttribute("id", "" + node.getID());
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to write");
        }

    }

    @Override
    public void endDocument() throws SAXException {
        try {
            if (secondRound) {
                writer.writeEndElement();
                writer.writeEndDocument();
                writer.close();
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to write");
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        try {
            if (!secondRound) {
                if (qName.equalsIgnoreCase("node")) {
                    writer.writeStartElement("node");
                    writer.writeAttribute("lat", attributes.getValue("lat"));
                    writer.writeAttribute("lon", attributes.getValue("lon"));
                    writer.writeAttribute("id", attributes.getValue("id"));
                } else if (qName.equalsIgnoreCase("tag")) {
                    
                    for (int i = 0; i < attributes.getLength(); i++) {
                        if (attributes.getQName(i).equalsIgnoreCase("addr:street")) {
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "addr:street");
                            writer.writeAttribute("v", attributes.getValue("addr:street"));
                        } else if (attributes.getQName(i).equalsIgnoreCase("addr:postcode")) {
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "addr:postcode");
                            writer.writeAttribute("v", attributes.getValue("addr:postcode"));
                        } else if (attributes.getQName(i).equalsIgnoreCase("addr:housenumber")) {
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "addr:housenumber");
                            writer.writeAttribute("v", attributes.getValue("addr:housenumber"));
                        }
                        //writer.writeAttribute(attributes.getQName(i), attributes.getValue(i));
                    }
                }
            } else {
                if (qName.equalsIgnoreCase("way")) {
                    writer.writeStartElement("way");
                } else if (qName.equalsIgnoreCase("tag")) {

                    if (attributes.getValue("k").equalsIgnoreCase("highway")) {
                        writer.writeEmptyElement("tag");
                        writer.writeAttribute("k", "highway");
                        writer.writeAttribute("v", attributes.getValue("v"));
                    } else if (attributes.getValue("k").equalsIgnoreCase("name")) {
                        writer.writeEmptyElement("tag");
                        writer.writeAttribute("k", "name");
                        writer.writeAttribute("v", attributes.getValue("v"));
                    } else if (attributes.getValue("k").equalsIgnoreCase("natural")) {
                        writer.writeEmptyElement("tag");
                        writer.writeAttribute("k", "natural");
                        writer.writeAttribute("v", "coastline");
                    }
                } else if (qName.equals("nd")) {
                    writer.writeEmptyElement("nd");
                    writer.writeAttribute("ref", attributes.getValue("ref"));
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to write");
        }
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {
        try {
            if (!secondRound && qName.equalsIgnoreCase("node")) {
                writer.writeEndElement();
                writer.flush();
            } else if (secondRound && qName.equalsIgnoreCase("way")) {
                writer.writeEndElement();
                writer.flush();
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to write");
        }

    }

    public void parseForRoads() {
        secondRound = true;
    }
}
