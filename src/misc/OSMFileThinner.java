/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamReader;
import org.xml.sax.SAXException;

/**
 *
 * @author christian
 */
public class OSMFileThinner {

    private static XMLStreamReader reader;
    
    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();

            OSMNodeLoader handler = new OSMNodeLoader();
            
            parser.parse("data/denmark-latest.osm", handler);
            
            OSMThinnerHandler finalHandler = new OSMThinnerHandler();
            finalHandler.setNodes(handler.getNodes());
            finalHandler.setWays(handler.getWays());
            handler = null;
            
            parser.parse("data/denmark-latest.osm", finalHandler);
            
        } catch (SAXException | IOException | ParserConfigurationException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
//        try
//        {
//        XMLInputFactory factory = XMLInputFactory.newInstance();
//        reader = factory.createXMLStreamReader(new FileReader("data/denmark-latest.osm"));
//        
//        int eventType;
//        
//        while (reader.hasNext()){
//            eventType = reader.next();
//            
//            if (eventType == XMLStreamReader.START_ELEMENT)
//            {
//                switch (reader.getLocalName())
//                {
//                    case "node":
//                        beginNode();
//                        break;
//                    case "way":
//                        processWay();
//                        break;
//                }
//            }
//        }
//            
//        }
//        catch (XMLStreamException | FileNotFoundException e)
//        {
//            e.printStackTrace();
//            throw new RuntimeException();
//        }
//    }
//    
//    private static void beginNode()
//    {
//        int i = 0;
//        double lat = -1;
//        double lon = -1;
//        long id = -1;
//        while (lat < 0 || lon < 0 || id < 0)
//        {
//            switch (reader.getAttributeLocalName(i))
//            {
//                case "lat":
//                    lat = Double.parseDouble(reader.getAttributeValue(i));
//                    break;
//                case "lon":
//                   
//            }
//        }
    }
}
