
package Model;

import View.HighlightAddress;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 *
 * Class used to create a data structure which contains objects implementing the
 * Data interface. Note that due to the way data is stored within the structure,
 * more data than the data which fits within the wanted area may be returned
 */
public class Model implements IModel, Serializable {

    private final Map<RoadType, QuadTree> quadTrees;
    private final Map<RoadType, Double> halfOfLongestLengths;
    private final double x;
    private final double y;
    private final double length;
    private final Map<String, List<Data>> roadMap;
    private final Graph graph;
    private final Map<Integer, String> zipcodes;
    private final Map<RoadType, Integer> maxSizeByType;
    private int dataSize;
    private final boolean testing;

    /**
     * Constructor. Note that all loading classes are predefined
     *
     * @param loaderType the type determines which data to use
     * @param testing whether or not testing methods should be available
     */
    public Model(LoaderType loaderType, boolean testing) {
        this.testing = testing;
        double previous = System.currentTimeMillis();
        IMapData loader = LoaderFactory.getLoader(loaderType);
        double time = (System.currentTimeMillis() - previous) / 1000;
        //NodeCollection.insertNodes(loader.getNodes());
        Map<RoadType, List<Data>> dataByType = loader.getData();
        System.gc();
        System.out.println("Loaded data in " + time);

        double startX = Double.MAX_VALUE;
        double endX = -1;
        double startY = Double.MAX_VALUE;
        double endY = -1;
        roadMap = new HashMap<>();
        zipcodes = new HashMap<>();

        String dir = "data/";
        try {
            loadZipcodes(dir + "postnumredk.txt");
        } catch (IOException e) {
            //Do nothing
        }

        //initializes half of longest road and determines size of INode array
        int size = 0;
        halfOfLongestLengths = new EnumMap<>(RoadType.class);
        for (RoadType currentType : RoadType.values()) {
            halfOfLongestLengths.put(currentType, 0.0);
            if (!currentType.isNotRoad(currentType)) {
                size += dataByType.get(currentType).size() * 2;
            }
        }
        dataSize = 0;
        for (RoadType currentType : RoadType.values()) {
            dataSize += dataByType.get(currentType).size();
        }

        INode[] nodes = new INode[size + 3];
        List<Data> tempList;
        for (RoadType type : RoadType.values()) {
            for (Data current : dataByType.get(type)) {
                if (!type.isNotRoad(type)) {
                    nodes[current.getFromNode().getID()] = current.getFromNode();
                    nodes[current.getToNode().getID()] = current.getToNode();
                }

                if (current.getRoadName() != null && !current.getRoadName().isEmpty() && current.getZipCode() != -1) {
                    String address = getRoadAndCityName(current);
                    if (roadMap.get(address) != null) {
                        tempList = roadMap.get(address);
                        tempList.add(current);
                        roadMap.put(address, tempList);
                    } else {
                        tempList = new ArrayList<>();
                        tempList.add(current);
                        roadMap.put(address, tempList);
                    }
                    tempList = null;
                }
                //determines half of the length of the longest road by type
                Coordinate to = current.getTo();
                Coordinate from = current.getFrom();
                double halfOfLength = GeometricCalculator.calculateDistanceBetweenCoordinates(to, from) * 0.5;
                if (halfOfLength > halfOfLongestLengths.get(current.getType())) {
                    halfOfLongestLengths.put(current.getType(), halfOfLength);
                }
                Coordinate center = current.getCenter();
                if (startX > center.x) {
                    startX = center.x;
                }
                if (endX < center.x) {
                    endX = center.x;
                }
                if (startY > center.y) {
                    startY = center.y;
                }
                if (endY < center.y) {
                    endY = center.y;
                }
            }
        }

        //insert nodes
        NodeCollection.insertNodes(nodes);

        //make it half of longest length and set max size per type
        maxSizeByType = new EnumMap<>(RoadType.class);
        for (RoadType currentType : RoadType.values()) {
            halfOfLongestLengths.put(currentType, halfOfLongestLengths.get(currentType) * 0.5);
            maxSizeByType.put(currentType, dataByType.get(currentType).size());
        }

        //determine the length of the covered square
        if (endX - startX > endY - startY) {
            length = endX - startX;
        } else {
            length = endY - startY;
        }
        x = startX;
        y = startY;

        //build quadtrees
        quadTrees = new EnumMap<>(RoadType.class);

        for (RoadType currentType
                : RoadType.values()) {
            quadTrees.put(currentType, new QuadTree(
                    dataByType.get(currentType), startX, startY, length));
        }

        System.gc();
        graph = new Graph(dataByType);
        dataByType = null;
        System.gc();
    }

    /**
     * This method will at the least return all data which fits within the
     * given area. Note that more data may be returned to compensate
     * for placing roads by their center point. The returned data will
     * all belong to one of the given road types.
     * @param startingPoint the coordinates of the lower left corner of the
     * wanted area
     * @param width the width of the wanted area 
     * @param height the height of the wanted area
     * @param roadTypes the road types that is wanted
     * @return a list containing at the least all data which resides
     * within the given area and belongs to the given type
     */
    @Override
    public List<Data> getAreaData(Coordinate startingPoint, double width, double height, Set<RoadType> roadTypes) {
        roadTypes = roadTypes == null ? new HashSet<RoadType>() : roadTypes;
        int maxSize = 0;
        for (RoadType type : roadTypes) {
            maxSize += maxSizeByType.get(type);
        }

        List<Data> data = new ArrayList<>(maxSize);

        for (RoadType type : roadTypes) {
            double halfOfLongestRoad = halfOfLongestLengths.get(type);
            double extendedX = startingPoint.x - halfOfLongestRoad;
            double extendedY = startingPoint.y - halfOfLongestRoad;
            double extendedWidth = width + halfOfLongestRoad * 2;
            double extendedHeight = height + halfOfLongestRoad * 2;

            quadTrees.get(type).getAreaData(new Coordinate(extendedX, extendedY), extendedWidth, extendedHeight, data);
        }
        return data;
    }

    /*
     * generates the Map of zipcodes, by parsing a text file with all the 
     * cities and zipcodes in Denmark.
     */
    public void loadZipcodes(String zipFile) throws IOException {
        /* Nodes. */
        BufferedReader br;
        br = new BufferedReader(new FileReader(zipFile));
        String line;
        int zip;
        String name;
        while ((line = br.readLine()) != null) {
            zip = Integer.parseInt(line.substring(0, 4));
            name = line.substring(6);
            zipcodes.put(zip, name);
        }

        br.close();

        for (int i = 1000; i < 1500; i++) {
            zipcodes.put(i, "København K");
        }

        for (int i = 1500; i < 1800; i++) {
            zipcodes.put(i, "København V");
        }

        for (int i = 1800; i < 2000; i++) {
            zipcodes.put(i, "Frederiksberg C");
        }
    }
    
    /**
     * This method returns the edge closest to the coordinate. It works by
     * recursively going through all quadtrees that are within a radius of the
     * coordinate. This is done to ensure that the closest road is returned, 
     * even if the coordinate is just by the edge of a quadtree. If the searched
     * Area doesn't contain any edges the radius is expanded.
     * 
     * If the radius becomes to big / If the coordinate is too far away from the 
     * covered area, the method returns an empty Edge.
     * 
     * @param coordinates = the coordinate for which you want to find the 
     * closest adress.
     * 
     * @return 
     */
    @Override
    public Data getClosestRoad(Coordinate coordinates) {
        Data road = new EmptyData();
        double distance = Double.MAX_VALUE;
        double searchLength = length * 0.001;
        EnumMap<RoadType, Double> searchLengths = new EnumMap<>(RoadType.class);
        EnumMap<RoadType, Data> roadFound = new EnumMap<>(RoadType.class);
        EnumMap<RoadType, Double> roadTypeDistances = new EnumMap<>(RoadType.class);
        for (RoadType type : RoadType.values()) {
            searchLengths.put(type, searchLength);
            roadFound.put(type, road);
            roadTypeDistances.put(type, Double.MAX_VALUE);
        }

        for (RoadType type : RoadType.values()) {
            if (type == RoadType.UNKNOWN || type == RoadType.COAST) {
                continue;
            }
            do {
                Data currentRoad = quadTrees.get(type).
                        getClosestRoad(coordinates, searchLengths.get(type) + halfOfLongestLengths.get(type));
                double searchie = searchLengths.get(type) * 2;
                searchLengths.put(type, searchie);
                if (currentRoad.getType() == RoadType.UNKNOWN) {
                    continue;
                }
                double currentDistance = GeometricCalculator.calculateDistance(currentRoad, coordinates);
                if (currentDistance < roadTypeDistances.get(type)) {
                    roadFound.put(type, currentRoad);
                    roadTypeDistances.put(type, currentDistance);
                }
            } while (roadFound.get(type).getType() == RoadType.UNKNOWN && searchLengths.get(type) < length);
        }
        for (RoadType type : RoadType.values()) {
            if (type == RoadType.UNKNOWN || type == RoadType.COAST) {
                continue;
            }
            if (roadTypeDistances.get(type) < distance) {
                road = roadFound.get(type);
                distance = roadTypeDistances.get(type);
            }
        }
        assert (road != null);
        assert (road.getRoadName() != null);
        return road;
    }

    /**
     * Returns the name of the road nearest the given coordinates
     *
     * @param coordinates
     * @return the name of the road nearest to the given coordinates
     */
    @Override
    public String getRoadName(Coordinate coordinates) {
        Data road = getClosestRoad(coordinates);
        if (road.getRoadName().isEmpty()) {
            return "Ukendt";
        }
        return getClosestRoad(coordinates).getRoadName();
    }

    /**
     * Returns a Square object which represents the area covered by this class
     *
     * @return a square object representing the area covered by this model
     */
    @Override
    public Square getCoveredSquare() {
        return new Square(x, y, length);
    }

    @Override
    public Coordinate getAddressCoordinate(String s) {
        if (!roadMap.containsKey(s)) {
            throw new NullPointerException(s + " is not a road in the map.");
        }

        return roadMap.get(s).get(0).getCenter();
    }

    /**
     * returns the first edge with s name. 
     */
    @Override
    public Data getEdgeFromAddress(String s) {
        if (!roadMap.containsKey(s)) {
            throw new NullPointerException(s + " is not a road in the map.");
        }

        return roadMap.get(s).get(0);
    }

    private INode getStart(String s) {
        if (!roadMap.containsKey(s)) {
            throw new NullPointerException(s + " is not a road in the map.");
        }

        return roadMap.get(s).get(0).getFromNode();
    }

    private INode getEnd(String s) {
        if (!roadMap.containsKey(s)) {
            throw new NullPointerException(s + " is not a road in the map.");
        }

        return roadMap.get(s).get(0).getToNode();
    }

    private List<Data> getPathBetweenPoints(String start, String end, boolean isWalking) {
        INode startNode = getStart(start);
        INode endNode = getEnd(end);
        if (startNode == null || endNode == null) {
            return null;
        }
        return getPathBetweenPoints(startNode, endNode, isWalking);
    }

    private List<Data> getPathBetweenPoints(INode start, INode end, boolean isWalking) {
        DijkstraSP route = new DijkstraSP(graph, start.getID(), end.getID(), isWalking);
        Stack<Data> path = route.pathTo(end.getID());
        if (path == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(path);
    }

    /**
     * Returns a list of all the names of roads as "ROADNAME, CITYNAME"
     * 
     * @return a set of Strings containing the unique names of all the roads
     * in the data set.
     */
    @Override
    public Set<String> getAllRoadNames() {
        return roadMap.keySet();
    }

    /**
     * Returns all the names of the cities in the zipcodes Map.
     * 
     * @return the citynames as a collection of String.
     */
    @Override
    public Collection<String> getAllCityNames() {
        return zipcodes.values();
    }

    /**
     * Returns the City belonging to that zipcode.
     * @param zip = the zipcode.
     * @return the name of the city.
     */
    @Override
    public String getCityFromZip(int zip) {
        return (String) zipcodes.get(zip);
    }
    
    /**
     * Returns the name and cityname of an edge.
     * 
     * @param d the Edge.
     * @returns the name of the road as "ROADNAME CITYNAME".
     */
    @Override
    public String getRoadAndCityName(Data d) {
        return d.getRoadName() + " " + getCityFromZip(d.getZipCode());
        
    }

    /**
     * The method gets two addresses as Strings and a housenumber. The method
     * gets two coordinates, by searching the RoadMap for the Strings with
     * corresponding numbers.
     * 
     * Returns the path between two addresses in the graph, as an ordered 
     * list of Edges. If there isn't any route between the adresses or the 
     * addresses aren't in the Roadmap, it returns an Empty edge.
     */
    @Override
    public List<Data> getPathBetweenPoints(String start, String end, boolean isWalking, int fromNumber, int toNumber) {
        graph.clearTemps();
        NodeCollection.clearTemps();
        Data from = getRoadPartWithNumber(start, fromNumber);
        Data to = getRoadPartWithNumber(end, toNumber);
        INode fromNode;
        INode toNode;
        if (from == null || to == null) return new ArrayList<>();

        if (from.hasHouseNumber(fromNumber)) {
            fromNode = from.getAddressAsNode(fromNumber, NodeCollection.getTempFromID());
            if (fromNode.getID() == NodeCollection.getTempFromID()) {
                NodeCollection.insertTempFrom(fromNode);
                graph.insertTempNode(fromNode, from);
            }
        } else {
            fromNode = new Node(NodeCollection.getTempFromID(), from.getCenter().x, from.getCenter().y);
            NodeCollection.insertTempFrom(fromNode);
            graph.insertTempNode(fromNode, from);
        }
        if (to.hasHouseNumber(toNumber)) {
            toNode = to.getAddressAsNode(toNumber, NodeCollection.getTempToID());
            if (toNode.getID() == NodeCollection.getTempToID()) {
                NodeCollection.insertTempTo(toNode);
                graph.insertTempNode(toNode, to);
            }
        } else {
            toNode = new Node(NodeCollection.getTempToID(), to.getCenter().x, to.getCenter().y);
            NodeCollection.insertTempTo(toNode);
            graph.insertTempNode(toNode, to);
        }

        List<Data> list = getPathBetweenPoints(fromNode, toNode, isWalking);

        Collections.reverse(list);

        //Makes edges of the same road into a Road data.
        List<Data> shorterList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Data e = list.get(i);
            if (i > 0) {
                if (e.getRoadName().equals(list.get(i - 1).getRoadName())) {
                    if (shorterList.get(shorterList.size() - 1) instanceof Road) {
                        shorterList.get(shorterList.size() - 1).add(e);
                    } else {
                        shorterList.add(new Road(e));
                    }
                } else {
                    shorterList.add(e);
                }
            } else {
                shorterList.add(e);
            }
        }
        return shorterList;
    }

    /*
     * Returns the path between two coordinates in the graph, as an ordered 
     * list of Edges. If there isn't any route between the coordinates, it 
     * returns an Empty edge.
     */
    @Override
    public List<Data> getPathBetweenPoints(Coordinate from, Coordinate to, boolean isWalking) {
        graph.clearTemps();
        NodeCollection.clearTemps();
        Data fromRoad = getClosestRoad(from);
        Data toRoad = getClosestRoad(to);
        INode fromNode = fromRoad.getFromNode();
        INode toNode = toRoad.getToNode();

        List<Data> list = getPathBetweenPoints(fromNode, toNode, isWalking);

        Collections.reverse(list);

        //Makes edges of the same road into a Road data.
        List<Data> shorterList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Data e = list.get(i);
            if (i > 0) {
                if (e.getRoadName().equals(list.get(i - 1).getRoadName())) {
                    if (shorterList.get(shorterList.size() - 1) instanceof Road) {
                        shorterList.get(shorterList.size() - 1).add(e);
                    } else {
                        shorterList.add(new Road(e));
                    }
                } else {
                    shorterList.add(e);
                }
            } else {
                shorterList.add(e);
            }
        }
        return shorterList;
    }

    private Data getRoadPartWithNumber(String name, int number) {
        List<Data> roadParts = roadMap.get(name);
        if (roadParts != null) {
            for (Data roadPart : roadParts) {
                if (roadPart.hasHouseNumber(number)) {
                    return roadPart;
                }
            }
        }
        return roadParts != null ? roadParts.get(0) : null;
        //return roadParts.get(0); //Return "first" part of road, if house number doesn't exist on road
    }

    @Override
    public HighlightAddress getClosestAddress(Coordinate realWorldCoordinate, boolean isFrom) {
        Data road = getClosestRoad(realWorldCoordinate);
        int houseNumber = road.getCoordinateAsHouseNumber(realWorldCoordinate);
        int possiblity = isFrom ? NodeCollection.getTempFromID() : NodeCollection.getTempToID();
        INode node = road.getAddressAsNode(houseNumber, possiblity);
        if (node.getID() == possiblity) {
            if (isFrom) {
                NodeCollection.insertTempFrom(node);
            } else {
                NodeCollection.insertTempTo(node);
            }
        }
        return new HighlightAddress(node.getCoordinate(), getRoadAndCityName(road),
                houseNumber);
    }

    public Graph getGraph() {
        return graph;
    }

    /**
     * Testing method, will throw error if model is not setup for testing
     * @return sum of all data
     */
    public int getDataSize() {
        if (testing)
            return dataSize;
        else
            throw new IllegalAccessError("dataSize is not available when not testing!");
    }
    
    /*
     * Returns all the quadtrees, arranged by their roadtype.
     */
    public Map<RoadType, QuadTree> getQuadTrees()
    {
        return quadTrees;
    }
}
