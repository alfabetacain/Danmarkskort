
package Model;

import Model.MaxPQ.vej;
import java.util.Set;

/**
 * This spellchecker works by using the Edit distance-algorithm. It contains a 
 * set of Strings, then it uses a two dimensional array to compare the target 
 * word to all strings in the set, by calculating the weight of the total number 
 * of operations it takes for one word to become the other (the distance). 
 * Finally it is then able to return an array of the words with smallest 
 * distance to the target word. 
 * If one word it alot closer to the target word than the others, it only 
 * returns that word. 
 * Even though it uses time linear to the size of the data, it is still
 * a lot faster than String manipulation.
 */
public class SpellChecker
{
    //the array used to calculate the distance between 2 words in.
    private final int[][] checArray = new int[100][100];
    private int xLength;
    private int yLength;
    private int maxValue = Integer.MAX_VALUE;
    private final int relevantDistance;
    private final Set<String> _allRoads;

    /**
     *
     * @param distance the tolerance for when a road is to be returned after
     * a spellcheck.
     */
    public SpellChecker(int distance, Set<String> roadNames)
    {
        this.relevantDistance = distance;
        this._allRoads = roadNames;
        int[][] ICcounter = new int[100][100];
        ICcounter[0][0] = 3;
        for (int x = 1; x < ICcounter.length; x++)
        {
            if (x < 3)
            {
                ICcounter[x][0] = ICcounter[x - 1][0] + 2;
            }
            else
            {
                ICcounter[x][0] = ICcounter[x - 1][0] + 1;
            }
        }

        for (int y = 1; y < ICcounter.length; y++)
        {
            if (y < 3)
            {
                ICcounter[0][y] = ICcounter[0][y - 1] + 2;
            }
            else
            {
                ICcounter[0][y] = ICcounter[0][y - 1] + 1;
            }
        }

        checArray[0][0] = 0;
        for (int x = 1; x < 50; x++)
        {
            checArray[x][0] = checArray[x - 1][0] + ICcounter[x - 1][0] + 1;
        }

        for (int y = 1; y < 50; y++)
        {
            checArray[0][y] = checArray[0][y - 1] + ICcounter[0][y - 1] + 1;
        }
    }

    /**
     * Finds the weight of the total number of operations it takes for one
     * word to be equal to the other. Then it returns that distance.
     *
     * @Input; source = The word to be changed, target = the target word
     */
    public int distance(String source, String target)
    {
        if (target.startsWith(source))
        {
            return 1;
        }
        if (source.startsWith(target))
        {
            return source.length() - target.length();
        }

        //insert/delete check
        int[][] ICcounter = new int[35][35];
        ICcounter[0][0] = 3;
        for (int x = 1; x < ICcounter.length; x++)
        {
            if (x < 3)
            {
                ICcounter[x][0] = ICcounter[x - 1][0] + 2;
            }
            else
            {
                ICcounter[x][0] = ICcounter[x - 1][0] + 1;
            }
        }

        for (int y = 1; y < ICcounter.length; y++)
        {
            if (y < 3)
            {
                ICcounter[0][y] = ICcounter[0][y - 1] + 2;
            }
            else
            {
                ICcounter[0][y] = ICcounter[0][y - 1] + 1;
            }
        }
        
        xLength = target.length() + 1;
        yLength = source.length() + 1;

        int xSnit;

        for (int y = 1; y < yLength; y++)
        {
            xSnit = 50;
            for (int x = 1; x < xLength; x++)
            {
                checArray[x][y] = Integer.MAX_VALUE;

                if (x > source.length())
                {
                    if(y == target.length())
                        return checArray[x - 1][y];
                    else
                        continue;           //free insert
                }
                else if (target.charAt(x - 1) == source.charAt(y - 1))
                {
                    checArray[x][y] = Math.min(checArray[x][y], checArray[x - 1][y - 1]);           //keep
                }
                else
                {    //replace
                    if (checArray[x - 1][y] + ICcounter[x - 1][y] + 1 < checArray[x][y - 1] + ICcounter[x][y - 1] + 1)
                    {
                        int saveCheck = checArray[x][y];
                        checArray[x][y] = Math.min(checArray[x][y], checArray[x - 1][y] + ICcounter[x - 1][y] + 1);         //insert
                        if (saveCheck != checArray[x][y])
                        {
                            if (x < 3)
                            {
                                ICcounter[x][y] = ICcounter[x - 1][y] + 2;
                            }
                            else
                            {
                                ICcounter[x][y] = ICcounter[x - 1][y] + 1;
                            }
                        }
                    }
                    else
                    {
                        int saveCheck = checArray[x][y];
                        checArray[x][y] = Math.min(checArray[x][y], checArray[x][y - 1] + ICcounter[x][y - 1] + 1);         //delete
                        if (saveCheck != checArray[x][y])
                        {
                            if (y < 3)
                            {
                                ICcounter[x][y] = ICcounter[x][y - 1] + 2;
                            }
                            else
                            {
                                ICcounter[x][y] = ICcounter[x][y - 1] + 1;
                            }
                        }
                    }
                    
                    if (y >= 2 && x >= 2)
                    {
                        if (target.charAt(x - 1) == source.charAt(y - 2))
                        {
                            if (target.charAt(x - 2) == source.charAt(y - 1))
                            {
                                checArray[x][y] = Math.min(checArray[x][y], checArray[x - 2][y - 2] + 1);   //swap
                            }
                        }
                    }
                }
                
                if (checArray[x][y] < xSnit)
                    xSnit = checArray[x][y];
            }
            if (xSnit > maxValue)
                return Integer.MAX_VALUE;
        }

        return checArray[xLength - 1][yLength - 1];
    }

    /**
     * The main function of the Class. Finds and returns the words closest to
     * the _input, contained in the set
     *
     * @param _input the target word
     * @return returns an array of maximum 10 words, with the closest word on
     * place 0.
     */
    public String[] findClosest(String _input)
    {
        maxValue = Integer.MAX_VALUE;
        int closestCount = 15;
        MaxPQ<MaxPQ.vej> pq = new MaxPQ<>(closestCount);

        for (String s : _allRoads)
        {
            vej v = new vej(distance(_input.toLowerCase(), s.toLowerCase()), s);
            
            if(v.getValue()< maxValue)
                maxValue = v.getValue();
            pq.insert(v);
            //maxValue = pq.max().getValue();
            
            if (v.getValue() == 0)
            {
                String[] fastString = new String[]{v.getTing()};
                return fastString;
            }
        }

        String[] closetsRoads = new String[closestCount];
        int[] roadClosnes = new int[closestCount];
        for (int i = closetsRoads.length - 1; i >= 0; i--)
        {
            closetsRoads[i] = pq.max().getTing();
            roadClosnes[i] = pq.delMax().getValue();
        }

        int arrayLength = closestCount;
        int smallestInt = roadClosnes[0];
        for (int i = 1; i < closetsRoads.length; i++)
        {
            if (roadClosnes[i] - smallestInt > relevantDistance)
            {
                arrayLength = i;
                break;
            }
        }
        
        if(smallestInt > 10)
            arrayLength = 5;
        
        String[] closetsRoadsSmall = new String[arrayLength];
        System.arraycopy(closetsRoads, 0, closetsRoadsSmall, 0, arrayLength);
        
        for(int i = 0; i < closetsRoadsSmall.length; i++)
        {
            if(closetsRoadsSmall[i].contains("København"))
            {
                String s = closetsRoadsSmall[0];
                closetsRoadsSmall[0] = closetsRoadsSmall[i];
                closetsRoadsSmall[i] = s;
            }
        }
        
        return closetsRoadsSmall;
    }

}
