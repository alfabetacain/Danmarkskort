
package Model;

import java.util.List;
import java.util.Map;

public interface IMapData {
    public Map<RoadType, List<Data>> getData();
}
