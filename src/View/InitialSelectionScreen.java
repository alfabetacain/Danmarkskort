package View;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * A class acting as a JFrame, allowing the user to select between initializing
 * the program with either the Krak dataset or the OSM dataset.
 */
public class InitialSelectionScreen extends JFrame {
    private Dimension prefDimension = new Dimension(300, 250);
    //JButton listeners
    private ActionListener krakListen;
    private ActionListener OSMListen;

    public InitialSelectionScreen(ActionListener krakListen, ActionListener OSMListen)
    {
        addListenersForSelectionButtons(krakListen, OSMListen);
        setUp();
    }
    
    private void addListenersForSelectionButtons(ActionListener krakListen, ActionListener OSMListen)
    {
        this.krakListen = krakListen;
        this.OSMListen = OSMListen;
    }

    private void setUp()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Sets the size of the frame
        setSize(prefDimension);
        //Makes the frame open at the center of the screen
        setLocationRelativeTo(null);
        setLayout(new GridLayout(2,1));

        add(createButton("Krak data", krakListen));
        add(createButton("Open Street Maps data", OSMListen));

        pack();
        setVisible(true);
    }

    private JButton createButton(String buttonName, ActionListener listener)
    {
        JButton button = new JButton(buttonName);
        button.setBorder(BorderFactory.createEtchedBorder());
        button.setFont(new Font("Meiryo", 0, 15));
        button.addActionListener(listener);
        return button;
    }

    @Override
    public Dimension getPreferredSize()
    {
        return prefDimension;
    }
}
