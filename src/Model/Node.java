

package Model;

public class Node implements INode {
    private final int id;
    private final Coordinate coordinate;
    
    public Node(int id, double x, double y)
    {
        this.id = id;
        coordinate = new Coordinate(x, y);
    }
    
    public Node(int id, Coordinate coord)
    {
        this.id = id;
        coordinate = coord;
    }
    
    @Override
    public int getID()
    {
        return id;
    }
    
    @Override
    public Coordinate getCoordinate()
    {
        return coordinate;
    }
}
