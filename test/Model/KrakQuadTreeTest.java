
package Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class KrakQuadTreeTest {
    private static Model model;
    private static Map<RoadType, QuadTree> quadTreesByType;
    private static Set<RoadType> types = new HashSet<>();
    private static int numberOfRecursiveCalls = 0;
    
    public KrakQuadTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        model = new Model(LoaderType.KRAKLOADER, true);
        quadTreesByType = model.getQuadTrees();
        types.addAll(Arrays.asList(RoadType.values()));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Checking if all data in every quad tree matches the type that the data
     * should have, in said quad tree.
     */
    @Test
    public void testTypesInQuadTrees()
    {
        Square square = model.getCoveredSquare();
        for(RoadType type : types) {
            QuadTree specificTypeQuadTree = quadTreesByType.get(type);
            List<Data> dataInQuadTree = new ArrayList<>();
            specificTypeQuadTree.getAreaData(new Coordinate(square.getX(), square.getY()), square.getLength(), square.getLength(), dataInQuadTree);
            for(Data road : dataInQuadTree) {
                assert(road.getType().equals(type));
            }
        }
    }
    
    /**
     * Tests that every quad tree contains either some data or other quad trees.
     */
    @Test
    public void testContentsOfAllQuadTrees()
    {
        for(RoadType type : types) {
            List<QuadTree> quadTreeList = new ArrayList<>();
            quadTreeList.add(quadTreesByType.get(type));
            testForDataOrQuadTrees(quadTreeList);
        }
    }
    
    /**
     * Test the contents of a smaller list of quad trees. If empty quadtree shows
     * up, fail the 'testContentsOfAllQuadTrees' test.
     * @param givenList list of quad trees to have their contents checked.
     */
    private void testForDataOrQuadTrees(List<QuadTree> givenList)
    {
        if(givenList.isEmpty()) {
            fail("A quad tree does not contain data and does not contain other "
                    + "quad trees");
        }
        System.out.println("Running 'testForDataOrQuadTrees'" + numberOfRecursiveCalls++);
        List<QuadTree> returnedTrees = new ArrayList<>();
        for(QuadTree quadTree : givenList) {
            returnedTrees = quadTree.getQuadTreesUnlessContainsData();
            if(returnedTrees != null) //the quad tree does not only contain data
                testForDataOrQuadTrees(returnedTrees); //recursive call
        }
    }
    
}