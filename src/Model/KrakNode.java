package Model;

/**
 * An object storing the raw node data from the krak data file.
 */
public class KrakNode {
	final int ARC;
	final int KDV;
	final int KDV_ID;
        public final Coordinate coordinate;

	/**
	 * Parses node data from line, throws an IOException
	 * if something unexpected is read
	 * @param line The source line from which the NodeData fields are parsed
	 */
	public KrakNode(String line) {
		DataLine dl = new DataLine(line);
		ARC = dl.getInt();
		KDV = dl.getInt();
		KDV_ID = dl.getInt();
                coordinate = new Coordinate(dl.getDouble(), dl.getDouble());
	}

	/**
	 * Returns a string representing the node data in the same format as used in the
	 * kdv_node_unload.txt file.
	 */
        @Override
	public String toString() {
		return ARC + "," + KDV + "," + KDV_ID + "," + coordinate.toString();
	}
}
