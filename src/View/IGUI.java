package View;
import Model.IModel;
import Controller.ArrowKeyDirection;
import Controller.Controller;
import Model.Coordinate;
import Model.Square;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

/**
 * Interface specifically made for the GUI class.
 * Used in relation to other parts of the project, limiting coupling between the
 * GUI class and the rest of the program.
 */
public interface IGUI {
    //zoom in on the map by one level
    void zoomIn();
    //zoom out on the map by one level
    void zoomOut();
    //zoom to fit the drawn square, with the nearest zoom level
    void zoomToSquare(double x, double y, double sideLength);
    //set up the layout of the GUI, requiring the model to be set up
    void setupMainGUI(IModel model);
    //set up the loading screen for the program to load data
    void setupLoadingScreen();
    //enable the GUI for use
    void enableGUI();
    //disable the GUI for use
    void disableGUI();
    //move map in a direction designated by a press of an arrow key
    void moveMap(ArrowKeyDirection direction);
    //draw the square as the user drags the mouse, to designate zoom destination
    void updateZoomSquare(Square squ);
    void setStatusText(String text);
    
    /**
     * used for changing the shown map in mapview, by continuosly calling this
     * method when the mouse is moved. The map is then moved with a vector
     * equal to the new coor - the last.
     */
    void mouseDrag(Coordinate coor);
    
    //JFrame methos
    void addMouseListeners(MouseListener ml, MouseMotionListener mml, MouseWheelListener mwl);
    void addTheKeyListener(KeyListener l);
    void start();
    public Coordinate pointToCoordinate(Point point);

    // sets the mapview to show the entire map.
    public void resetView();

    //Changes the cursor to 'cursor'.
    public void setCursor(Cursor cursor);

    //the first method to be called, as it tells the gui what controller to use.
    public void setController(Controller con);
    
    //Sets the to-adress that should be marked in the MapView.
    public void setToAddress(Coordinate c, String s, int number);
    
    //Sets the to-adress that should be marked in the MapView.
    public void setFromAddress(Coordinate c, String s, int number);
    
    // change wether the route is calculated by driving or walking.
    public void changeWalkingStatus();
    
    //Get whether the route is calculated by driving or walking.
    public boolean isWalking();
    
    //If the to-address is set.
    public boolean hasTo();
    
    //If the from-address is set.
    public boolean hasFrom();
    
    //Clears the set to and from addresses.
    public void clearFromAndTo();
    
    //makes the mapView request focus.
    public void focusMapView();

    // enables the CSIZoom effect
    public void toggleCSIZoom();

    public boolean usesCSIZoom();
}
