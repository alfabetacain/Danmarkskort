package Model;

import java.util.List;

/**
 * Class for modeling political boundaries. Has type POLITICALBORDER
 */
public class PoliticalData extends BasicData
{

    private static final String name = "Grænse";
    private static final RoadType type = RoadType.POLITICALBORDER;

    public PoliticalData(double startX, double startY, double endX, double endY)
    {
        super(startX, startY, endX, endY);
    }

    @Override
    public RoadType getType()
    {
        return type;
    }

    @Override
    public String getRoadName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return "Political Border " + super.toString();
    }
    
    @Override
    public double getRotation() {
       return 0;
    }
    
    @Override
    public int getZipCode() {
        return 0;
                }
    
    public Coordinate getCoordFromNumber(int fromNumber){
        return getCenter();
    }

    @Override
    public boolean hasHouseNumber(int number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double weight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
