package Controller;

import Model.Coordinate;
import Model.IModel;
import Model.LoaderType;
import Model.Model;
import Model.Square;
import View.GUI;
import View.HighlightAddress;
import View.IGUI;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class Controller {

    private static Controller instance;
    private IModel model;
    private static IGUI gui;
    private long lastEvent;
    private long firstEvent;
    private LoaderType dataChosen;
    public Square mouseSquare;
    private double rightMouseClickTime;

    private Controller() {
        gui = new GUI();
    }

    /*
     * Starts the loading of the map, with the data set, that has been chosen
     * with the method chosenData. If no data has been chosen when this method
     * is called, the dataChosen reverts to the default 'KRAKLOADER'.
     */
    private void initializeLoading() {
        if (dataChosen == null) {
            dataChosen = LoaderType.KRAKLOADER;
        }

        model = new Model(dataChosen, false);
        gui.setupMainGUI(model);
        //Add listeners
        MouseListener ML = new MouseListener();
        gui.addMouseListeners(ML, ML, ML);


        gui.addTheKeyListener(new KeyListener());
        //Enable the GUI for user interaction
        gui.start();
        gui.enableGUI();
    }

    //Getting the singleton of the Controller.
    public static Controller getInstanceOf() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public void chosenData(LoaderType loaderType) {
        dataChosen = loaderType;

        initializeLoading();
    }

    /*
     * The keylistener for the keyboard buttons, used for movements in the 
     * MapView.
     */
    private class KeyListener extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    gui.moveMap(ArrowKeyDirection.UP);
                    break;
                case KeyEvent.VK_DOWN:
                    gui.moveMap(ArrowKeyDirection.DOWN);
                    break;
                case KeyEvent.VK_LEFT:
                    gui.moveMap(ArrowKeyDirection.LEFT);
                    break;
                case KeyEvent.VK_RIGHT:
                    gui.moveMap(ArrowKeyDirection.RIGHT);
                    break;
                case KeyEvent.VK_PLUS:
                    gui.zoomIn();
                    break;
                case KeyEvent.VK_MINUS:
                    gui.zoomOut();
                    break;
                case KeyEvent.VK_HOME:
                    gui.resetView();
                    break;
            }
        }
    }

    /*
     * the MouseListener for the mousebuttons and mouseWheel. Rightclick used 
     * for right-click menu and ZoomInBox. LeftClick used for mousedrag.
     * Mousewheel used for zoom in/out.
     */
    private class MouseListener extends MouseAdapter {
        private Coordinate MouseStart;
        private Coordinate MouseEnd;
        private boolean makingSquare = false;
        private boolean draggingMap = false;

        private void doPop(MouseEvent e) {
            PopUpMenu menu = new PopUpMenu(MouseStart, gui.isWalking());
            menu.show(e.getComponent(), e.getX(), e.getY());
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            gui.focusMapView();
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {

            if (e.getWhen() - lastEvent < 200) {
                lastEvent = e.getWhen();
                if (lastEvent - firstEvent > 500) {
                    return;
                }
            } else {
                firstEvent = e.getWhen();
            }


            int notches = e.getWheelRotation();

            if (notches < 0) {
                gui.zoomIn();
            } else if (notches > 0) {
                gui.zoomOut();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            Coordinate coordinates = gui.pointToCoordinate(e.getPoint());
            String roadName = model.getRoadName(coordinates);
            gui.setStatusText(roadName);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == 1 && !makingSquare) {
                draggingMap = true;
                MouseStart = new Coordinate(e.getX(), e.getY());
                MouseEnd = new Coordinate(e.getX(), e.getY());
            } else if (e.getButton() == 3 && !draggingMap) {
                rightMouseClickTime = System.currentTimeMillis();
                makingSquare = true;
                MouseStart = new Coordinate(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.getButton() == 1) {
                draggingMap = false;
                MouseEnd = new Coordinate(e.getX(), e.getY());
            } else if (e.getButton() == 3 && makingSquare) {
                makingSquare = false;
                MouseEnd = new Coordinate(e.getX(), e.getY());;
                setSquare();
                updateZoomSquare(mouseSquare);


                gui.zoomToSquare(mouseSquare.getX(), mouseSquare.getY(), mouseSquare.getLength());

                if (System.currentTimeMillis() - rightMouseClickTime < 250.0) {
                    doPop(e);
                }
            }
            gui.setCursor(Cursor.getDefaultCursor());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (draggingMap) {
                MouseStart = MouseEnd.Clone();
                MouseEnd = new Coordinate(e.getX(), e.getY());
                Coordinate coor = new Coordinate(MouseEnd.x - MouseStart.x, MouseEnd.y - MouseStart.y);
                gui.mouseDrag(coor);
                gui.setCursor(new Cursor(Cursor.MOVE_CURSOR));
            }

            if (makingSquare) {
                MouseEnd = new Coordinate(e.getX(), e.getY());
                setSquare();
                updateZoomSquare(mouseSquare);
            }
        }

        private void setSquare() {
            //variables to use
            Coordinate TMPMouseStart;
            Coordinate TMPMouseEnd;
            double length;

            //ther is two places wher ther is a error that I corekt her
            if (Math.abs(MouseEnd.x - MouseStart.x) < Math.abs(MouseEnd.y - MouseStart.y) && MouseEnd.x < MouseStart.x && MouseEnd.y > MouseStart.y) {
                length = MouseEnd.y - MouseStart.y;
                TMPMouseStart = new Coordinate(MouseStart.x - length, MouseStart.y);
                mouseSquare = new Square(TMPMouseStart, length);
                return;
            } else if (Math.abs(MouseEnd.x - MouseStart.x) > Math.abs(MouseEnd.y - MouseStart.y) && MouseEnd.x > MouseStart.x && MouseEnd.y < MouseStart.y) {
                length = MouseEnd.x - MouseStart.x;
                TMPMouseStart = new Coordinate(MouseStart.x, MouseStart.y - length);
                mouseSquare = new Square(TMPMouseStart, length);
                return;
            }

            //to make sure the box is a square 
            if (Math.abs(MouseEnd.x - MouseStart.x) > Math.abs(MouseEnd.y - MouseStart.y)) {
                gui.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
                TMPMouseEnd = new Coordinate(MouseEnd.x, MouseStart.y + (MouseEnd.x - MouseStart.x));
            } else {
                gui.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
                TMPMouseEnd = new Coordinate(MouseStart.x + (MouseEnd.y - MouseStart.y), MouseEnd.y);
            }

            int closeToCorner = 10;
            double xdiff = MouseEnd.x - MouseStart.x;
            double ydiff = MouseEnd.y - MouseStart.y;
            double diff = Math.abs(xdiff) - Math.abs(ydiff);
            if (0 - closeToCorner < diff
                    && diff < closeToCorner) {
                if (xdiff > 0) {
                    if (ydiff < 0) {
                        gui.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
                    } else {
                        gui.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
                    }

                } else {
                    if (ydiff < 0) {
                        gui.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
                    } else {
                        gui.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
                    }

                }
            }


            //make the values to put into the square
            length = Math.abs(TMPMouseEnd.x - MouseStart.x);

            if (MouseEnd.x < MouseStart.x) {
                TMPMouseStart = new Coordinate(TMPMouseEnd.x, 0);
            } else {
                TMPMouseStart = new Coordinate(MouseStart.x, 0);
            }

            if (MouseEnd.y < MouseStart.y) {
                TMPMouseStart = new Coordinate(TMPMouseStart.x, TMPMouseEnd.y);
            } else {
                TMPMouseStart = new Coordinate(TMPMouseStart.x, MouseStart.y);
            }

            //Make the square
            mouseSquare = new Square(TMPMouseStart, length);
        }
    }

    private class PopUpMenu extends JPopupMenu {

        JMenuItem menuItem;

        public PopUpMenu(Coordinate popCoor, boolean walking) {
            final Coordinate MouseStart = popCoor;

            menuItem = new JMenuItem("Zoom ind");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    gui.zoomIn();
                }
            });
            add(menuItem);

            menuItem = new JMenuItem("Zoom ud");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    gui.zoomOut();
                }
            });
            add(menuItem);

            menuItem = new JMenuItem("Reset zoom");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    gui.resetView();
                }
            });
            add(menuItem);

            menuItem = new JMenuItem("Zoom ind ved musen");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    int zoomRadius = 50;
                    Square zoomSquare = new Square(MouseStart.x - zoomRadius, MouseStart.y - zoomRadius, zoomRadius * 2);
                    gui.zoomToSquare(zoomSquare.getX(), zoomSquare.getY(), zoomSquare.getLength());
                }
            });
            add(menuItem);

            addSeparator();

            menuItem = new JCheckBoxMenuItem("Navigér herfra", gui.hasFrom());
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    Coordinate realWorldCoordinate = gui.pointToCoordinate(
                            new Point((int) MouseStart.x, (int) MouseStart.y));
                    HighlightAddress high = model.getClosestAddress(realWorldCoordinate, true);

                    gui.setFromAddress(high.coord, high.name, high.number);
                }
            });
            add(menuItem);

            menuItem = new JCheckBoxMenuItem("Navigér herhen", gui.hasTo());
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    Coordinate realWorldCoordinate = gui.pointToCoordinate(
                            new Point((int) MouseStart.x, (int) MouseStart.y));
                    HighlightAddress high = model.getClosestAddress(realWorldCoordinate, false);

                    gui.setToAddress(high.coord, high.name, high.number);
                }
            });
            add(menuItem);

            menuItem = new JMenuItem("Ryd ruten");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    gui.clearFromAndTo();
                }
            });
            add(menuItem);

            menuItem = new JCheckBoxMenuItem("CSIZoom", gui.usesCSIZoom());
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    gui.toggleCSIZoom();
                }
            });

            add(menuItem);
        }
    }

    private void updateZoomSquare(Square squ) {
        gui.updateZoomSquare(squ);
    }

    public static void main(String[] args) {
        //Initialization of actual program's intended behaviour
        Controller con = Controller.getInstanceOf();
        gui.setController(con);
    }
}
