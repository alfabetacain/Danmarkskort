package Controller;

/**
 * An enum to represent directions. 
 */
public enum ArrowKeyDirection {
    UP,DOWN,LEFT,RIGHT;
}