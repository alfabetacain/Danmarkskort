package Model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests the model class
 *
 */
public class OSMModelTest {

    private static Model model;

    public OSMModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        model = new Model(LoaderType.OSMLOADER, true);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Tests that all data is returned if data for the whole covered area and
     * all road types are requested
     */
    @Test
    public void testGetAllAreaData() {
        Square square = model.getCoveredSquare();
        Set<RoadType> types = new HashSet<>();
        types.addAll(Arrays.asList(RoadType.values()));
        List<Data> returned = model.getAreaData(
                new Coordinate(square.getX(), square.getY()),
                square.getLength(), square.getLength(), types);
        assert (returned.size() == model.getDataSize());
    }

    /**
     * Tests that all roads returned have an allowed road type when the given
     * area is all of the covered area
     */
    @Test
    public void testNoWrongTypeIsReturned() {
        Square square = model.getCoveredSquare();
        Set<RoadType> types = new HashSet<>();
        types.add(RoadType.PATH);
        types.add(RoadType.HIGHWAY);
        types.add(RoadType.POLITICALBORDER);
        List<Data> returned = model.getAreaData(
                new Coordinate(square.getX(), square.getY()),
                square.getLength(), square.getLength(), types);
        for (Data current : returned) {
            if (!types.contains(current.getType())) {
                System.out.println("\"" + current.getRoadName() + "\" with "
                        + "type " + current.getType() + " was not allowed");
                fail();
            }
        }
    }

    /**
     * Tests that no data is returned if no types are requested
     */
    @Test
    public void testNoDataReturnedWhenNoTypesRequested() {
        Square square = model.getCoveredSquare();
        Set<RoadType> types = new HashSet<>();
        List<Data> returned = model.getAreaData(
                new Coordinate(square.getX(), square.getY()),
                square.getLength(), square.getLength(), types);
        assert (returned.isEmpty());
    }

    /**
     * Tests that no data is returned when type set is null
     */
    @Test
    public void testNoDataReturnedWhenTypeSetIsNull() {
        Square square = model.getCoveredSquare();
        List<Data> returned = model.getAreaData(
                new Coordinate(square.getX(), square.getY()),
                square.getLength(), square.getLength(), null);
        assert (returned.isEmpty());
    }

    /**
     * Tests that no data is returned when the wanted area is off the charts
     */
    @Test
    public void testNoDataReturnedWhenAreaIsOffCharts() {
        Square square = model.getCoveredSquare();
        Set<RoadType> types = new HashSet<>();
        types.addAll(Arrays.asList(RoadType.values()));
        List<Data> returned = model.getAreaData(
                new Coordinate(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY),
                Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, types);
        assert (returned.isEmpty());
    }

    /**
     * Tests that null is never returned from the getRoadName method in Model
     */
    @Test
    public void testSomeNameIsAlwaysReturned() {
        String name = model.getRoadName(
                new Coordinate(Double.POSITIVE_INFINITY,
                        Double.POSITIVE_INFINITY)
        );
        assertNotNull(name);
    }

    /**
     * Tests that no data is lost when transitioning from a small area to a
     * larger with the same requested types
     */
    @Test
    public void noDataLostWhenIncreasingRequestedArea() {
        Square square = model.getCoveredSquare();
        Square smallSquare = new Square(square.getX(),
                square.getY(), square.getLength() / 8);
        Set<RoadType> types = new HashSet<>();
        types.addAll(Arrays.asList(RoadType.values()));
        List<Data> smallArea = model.getAreaData(
                new Coordinate(smallSquare.getX(), smallSquare.getY()),
                smallSquare.getLength(), smallSquare.getLength(), types);
        Set<Data> allRoads = new HashSet<>(model.getAreaData(
                new Coordinate(square.getX(), square.getY()),
                square.getLength() / 6, square.getLength() / 6, types));
        for (Data data : smallArea) {
            if (!allRoads.contains(data)) {
                System.out.println(data.getRoadName() + " with center: "
                        + data.getCenter() + " and type " + data.getType()
                        + " is not in large area");
                fail();
            }
        }
    }
    
    @Test
    public void testGetClosestRoad()
    {
        //Data road = model.getClosestRoad(null);
        //assertNull(road.getRoadName());
        //System.out.println(road.getRoadName());
        
        Data road = model.getClosestRoad(new Coordinate(0, 0));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
        
        road = model.getClosestRoad(new Coordinate(5585, 2534));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
        
        road = model.getClosestRoad(new Coordinate(-100, -100));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
        
        road = model.getClosestRoad(new Coordinate(2000, -2000));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
        
        road = model.getClosestRoad(new Coordinate(-2000, 2000));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
        
        road = model.getClosestRoad(new Coordinate(Double.POSITIVE_INFINITY,
                        Double.POSITIVE_INFINITY));
        assertNotNull(road.getRoadName());
        System.out.println(road.getRoadName());
    }
}
