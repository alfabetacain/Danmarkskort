package Model;

import java.util.List;
import java.util.Map;

public class MapData implements IMapData {
    
    private final KrakLoader loader;
    public MapData()
    {
        loader = new KrakLoader();
    }
    @Override
    public Map<RoadType, List<Data>> getData() {
        return loader.edges;
    }
    
    
}
