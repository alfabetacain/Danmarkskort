package Model;

/**
 * Class used for geometric calculations
 *
 */
public class GeometricCalculator {

    /**
     * Calculates the distance between a point and a line segment The class
     * representing the line must implement the Data interface
     *
     * @param edge the object representing the line
     * @param point the point to measure the distance to
     * @return the distance between the point and the line segment
     */
    public static double calculateDistance(Data edge, Coordinate point) {
        return calculateDistance(edge.getFrom(), edge.getTo(), point);
    }

    public static double calculateDistance(Coordinate lineStart, Coordinate lineEnd, Coordinate point) {
        if (!isNaN(lineStart) && !isNaN(lineEnd)) {
            Coordinate from = lineStart;
            Coordinate to = lineEnd;
            double A = point.x - from.x;
            double B = point.y - from.y;
            double C = to.x - from.x;
            double D = to.y - from.y;

            double dotProduct = A * C + B * D;
            double squaredLength = C * C + D * D;
            double param = dotProduct / squaredLength;

            double xOnLine, yOnLine;
            if (param < 0 || (Double.compare(from.x, to.x) == 0 && Double.compare(from.y, to.y) == 0)) {
                xOnLine = from.x;
                yOnLine = from.y;
            } else if (param > 1) {
                xOnLine = to.x;
                yOnLine = to.y;
            } else {
                xOnLine = from.x + param * C;
                yOnLine = from.y + param * D;
            }

            double dx = point.x - xOnLine;
            double dy = point.y - yOnLine;
            double distance = Math.sqrt(dx * dx + dy * dy);

            return distance;
        }
        return Double.MAX_VALUE;
    }

    private static boolean isNaN(Coordinate coordinates) {
        if (Double.isNaN(coordinates.x) || Double.isNaN(coordinates.y)) {
            return true;
        }
        return false;
    }

    /**
     * Calculates the distance between two points
     *
     * @param first the first point
     * @param second the second point
     * @return the distance between the points
     */
    public static double calculateDistanceBetweenCoordinates(Coordinate first, Coordinate second) {
        double x2 = second.x;
        double x1 = first.x;
        double y2 = second.y;
        double y1 = first.y;
        double distance = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        return (distance == Double.POSITIVE_INFINITY || distance == Double.NEGATIVE_INFINITY) ? 0 : distance;
    }

    /**
     * Calculates the angle between two adjacent edges.
     *
     * @param first the first edged
     * @param second the second edge
     * @return the angle
     */
    public static double getAngle(Data first, Data second) {

        if (first.getTo() != second.getFrom()) {
            System.out.println("Edges are not adjacent");
        }
        return getAngle(first.getTo(), first.getFrom(), second.getTo());
        //return 0;
    }

    public static double getAngle(Coordinate center, Coordinate from, Coordinate to) {
        double angle = Math.toDegrees(Math.atan2(to.x - center.x, to.y - center.y)
                - Math.atan2(from.x - center.x, from.y - center.y));

        return (angle < 0) ? Math.abs(angle) : 360 - angle;
    }

    private static double lengthFromCoords(Coordinate one, Coordinate two) {
        return Math.sqrt((one.x - two.x) * (one.x - two.x) + (one.y - two.y) * (one.y - two.y));
    }
}
