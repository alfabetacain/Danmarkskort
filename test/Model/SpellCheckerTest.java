package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class SpellCheckerTest
{
    private static Model M;
    private static SpellChecker SC;
    
    public SpellCheckerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
        M = new Model(LoaderType.KRAKLOADER,true);
        SC = new SpellChecker(2, M.getAllRoadNames());
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of distance method, of class SpellChecker.
     */
    @Test
    public void testDistanceVirker()
    {
        System.out.println("distance");
        String source = "a";
        String target = "a";
        SpellChecker instance = SC;
        int expResult = 1;
        int result = instance.distance(source, target);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDistanceMinimumsInput()
    {
        System.out.println("distance");
        String source = "";
        String target = "";
        SpellChecker instance = SC;
        int expResult = 1;
        int result = instance.distance(source, target);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDistanceMaxsimumsInput()
    {
        System.out.println("distance");
        String source = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        String target = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        SpellChecker instance = SC;
        int expResult = 1;
        int result = instance.distance(source, target);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDistanceBukstav()
    {
        System.out.println("distance");
        String source = "æøaå";
        String target = "æøåa";
        SpellChecker instance = SC;
        int expResult = 1;
        int result = instance.distance(source, target);
        assertEquals(expResult, result);
    }

    /**
     * Test of findClosest method, of class SpellChecker.
     */    
    @Test
    public void testFindClosestFast()
    {        
        SpellCheckerGodTest instance = new SpellCheckerGodTest();
        boolean result = instance.getResult();
        
        assertEquals("den store test: ", true, result);
    }
}