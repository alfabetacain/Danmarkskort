
package Model;

import java.util.Collection;

public class NodeCollection {
    private static INode[] nodes = null;
    
    public static void insertNodes(Collection<INode> list)
    {
        if (nodes != null) throw new 
        IllegalStateException("Nodes can only be inserted "
                + "into NodeCollection once");
        nodes = new Node[list.size()+3];
        System.out.println("List size: " + list.size());
        for (INode current : list)
        {
            nodes[current.getID()] = current;
        }
        System.out.println("size of nodes: " + nodes.length);
    }
    
    public static void insertNodes(INode[] array)
    {
        if (nodes != null) throw new 
        IllegalStateException("Nodes can only be inserted "
                + "into NodeCollection once");
        nodes = array;
    }
    
    public static INode getNode(int i)
    {
        if (i < 0 || i > nodes.length) throw new IllegalArgumentException(
                "i must be between 0 and " + nodes.length + " - i = " + i
        );
        return nodes[i];
    }
    
    public static int size()
    {
        return nodes.length;
    }
    
    public static int getTempFromID()
    {
        return nodes.length - 2;
    }
    
    public static int getTempToID()
    {
        return nodes.length - 1;
    }
    
    public static void insertTempFrom(INode node)
    {
        if (node.getID() != getTempFromID()) 
            throw new IllegalArgumentException("Node must have"
                    + " the same id as the integer returned from"
                    + " getTempFromID()");
        nodes[nodes.length-2] = node;
    }
    
    public static void insertTempTo(INode node)
    {
        if (node.getID() != getTempToID()) 
            throw new IllegalArgumentException("Node must have"
                    + " the same id as the integer returned from"
                    + " getTempToID()");
        nodes[nodes.length-1] = node;
    }
    
    public static void clearTemps()
    {
        nodes[nodes.length-2] = null;
        nodes[nodes.length-1] = null;
    }
}
