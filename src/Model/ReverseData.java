
package Model;

import java.io.Serializable;
import java.util.List;

public class ReverseData implements Data, Serializable {

    private final Data origin;
    public ReverseData(Data origin)
    {
        this.origin = origin;
    }
    @Override
    public Coordinate getFrom() {
        return origin.getTo();
    }

    @Override
    public Coordinate getTo() {
        return origin.getFrom();
    }

    @Override
    public Coordinate getCenter() {
        return origin.getCenter();
    }

    @Override
    public RoadType getType() {
        return origin.getType();
    }

    @Override
    public String getRoadName() {
        return origin.getRoadName();
    }
    
    @Override
    public double weight()
    {
        return origin.weight();
    }

    @Override
    public INode getFromNode() {
        return origin.getToNode();
    }

    @Override
    public INode getToNode() {
        return origin.getFromNode();
    }

    @Override
    public double getLength() {
        return origin.getLength();
    }
    
    @Override
    public double getRotation() {
       return 0;
    }
    
    @Override
    public int getZipCode() {
        return 0;
                }
    
    public Coordinate getCoordFromNumber(int fromNumber){
        return getCenter();
    }

    @Override
    public boolean hasHouseNumber(int number) {
        return origin.hasHouseNumber(number);
    }

    @Override
    public INode getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        return origin.getAddressAsNode(houseNumber, possibleNodeNumber);
    }
    
    @Override
    public void add(Data d)
    {
        
    }

    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
