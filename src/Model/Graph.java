package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Robert Sedgewick
 * @author Kevin Wayne
 */
public class Graph implements Serializable {

    private final int V;
    private int E;
    private List<Data>[] adj;

    /**
     * Initializes an empty edge-weighted digraph with <tt>V</tt> vertices and 0
     * edges. param V the number of vertices
     *
     * @param data
     * @throws java.lang.IllegalArgumentException if <tt>V</tt> < 0
     */
    public Graph(Map<RoadType, List<Data>> data) {
        if (data.size() < 0) {
            throw new IllegalArgumentException("Number of vertices in a Digraph must be nonnegative");
        }
        int size = 0;
        for (RoadType type : RoadType.values()) {
            if (!type.isNotRoad(type)) {
                size += data.get(type).size()*2;
            }
        }
        this.V = size + 3;
        this.E = 0;
        
        createArray(V);
        fillArray(data);
    }
    
    private void createArray(int size)
    {
        adj = (List<Data>[]) new ArrayList[size];
        for (int i = 0; i < adj.length; i++)
            adj[i] = new ArrayList<>();
    }
    
    private void fillArray(Map<RoadType, List<Data>> map)
    {
        for (RoadType type : RoadType.values()) {
            if (!type.isNotRoad(type)) {
                for (Data current : map.get(type)) {
                    addEdge(current);
                    addEdge(new ReverseData(current));
                }
            }
        }
    }

    /**
     * Returns the number of vertices in the edge-weighted digraph.
     *
     * @return the number of vertices in the edge-weighted digraph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in the edge-weighted digraph.
     *
     * @return the number of edges in the edge-weighted digraph
     */
    public int E() {
        return E;
    }

    /**
     * Adds the directed edge <tt>e</tt> to the edge-weighted digraph.
     *
     * @param e the edge
     */
    public void addEdge(Data e) {
        adj[e.getFromNode().getID()].add(e);
//        adj.get(e.getFrom()).add(e);
        E++;
    }

    /**
     * Returns the directed edges incident from vertex <tt>v</tt>.
     *
     * @return the directed edges incident from vertex <tt>v</tt> as an Iterable
     * @param v the vertex
     * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
     */
    public Iterable<Data> adj(int v) {
//        return adj.get(v);
        return adj[v];
    }

    /**
     * Returns all directed edges in the edge-weighted digraph. To iterate over
     * the edges in the edge-weighted graph, use foreach notation:
     * <tt>for (DirectedEdge e : G.edges())</tt>.
     *
     * @return all edges in the edge-weighted graph as an Iterable.
     */
    public Iterable<Data> edges() {
//        List<Data> list = new ArrayList<>(V);
//        for (Coordinate coordinate : adj.keySet()) {
//            for (Data e : adj.get(coordinate)) {
//                list.add(e);
//            }
//        }
//        return list;
        List<Data> list = new ArrayList<>(V);
        for (int i = 0; i < V; i++) {
            for (Data data : adj[i]) {
                list.add(data);
            }
        }
        return list;
    }

    public Iterable<Coordinate> vertices() {
        Set<Coordinate> coordinates = new HashSet<>();
        for (Data current : edges()) {
            coordinates.add(current.getFrom());
            coordinates.add(current.getTo());
        }
        return coordinates;
    }

    /**
     * Returns the number of directed edges incident from vertex <tt>v</tt>.
     * This is known as the <em>outdegree</em> of vertex <tt>v</tt>.
     *
     * @return the number of directed edges incident from vertex <tt>v</tt>
     * @param v the vertex
     * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
     */
    public int outdegree(int v) {
//        return adj.get(v).size();
        return adj[v].size();
    }

    public void insertTempNode(INode node, Data edge) {
//        int insertion = node.getID();
//        if (adj[adj.length - 2].isEmpty()) {
//            insertion = adj.length - 2;
//        } else {
//            insertion = adj.length - 1;
//        }
        addEdge(new TempGraphEdge(node, edge.getFromNode(), edge));
        addEdge(new TempGraphEdge(node, edge.getToNode(), edge));
        addEdge(new TempGraphEdge(edge.getFromNode(), node, edge));
        addEdge(new TempGraphEdge(edge.getToNode(), node, edge));
    }

    public void clearTemps() {
        if (!adj[NodeCollection.getTempFromID()].isEmpty()) {
            purgeNode(NodeCollection.getTempFromID());
        }
        if (!adj[NodeCollection.getTempToID()].isEmpty()) {
            purgeNode(NodeCollection.getTempToID());
        }
    }

    private void purgeNode(int node) {
        for (Data edge : adj[node]) {
            Iterator<Data> iterator = adj[edge.getToNode().getID()].iterator();
            while (iterator.hasNext()) {
                Data current = iterator.next();
                if (current.getToNode().getID() == node) {
                    iterator.remove();
                }
            }
        }
        adj[node].clear();
    }
}
