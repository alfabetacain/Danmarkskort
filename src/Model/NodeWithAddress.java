
package Model;

import java.io.Serializable;
import java.util.List;

public class NodeWithAddress implements Serializable, INode {
    
    private static final long serialVersionUID = 1;
    private final int[] houseNumbers;
    private final int zip;
    private final int id;
    private final Coordinate coordinate;
    
    public NodeWithAddress(int i, double x, double y, String roadName, List<String> houseNumbers, int zip)
    {
        id = i;
        coordinate = new Coordinate(x,y);
        this.houseNumbers = new int[houseNumbers.size()];
        for (int j = 0; j < houseNumbers.size(); j++)
        {
            String houseNumber = houseNumbers.get(j).replaceAll("\\D", "");
            if (!houseNumber.isEmpty())this.houseNumbers[j] = Integer.parseInt(houseNumber);
        }
        
        this.zip = zip;
    }
    
    public int[] getHouseNumber()
    {
        return houseNumbers;
    }
    
    public int getZip()
    {
        return zip;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public Coordinate getCoordinate() {
        return coordinate;
    }
}
