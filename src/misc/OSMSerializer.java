/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import Model.Data;
import Model.OSMLoader;
import Model.RoadType;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class OSMSerializer {

    public static void main(String[] args) {

        if (true) {
            OSMLoader loader = new OSMLoader();
            //Collection<NodeWithAddress> nodes = loader.getSerialNodes();
            Map<RoadType, List<Data>> data = loader.getData();
            loader = null;

            try {
                RandomAccessFile raf = new RandomAccessFile("data/serialEdges.ser", "rw");
                FileOutputStream fileOut = new FileOutputStream(raf.getFD());
                ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(fileOut, 64 * 1024));

                out.writeObject(data);
//                out.writeObject(data);
                out.close();
                fileOut.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Failure to write");
            }
        } else {
            try {
                RandomAccessFile raf = new RandomAccessFile("data/serialEdges.ser", "r");
                FileInputStream fileIn = new FileInputStream(raf.getFD());
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fileIn, 65536));
                Map<RoadType, List<Data>> map = (EnumMap<RoadType, List<Data>>) in.readObject();

                int size = 0;
                for (RoadType type : RoadType.values())
                    size += map.get(type).size();
                    System.out.println("Size: " + size);
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
