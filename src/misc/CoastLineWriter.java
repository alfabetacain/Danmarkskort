package misc;

import Model.CoordinateConversion;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 *
 * Takes a file with coordinates in the format WGS84 and writes to a new file in
 * the format UTM 32 North and writes the single coordinates together so as to
 * represent lines rather than single point The file must declare when a new
 * line segment starts with the marker '<' or simply by having an empty line
 * The new file will be called correctedCoastLine.txt and will be placed
 * in a data folder
 * This class uses a modified version of the class Coordinate Conversion
 * originally written by Sami Salkosuo. 
 */
public class CoastLineWriter
{

    /**
     * Takes the name of a file with coordinates in the format WGS84 and writes
     * to a new file in the format UTM 32 North and writes the single
     * coordinates together so as to represent lines rather than single point
     * The file must declare when a new line segment starts with the marker '<'
     *
     * @param rawDataFileName file name of the fiile containing the raw data
     */
    public void writeCorrectedCoastLine(String rawDataFileName, String endDataFileName)
    {
        try
        {
            write(rawDataFileName, endDataFileName);
        }
        catch (IOException e)
        {
            System.out.println("Error when writing!");
            e.printStackTrace();
        }
    }

    private void write(String rawCoastData, String endDataFileName) throws IOException
    {
        //raw data in WGS84 format
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("data\\" + rawCoastData + ".txt")));

        //File to be written to with the coordinates in UTM 32 North format
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("data\\" + endDataFileName + ".txt")));

        in.readLine(); //column names

        CoordinateConversion converter = new CoordinateConversion();

        String[] array;

        String line;
        String tempLine;

        double tempX, tempY, previousX, previousY, currentX, currentY;
        previousX = -1.0;
        previousY = -1.0;

        while ((line = in.readLine()) != null)
        {
            if (line.contains(">") || line.isEmpty())
            {
                previousX = -1;
            }
            else if (previousX < 0)
            {
                line = line.trim();
                array = line.split("\\s+");

                tempX = Double.parseDouble(array[0]);
                tempY = Double.parseDouble(array[1]);

                tempLine = converter.latLon2UTM(tempY, tempX);

                array = tempLine.split("\\s");

                previousX = Double.parseDouble(array[2]);
                previousY = Double.parseDouble(array[3]);
            }
            else
            {
                line = line.trim();
                array = line.split("\\s+");

                tempX = Double.parseDouble(array[0]);
                tempY = Double.parseDouble(array[1]);

                tempLine = converter.latLon2UTM(tempY, tempX);

                array = tempLine.split("\\s");

                currentX = Double.parseDouble(array[2]);
                currentY = Double.parseDouble(array[3]);

                out.append(previousX + "," + previousY + "," + currentX + "," + currentY + "\r\n");
                out.flush();

                previousX = currentX;
                previousY = currentY;
            }

        }
        in.close();
        out.close();
    }

    /*
    Main method for quickly writing a file which can be read
    */
    public static void main(String[] args)
    {
        CoastLineWriter writer = new CoastLineWriter();
        //writer.writeCorrectedCoastLine("rawCoastData", "correctedCoastLine");
        
        writer.writeCorrectedCoastLine("danish_coastline_pol", "correctedPoliticalBorder");
        writer.writeCorrectedCoastLine("danish_coastline_shore", "correctedCoastLine");
        writer.writeCorrectedCoastLine("danish_coastline_river", "correctedRiverLine");
    }
}
