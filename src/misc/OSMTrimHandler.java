/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import Model.RoadType;
import java.io.FileWriter;
import java.io.IOException;
import javanet.staxutils.IndentingXMLStreamWriter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author christian
 */
public class OSMTrimHandler extends DefaultHandler {

    private IndentingXMLStreamWriter writer;
    private boolean parsingNode = false;
    private boolean parsingWay = false;

    public OSMTrimHandler() {
        XMLOutputFactory writerFactory = XMLOutputFactory.newFactory();
        try {
            writer = new IndentingXMLStreamWriter(writerFactory.createXMLStreamWriter(new FileWriter("data/OSMUTM-Final.osm")));
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to create writer");
        }

    }

    @Override
    public void startDocument() throws SAXException {
        try {
            writer.writeStartDocument("1.0");
            writer.writeStartElement("OpenStreetMaps");
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to write");
        }
    }

    @Override
    public void endDocument() throws SAXException {
        try {
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.close();
        } catch (XMLStreamException e) {

        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        try {
            if (qName.equalsIgnoreCase("node")) {
                writer.writeStartElement("node");
                writer.writeAttribute("lat", attributes.getValue("lat"));
                writer.writeAttribute("lon", attributes.getValue("lon"));
                writer.writeAttribute("id", attributes.getValue("id"));
                parsingNode = true;
            } else if (qName.equalsIgnoreCase("way")) {
                writer.writeStartElement("way");
                parsingWay = true;
            } else if (qName.equalsIgnoreCase("tag")) {
                if (attributes.getValue("v") != null && !attributes.getValue("v").trim().isEmpty()) {
                    switch (attributes.getValue("k")) {
                        case "addr:postcode":
                            if (!attributes.getValue("v").replaceAll("\\D", "").trim().isEmpty()) {
                                writer.writeEmptyElement("tag");
                                writer.writeAttribute("k", "addr:postcode");
                                writer.writeAttribute("v", attributes.getValue("v").replaceAll("\\D", "").trim());
                            }
                            break;
                        case "addr:housenumber":
                            if (!attributes.getValue("v").replaceAll("\\D", "").trim().isEmpty()) {
                                writer.writeEmptyElement("tag");
                                writer.writeAttribute("k", "addr:housenumber");
                                writer.writeAttribute("v", attributes.getValue("v").replaceAll("\\D", "").trim());
                            }
                            break;
                        case "addr:street":
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "addr:street");
                            writer.writeAttribute("v", attributes.getValue("v").trim());
                            break;
                        case "name":
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "name");
                            writer.writeAttribute("v", attributes.getValue("v"));
                            break;
                        case "highway":
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "highway");
                            writer.writeAttribute("v", getRoadType(attributes.getValue("v")));
                            break;
                        case "natural":
                            writer.writeEmptyElement("tag");
                            writer.writeAttribute("k", "natural");
                            writer.writeAttribute("v", "coastline");
                            break;
                    }
                }
            } else if (qName.equalsIgnoreCase("nd")) {
                writer.writeEmptyElement("nd");
                writer.writeAttribute("ref", attributes.getValue("ref"));
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to write");
        }
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {
        try {
            if (qName.equalsIgnoreCase("node") && parsingNode) {
                writer.writeEndElement();
                parsingNode = false;
            } else if (qName.equalsIgnoreCase("way") && parsingWay) {
                writer.writeEndElement();
                parsingWay = false;
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to write");
        }
    }

    private String getRoadType(String type) {
        switch (type) {
            case "HIGHWAY":
                return RoadType.HIGHWAY.toString();

            case "EXPRESSWAY":
                return RoadType.EXPRESSWAY.toString();

            case "SUBROAD":
                return RoadType.SUBROAD.toString();

            case "MAINROAD":
                return RoadType.MAINROAD.toString();

            case "REGULARROAD":
                return RoadType.REGULARROAD.toString();

            case "PATH":
                return RoadType.PATH.toString();

            case "FERRYROUTE":
                return RoadType.FERRYROUTE.toString();

            case "motorway":
                return RoadType.HIGHWAY.toString();

            case "trunk":
                return RoadType.EXPRESSWAY.toString();

            case "primary":
                return RoadType.MAINROAD.toString();

            case "secondary":
                return RoadType.REGULARROAD.toString();

            case "tertiary":
                return RoadType.SUBROAD.toString();

            case "unclassified":
                return RoadType.SUBROAD.toString();

            case "residential":
                return RoadType.SUBROAD.toString();

            case "service":
                return RoadType.SUBROAD.toString();

            case "motorway_link":
                return RoadType.HIGHWAY.toString();

            case "trunk_link":
                return RoadType.EXPRESSWAY.toString();

            case "primary_link":
                return RoadType.MAINROAD.toString();

            case "secondary_link":
                return RoadType.REGULARROAD.toString();

            case "tertiary_link":
                return RoadType.SUBROAD.toString();

            case "living_street":
                return RoadType.SUBROAD.toString();

            case "pedestrian":
                return RoadType.PATH.toString();

            case "track":
                return RoadType.SUBROAD.toString();

            case "footway":
                return RoadType.PATH.toString();

            case "bridleway":
                return RoadType.PATH.toString();

            case "steps":
                return RoadType.PATH.toString();

            case "path":
                return RoadType.PATH.toString();

            case "cycleway":
                return RoadType.PATH.toString();

            case "crossing":
                return RoadType.PATH.toString();

            case "escape":
                return RoadType.HIGHWAY.toString();

            default:
                return RoadType.SUBROAD.toString();

        }
    }

    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            OSMTrimHandler handler = new OSMTrimHandler();
            parser.parse("data/OSMUTM-Address-Final-4Real.osm", handler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failure to create parser");
        }
    }
}
