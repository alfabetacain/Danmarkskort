package View;

/**
 * An enum class to help define in which scenarios a search suggestion frame was
 * created.
 */
public enum SuggestionFrameType {
    NAVIGATEFROMANDTO, NAVIGATETO, NAVIGATEFROM, SEARCH;
}
