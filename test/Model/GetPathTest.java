package Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests the model class 
 */
public class GetPathTest
{

    private static Model model;

    public GetPathTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        model = new Model(LoaderType.TESTER, true);
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testInputA()
    {
        List<Data> list = model.getPathBetweenPoints("Vej1 Valby", "Vej2 Valby", false, 1, 1);
        assert(list.isEmpty());
    }
    
    @Test
    public void testInputB()
    {
        List<Data> list = model.getPathBetweenPoints("Vej3 Valby", "Vej4 Valby", false, 0, 0);
        System.out.println("Test b has size " + list.size());
        assert(list.size() == 1);
    }
    
    @Test
    public void testInputC()
    {
        List<Data> list = model.getPathBetweenPoints("Vej5 Valby", "Vej6 Valby", false, 1, 1);
        System.out.println("Test c has size " + list.size());
        assert(list.size() > 1);
    }
    
    @Test
    public void testInputD()
    {
        List<Data> list = model.getPathBetweenPoints("", "", false, 0, 0);
        System.out.println("Test d has size " + list.size());
        assert(list.isEmpty());
    }
}
    
