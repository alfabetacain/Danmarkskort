
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DataTest
{

    private static Data coastData;

    public DataTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        coastData = new CoastData(20.0, 20.0, 10.0, 10.0);
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testCoastDataHasTypeCoast()
    {
        assert (coastData.getType().compareTo(RoadType.COAST) == 0);
    }

    /**
     * Test of getTo method, of class Data.
     */
    @Test
    public void testCoastDataCenterCorrectlyCalculated()
    {
        Coordinate center = coastData.getCenter();
        assert (Double.compare(center.x, 15.0) == 0);
        assert (Double.compare(center.y, 15.0) == 0);
    }

    @Test
    public void testCoastDataGetFrom()
    {
        double startX = 20.0;
        double startY = 20.0;

        Coordinate from = coastData.getFrom();
        assert (Double.compare(from.x, startX) == 0);
        assert (Double.compare(from.y, startY) == 0);
    }

    @Test
    public void testCoastDataGetTo()
    {
        double endX = 10.0;
        double endY = 10.0;

        Coordinate to = coastData.getTo();
        assert (Double.compare(to.x, endX) == 0);
        assert (Double.compare(to.y, endY) == 0);
    }

    /**
     * Test of getRoadName method, of class CoastData.
     */
    @Test
    public void testCoastDataGetRoadName()
    {
        String name = "Kyst";
        assertEquals(name, coastData.getRoadName());
    }
}
