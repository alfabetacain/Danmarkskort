
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to contain an ArrayList of edges.
 */
public class Road implements  Data  {
    private List<Data> edges;
    private double weight;
    private double LENGTH;
    
    
    public Road(Data e) {
        edges = new ArrayList<>();
        edges.add(e);
        weight += e.weight();
        LENGTH += e.getLength();
    }
    
    

    @Override
    public Coordinate getFrom() {
        return edges.get(0).getFrom();
    }

    @Override
    public Coordinate getTo() {
        
        return edges.get(edges.size()-1).getTo();
    }

    @Override
    public Coordinate getCenter() {
        //is not reliable, but easy.
        return edges.get((int)(edges.size()/2)).getCenter();
    }

    @Override
    public RoadType getType() {
        return edges.get(0).getType();
    }

    @Override
    public String getRoadName() {
        return edges.get(0).getRoadName();
    }

    @Override
    public double weight() {return weight;    }

    @Override
    public INode getFromNode() {
        return edges.get(0).getFromNode();    }

    @Override
    public INode getToNode() {
        return edges.get(edges.size()-1).getToNode();
    }

    @Override
    public double getRotation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getLength() {
        return LENGTH;
    }

    @Override
    public int getZipCode() {
        
        return edges.get(0).getZipCode();        
    }

    @Override
    public Coordinate getCoordFromNumber(int fromNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasHouseNumber(int number) {
        boolean b = false;
        for (Data d : edges) {
            if(d.hasHouseNumber(number))
                b=true;
        }
        return b;
    }

    @Override
    public boolean isNode(int houseNumber) {
        return false;
    }

    @Override
    public Node getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        throw new UnsupportedOperationException("Not supported in Road");
    }
    
    @Override
    public void add(Data d)
    {
        edges.add(d);
        
        weight += d.weight();
        LENGTH += d.getLength();
    }
    
    @Override
    public List<Data> getEdges()
    {
        return edges;
    }

    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
