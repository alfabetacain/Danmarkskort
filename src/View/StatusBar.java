package View;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class which is used to represent a status bar Has method for changing what is
 * displayed in the bar
 */
public class StatusBar extends JPanel
{

    JLabel text;

    /**
     * Constructs the StatusBar with a predefined initial message
     */
    public StatusBar(boolean borderBox)
    {
        text = new JLabel(" Initialized ");
        if(borderBox) {
            text.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        }
        text.setFont(new java.awt.Font("Meiryo UI", 0, 12));
        add(text);
    }

    /**
     * Changes the text in the status bar to the given input string
     *
     * @param text the string which the status bar shall display
     */
    public void setText(String text)
    {
        this.text.setText(" " + text + " ");
        repaint();
    }

    @Override
    public Dimension getPreferredSize()
    {
        //Pixels chosen as per frame dimension definition of 650x650
        return new Dimension(634, 28);
    }
    
    public String getText()
    {
        return text.getText();
    }
}
