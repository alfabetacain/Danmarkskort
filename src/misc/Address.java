/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package misc;

/**
 *
 * @author christian
 */
public class Address {
    private final String roadName;
    private final String zip;
    private final String housenumber;
    
    public Address(String name, String zip, String housenumber)
    {
        this.housenumber = housenumber;
        this.zip = zip;
        this.roadName = name;
    }
    
    public String getRoadName()
    {
        return roadName;
    }
    
    public String getZip()
    {
        return zip;
    }
    
    public String getHouseNumber()
    {
        return housenumber;
    }
}
