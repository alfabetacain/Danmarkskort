package Model;

import java.io.Serializable;

/**
 * Class used to represent coordinates. No specific coordinate system is
 * required
 */
public class Coordinate implements Serializable
{
    public final double x;
    public final double y;
    
    public Coordinate(double _x, double _y)
    {
        this.x = _x;
        this.y = _y;
    }
    
    public Coordinate Clone()
    {
        return new Coordinate(x, y);
    }
    
    @Override
    public String toString()
    {
        return ("(" + x + "," + y + ")");
    }
}
