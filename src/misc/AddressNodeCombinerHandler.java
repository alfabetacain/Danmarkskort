/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import Model.Coordinate;
import Model.CoordinateConversion;
import Model.Data;
import Model.GeometricCalculator;
import Model.IModel;
import Model.Node;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javanet.staxutils.IndentingXMLStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author christian
 */
public class AddressNodeCombinerHandler extends DefaultHandler {

    private Document document;
    private IndentingXMLStreamWriter writer;
    private boolean parsingNode;
    private boolean parsingWay;
    private Map<Node, List<Address>> nodes;
    private HashMap<String, String> realNodeId;
    private Map<String, List<Data>> ways;
    private CoordinateConversion converter;
    private String roadName;
    private String zip;
    private String housenumber;
    private Coordinate coordinate;
    private List<Data> coast;
    private static final String EMPTY_STRING = "";
    private int count = 0;
    private final IModel model;
    private int numberOfBounds = 0;

    public AddressNodeCombinerHandler(Set<Node> nodes, List<Data> ways, List<Data> coast, IModel model) {
        this.coast = coast;
        this.model = model;
        converter = new CoordinateConversion();
        this.nodes = new HashMap<>();
        for (Node node : nodes) {
            this.nodes.put(node, new ArrayList<Address>());
        }

        this.ways = new HashMap<>();
        for (Data data : ways) {
            if (this.ways.containsKey(data.getRoadName())) {
                this.ways.get(data.getRoadName()).add(data);
            } else {
                this.ways.put(data.getRoadName(), new ArrayList<Data>());
                this.ways.get(data.getRoadName()).add(data);
            }
        }
        
        for (String name : this.ways.keySet())
            count += this.ways.get(name).size();
        System.out.println("Total number of roads: " + count);
        count = 0;

        roadName = EMPTY_STRING;
    }

    @Override
    public void startDocument() throws SAXException {

    }

    @Override
    public void endDocument() throws SAXException {
        XMLOutputFactory writerFactory = XMLOutputFactory.newFactory();
        try {
            writer = new IndentingXMLStreamWriter(writerFactory.createXMLStreamWriter(new FileWriter("data/OSMUTM-Address.osm")));
            writer.writeStartDocument("1.0");
            writer.writeStartElement("OpenStreetMaps");

            //write nodes
            for (Node node : nodes.keySet()) {
                writer.writeStartElement("node");
                writer.writeAttribute("lat", "" + node.getCoordinate().y);
                writer.writeAttribute("lon", "" + node.getCoordinate().x);
                writer.writeAttribute("id", "" + node.getID());
                //write addresses if any
                for (Address address : nodes.get(node)) {
                    writer.writeEmptyElement("tag");
                    writer.writeAttribute("addr:street", address.getRoadName());
//                    writer.writeEndElement();
                    writer.writeEmptyElement("tag");
                    writer.writeAttribute("addr:postcode", address.getZip());
//                    writer.writeEndElement();
                    writer.writeEmptyElement("tag");
                    writer.writeAttribute("addr:housenumber", address.getHouseNumber());
//                    writer.writeEndElement();
                }
                writer.writeEndElement();
            }

            //write roads
            for (List<Data> list : ways.values()) {
                for (Data data : list) {
                    writer.writeStartElement("way");
                    writer.writeEmptyElement("nd");
                    writer.writeAttribute("ref", "" + data.getFromNode().getID());
                    writer.writeEmptyElement("nd");
                    writer.writeAttribute("ref", "" + data.getToNode().getID());
                    writer.writeEmptyElement("tag");
                    writer.writeAttribute("k", "highway");
                    writer.writeAttribute("v", data.getType().toString());
                    writer.writeEndElement();
                }
            }

            //write coast
            for (Data data : coast) {
                writer.writeStartElement("way");
                writer.writeEmptyElement("nd");
                writer.writeAttribute("ref", "" + data.getFromNode().getID());
                writer.writeEmptyElement("nd");
                writer.writeAttribute("ref", "" + data.getToNode().getID());
                writer.writeEmptyElement("tag");
                writer.writeAttribute("natural", "coastline");
                writer.writeEndElement();
            }

            writer.writeEndElement();
            writer.writeEndDocument();
            writer.close();
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (count++ % 100000 == 0) System.out.println("Count: " + count);
        if (qName.equalsIgnoreCase("node")) {
            parsingNode = true;
            double lon = Double.parseDouble(attributes.getValue("lon"));
            double lat = Double.parseDouble(attributes.getValue("lat"));
            String utmFormat = converter.latLon2UTM(lat, lon);
            String[] array = utmFormat.split("\\s");
            coordinate = new Coordinate(Double.parseDouble(array[2]), Double.parseDouble(array[3]));
        } else if (parsingNode && qName.equalsIgnoreCase("tag")) {
            switch (attributes.getValue("k")) {
                case "addr:street":
                    roadName = attributes.getValue("v");
                    break;
                case "addr:postcode":
                    zip = attributes.getValue("v");
                    break;
                case "addr:housenumber":
                    housenumber = attributes.getValue("v");
                    break;
            }
        }
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {
        
        if (qName.equalsIgnoreCase("node") && parsingNode && !roadName.isEmpty() && roadName != null) {
            double distance = Double.POSITIVE_INFINITY;
            Node chosen = null;
            Data road = model.getClosestRoad(coordinate);
            
            double fromDistance = GeometricCalculator.calculateDistanceBetweenCoordinates(road.getFrom(), coordinate);
            double toDistance = GeometricCalculator.calculateDistanceBetweenCoordinates(road.getTo(), coordinate);
            //chosen = fromDistance < toDistance ? road.getFromNode() : road.getToNode();
//            if (ways.get(roadName) == null) {System.out.println("Could not find road: " + roadName); return;}
//            for (Data way : ways.get(roadName)) {
//                    Node node = way.getFromNode();
//                    double newDistance = GeometricCalculator.calculateDistanceBetweenCoordinates(coordinate, node.getCoordinate());
//                    if (newDistance < distance) {
//                        distance = newDistance;
//                        chosen = node;
//                    }
//
//                    node = way.getToNode();
//                    newDistance = GeometricCalculator.calculateDistanceBetweenCoordinates(coordinate, node.getCoordinate());
//                    if (newDistance < distance) {
//                        distance = newDistance;
//                        chosen = node;
//                    }
//                
//            }
            if (chosen != null) {
                nodes.get(chosen).add(new Address(roadName, zip, housenumber));
                if (++numberOfBounds % 1000 == 0) System.out.println("Bounds so far: " + numberOfBounds);
            }
            zip = housenumber = roadName = EMPTY_STRING;
            //roadName = EMPTY_STRING;
            coordinate = null;
            parsingNode = false;
        }
    }

}
