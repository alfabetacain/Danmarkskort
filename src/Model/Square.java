package Model;

import java.awt.Point;

/**
 * Class used to represent a square
 *
 */
public class Square {

    private final double x, y, length;

    /**
     * Constructs the square with the given x, y coordinate and the given length
     *
     * @param x the x coordinate of the lower left corner of the square
     * @param y the y coordinate of the lower left corner of the square
     * @param length the length of the square
     */
    public Square(double x, double y, double length) {
        this.x = x;
        this.y = y;
        this.length = length;
    }

    public Square(Coordinate coor, double length) {
        this.x = coor.x;
        this.y = coor.y;
        this.length = length;
    }

    /*
     * Makes a square from two coordinates. 
     */
    public Square(Point coor1, Point coor2) {


        double xDiff = Math.abs(coor1.x - coor2.x);
        double yDiff = Math.abs(coor1.y - coor2.y);

        if (xDiff >= yDiff) {
            this.length = xDiff;

            System.out.println(coor1);
            System.out.println(coor2);
            
            this.x = Math.min(coor1.x, coor2.x);
            this.y = Math.min(coor1.y, coor2.y) - (xDiff / 2 - yDiff / 2);
            System.out.println("this.x&y = "+ this.x+this.y);
        } else {
            this.length = yDiff;

            
            System.out.println(coor1);
            System.out.println(coor2);
            
            
            this.x = Math.min(coor1.x, coor2.x) - (yDiff / 2 - xDiff / 2);
            this.y = Math.min(coor1.y, coor2.y);
            
            System.out.println("this.x&y = "+ this.x+this.y);
        }

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return ("Square: " + "(" + x + "," + y + ") length: " + length);
    }
}
