/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package misc;

import java.util.HashSet;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author christian
 */
public class OSMNodeLoader extends DefaultHandler {
    
    private HashSet<String> nodes;
    private HashSet<String> tempNodes;
    private HashSet<String> ways;
    private String tempWay;
    private boolean parsingWay;
    
    @Override
    public void startDocument() throws SAXException
    {
        nodes = new HashSet<>();
        tempNodes = new HashSet<>();
        ways = new HashSet<>();
        tempWay = null;
        parsingWay = false;
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("way"))
        {
            parsingWay = true;
            tempWay = attributes.getValue("id");
        }
        else if (parsingWay)
        {
            if (qName.equals("nd"))
            {
                tempNodes.add(attributes.getValue("ref"));
            }
            else if (qName.equalsIgnoreCase("tag"))
            {
                if (attributes.getValue("k").equalsIgnoreCase("highway") ||
                        attributes.getValue("v").equalsIgnoreCase("coastline"))
                {
                    nodes.addAll(tempNodes);
                    tempNodes.clear();
                    ways.add(tempWay);
                    tempWay = null;
                }
            }
        }
    }
    
    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {
        if (qName.equalsIgnoreCase("way"))
        {
            parsingWay = false;
            tempNodes.clear();
            tempWay = null;
        }
    }
    
    public HashSet<String> getNodes()
    {
        return nodes;
    }
    
    public HashSet<String> getWays()
    {
        return ways;
    }
}
