
package Model;

public class InvalidAddressException extends Exception
{
    public InvalidAddressException(String message)
    {
        super(message);
    }
}
