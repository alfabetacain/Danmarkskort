package misc;

import Model.Data;
import Model.OSMLoader;
import Model.RoadType;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * 
 */
public class ReadingTester {

    public static void main(String[] args) {
        double previous = 0;

        double[] times = new double[3];
        double average = 0;
        System.out.println("Test: Serialized data with RAF & Buffer (65536)");
        for (int i = 0; i < 3; i++) {
            try {
                previous = System.currentTimeMillis();
                RandomAccessFile raf = new RandomAccessFile("data/serialEdges.ser", "r");
                FileInputStream fileIn = new FileInputStream(raf.getFD());
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fileIn, 65536));
                Map<RoadType, List<Data>> map = (EnumMap<RoadType, List<Data>>) in.readObject();
                times[i] = ((System.currentTimeMillis() - previous) / 1000) / 1000;
                System.out.println("Run " + i + " - " + times[i]);
                average += times[i];
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                times[i] = 60000;
            }
        }
        average = average / 3;
        System.out.println("Average: " + average);
        
        System.out.println("Test: Serialized data with RAF & Buffer (128*1024)");
        average = 0;
        for (int i = 0; i < 3; i++) {
            try {
                previous = System.currentTimeMillis();
                RandomAccessFile raf = new RandomAccessFile("data/serialEdges.ser", "r");
                FileInputStream fileIn = new FileInputStream(raf.getFD());
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fileIn, 128*1024));
                Map<RoadType, List<Data>> map = (EnumMap<RoadType, List<Data>>) in.readObject();
                times[i] = (System.currentTimeMillis() - previous) / 1000;
                System.out.println("Run " + i + " - " + times[i]);
                average += times[i];
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                times[i] = 60000;
            }
        }
        average = average / 3;
        System.out.println("Average: " + average);

        System.out.println("Test: Serialized data with Buffer (65536)");
        average = 0;
        for (int i = 0; i < 3; i++) {
            try {
                previous = System.currentTimeMillis();
                FileInputStream fileIn = new FileInputStream("data/serialEdges.ser");
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fileIn, 65536));
                Map<RoadType, List<Data>> map = (EnumMap<RoadType, List<Data>>) in.readObject();
                times[i] = (System.currentTimeMillis() - previous) / 1000;
                System.out.println("Run " + i + " - " + times[i]);
                average += times[i];
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                times[i] = 60000;
            }
        }
        average = average / 3;
        System.out.println("Average: " + average);

        System.out.println("Test: Serialized data with buffer (128 * 1024)");
        average = 0;
        for (int i = 0; i < 3; i++) {
            try {
                previous = System.currentTimeMillis();
                FileInputStream fileIn = new FileInputStream("data/serialEdges.ser");
                ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fileIn, 128 * 1024));
                Map<RoadType, List<Data>> map = (EnumMap<RoadType, List<Data>>) in.readObject();
                times[i] = (System.currentTimeMillis() - previous) / 1000;
                System.out.println("Run " + i + " - " + times[i]);
                average += times[i];
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                times[i] = 60;
            }
        }
        average = average / 3;
        System.out.println("Average: " + average);

        System.out.println("Test: OSMLoader");
        average = 0;
        for (int i = 0; i < 3; i++) {
            previous = System.currentTimeMillis();
            OSMLoader loader = new OSMLoader();
            times[i] = (System.currentTimeMillis() - previous) / 1000;
            System.out.println("Run " + i + " - " + times[i]);
            average += times[i];
        }
        average = average / 3;
        System.out.println("Average: " + average);

        System.out.println("Test: Purely reading osm file with buff (65536)");
        average = 0;
        for (int i = 0; i < 3; i++) {
            try {
                previous = System.currentTimeMillis();
                FileReader fileIn = new FileReader("data/OSMUTM-Final.osm");
                BufferedReader buffIn = new BufferedReader(fileIn, 64*1024);
                String line;
                while ((line = buffIn.readLine()) != null);
                times[i] = (System.currentTimeMillis() - previous) / 1000;
                System.out.println("Run " + i + " - " + times[i]);
                average += times[i];
            } catch (IOException e) {
                e.printStackTrace();
                times[i] = 60000;
            }
        }
        average = average / 3;
        System.out.println("Average: " + average);
    }
}
