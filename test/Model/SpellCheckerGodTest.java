package Model;

public class SpellCheckerGodTest 
{
    Model M;
    SpellChecker SC;
    boolean B = true;

    public SpellCheckerGodTest()
    {
        M = new Model(LoaderType.KRAKLOADER,true);
        SC = new SpellChecker(2, M.getAllRoadNames());
        testAlle();
    }
    
    public SpellCheckerGodTest(Model _M)
    {
        M = _M;
        SC = new SpellChecker(2, M.getAllRoadNames());
        testAlle();
    }
    
    private void testAlle()
    {
        //systems tests
        
        //tests af staveratninges begransninger
        System.out.println("Man kan finde vejen Engbovej Rødovre:");
        enkelTest("med den fulde navn: ",                       "Engbovej Rødovre",     "Engbovej Rødovre");
        enkelTest("med den fulde navn unden mellemrum: ",       "EngbovejRødovre",     "Engbovej Rødovre");
        enkelTest("med den fulde navn med små bukstaver: ",     "engbovej rødovre",     "Engbovej Rødovre");
        enkelTest("hvis der mangler nogle bukstaver i enden: ", "Engbovej Rød",         "Engbovej Rødovre");
         
        //forkerte bukstaver: e != æ ...
        enkelTest("med et bukstav der er forkert: ",            "Engbowej Rødovre",     "Engbovej Rødovre");
        enkelTest("med 2 bukstave der er forkerte: ",           "Engbowej Rødowre",     "Engbovej Rødovre");
        enkelTest("med 3 bukstave der er forkerte: ",           "Engbowej Rødowræ",     "Engbovej Rødovre");
         
        //forkerte bukstaver i starten
        enkelTest("det første bukstav er forkert: ",            "Ængbovej Rødovre",     "Engbovej Rødovre");
        enkelTest("det andet bukstav er forkert: ",             "Emgbovej Rødovre",     "Engbovej Rødovre");
        enkelTest("det tradje bukstav er forkert: ",            "Enqbovej Rødovre",     "Engbovej Rødovre");
         
        //manglene bukstaver
        enkelTest("der mangler et buktstav: ",                  "Engbvej Rødovre",      "Engbovej Rødovre");
        enkelTest("der mangler 2 buktstaver: ",                 "Engbvej Rødvre",       "Engbovej Rødovre");
        enkelTest("der mangler 3 buktstaver: ",                 "Engbvej Rdvre",        "Engbovej Rødovre");
        
        //ekstra bukstaver
        enkelTest("der er et bukstav for maget: ",              "Engboovej Rødovre",    "Engbovej Rødovre");
        enkelTest("der er 2 bukstaver for maget: ",             "Engboovej Rødoovre",   "Engbovej Rødovre");
        enkelTest("der er 3 bukstaver for maget: ",             "Engboovej Rødoovrre",  "Engbovej Rødovre");
        
        //ekstra bukstaver der er forkerte
        enkelTest("der er et forkert bukstav for maget: ",      "Engboåvej Rødovre",    "Engbovej Rødovre");
        enkelTest("der er 2 forkerte bukstaver for maget: ",    "Engboåvej Rødoåvre",   "Engbovej Rødovre");
        enkelTest("der er 3 forkerte bukstaver for maget: ",    "Engboåvej Rødoåvråe",  "Engbovej Rødovre");
        
        //bukstaver byttet om
        enkelTest("der er en gang bukstaver byttet om: ",       "Engbvoej Rødovre",     "Engbovej Rødovre");
        enkelTest("der er 2 gange bukstaver byttet om: ",       "Engbvoej Rdøovre",     "Engbovej Rødovre");
        enkelTest("der er 3 gange bukstaver byttet om: ",       "Engbvoej Rdøvore",     "Engbovej Rødovre");
        
        //forkert by
        enkelTest("uden by: ",                                  "Engbovej",            "Engbovej Rødovre");
        enkelTest("uden by med mellemrum: ",                    "Engbovej ",           "Engbovej Rødovre");
        enkelTest("med en forkert by: ",                        "Engbovej Malling",    "Engbovej Rødovre");
        enkelTest("med en forkert by uden mellemrum: ",         "EngbovejMalling",     "Engbovej Rødovre");
        
        //lærens test - Rued Langgaards Vej
        enkelTest("undervierenes test: ",                       "Ruud Langrds vej",    "Rued Langgaards Vej");
        
    }
    
    private void enkelTest(String text, String test, String raktig)
    {
        String[] stringArray = SC.findClosest(test);
        System.out.println("\t" + getIfInQue(raktig, stringArray) + "\t" + stringArray[0].equals(raktig) + "\t" + text);
        
        if(!getIfInQue(raktig, stringArray) || !stringArray[0].equals(raktig))
            B = false;
    }
    
    private boolean getIfInQue(String s, String[] in)
    {
        for(String string : in)
        {
            if(s.equals(string))
                return true;
        }
        return false;
    }
    
    public boolean getResult()
    {
        return B;
    }
    
}
