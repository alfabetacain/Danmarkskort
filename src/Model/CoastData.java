package Model;

import java.io.Serializable;
import java.util.List;

/**
 * Data class which represents coast lines. Has type COAST
 */
public class CoastData implements Data, Serializable {

    private static final long serialVersionUID = 1;
    private final INode from;
    private final INode to;
    private static final RoadType type = RoadType.COAST;
    private static final String name = "Kyst";
    private final double centerX, centerY;

    /**
     * Initializes the CoastData object. Takes the start point and end point of
     * the line/edge
     *
     * @param startX the x coordinate of the starting point
     * @param startY the y coordinate of the starting point
     * @param endX the x coordinate of the ending point
     * @param endY the y coordinate of the ending point
     */
    public CoastData(double startX, double startY, double endX, double endY) {
        this(new Coordinate(startX, startY), new Coordinate(endX, endY));
    }

    public CoastData(INode from, INode to) {
        this.from = from;
        this.to = to;
        Coordinate fTemp = from.getCoordinate();
        Coordinate tTemp = to.getCoordinate();
        centerX = fTemp.x < tTemp.x
                ? fTemp.x + (tTemp.x - fTemp.x) * 0.5
                : tTemp.x + (fTemp.x - tTemp.x) * 0.5;
        centerY = fTemp.y < tTemp.y
                ? fTemp.y + (tTemp.y - fTemp.y) * 0.5
                : tTemp.y + (fTemp.y - tTemp.y) * 0.5;
    }

    public CoastData(Coordinate start, Coordinate end) {
        this(new Node(-1, start), new Node(-1, end));
    }

    @Override
    public RoadType getType() {
        return type;
    }

    @Override
    public String getRoadName() {
        return name;
    }

    @Override
    public String toString() {
        return "Coast " + super.toString();
    }

    @Override
    public double getRotation() {
        return 0;
    }

    @Override
    public int getZipCode() {
        return 0;
    }

    @Override
    public Coordinate getCoordFromNumber(int fromNumber) {
        return getCenter();
    }

    @Override
    public boolean hasHouseNumber(int number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double weight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Coordinate getFrom() {
        return from.getCoordinate();
    }

    @Override
    public Coordinate getTo() {
        return to.getCoordinate();
    }

    @Override
    public Coordinate getCenter() {
        return new Coordinate(centerX, centerY);
    }

    @Override
    public INode getFromNode() {
        return from;
    }

    @Override
    public INode getToNode() {
        return to;
    }

    @Override
    public double getLength() {
        return GeometricCalculator.calculateDistanceBetweenCoordinates(from.getCoordinate(), to.getCoordinate());
    }

    @Override
    public void add(Data d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
