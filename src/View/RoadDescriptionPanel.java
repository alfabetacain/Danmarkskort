package View;

import Model.Data;
import Model.GeometricCalculator;
import Model.Road;
import Model.RoadType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;

/**
 * A class to act as a JPanel, displaying the road description for a given route
 * navigation.
 */
public class RoadDescriptionPanel extends JComponent {

    private String fromAddress, toAddress;
    private JPanel descriptionPanel;
    private JList list;
    private JPanel listPanel;
    private JScrollPane pane;
    private String[] routeDescription;
    private List<Data> route;
    private Font nonHeadLineFont = new Font("Meiryo", 0, 11);
    private Font listFont = new Font("Meiryo", 0, 12);
    private final Dimension panelDimension = new Dimension(296, 566);
    private final DecimalFormat format = new DecimalFormat("0.00");
    private final boolean isWalking;

    public RoadDescriptionPanel(String from, String to, List<Data> route, ListSelectionListener listener, boolean isWalking) {
        fromAddress = from;
        toAddress = to;
        this.route = route;

        this.isWalking = isWalking;

        //returns a String[] with where every turn along the route
        List<String> tempList = descriptionOfRoute(route);

        descriptionPanel = createDescriptionPanel(tempList.get(tempList.size() - 1));
        tempList.remove(tempList.size() - 1);

        routeDescription = tempList.toArray(new String[tempList.size()]);

        //make a list containing the route
        list = new JList(routeDescription);
        list.setFixedCellWidth(280);
        list.setVisibleRowCount(10);
        list.setFont(nonHeadLineFont);
        list.addListSelectionListener(listener);

        //create pane to contain the list
        pane = new JScrollPane(list);
        pane.setPreferredSize(new Dimension(284, 500));
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        //create panel to contain pane with list
        listPanel = new JPanel();
        listPanel.add(pane);

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(descriptionPanel);
        this.add(listPanel);
    }

    private JPanel createDescriptionPanel(String text) {
        JPanel panel = new JPanel();
        JLabel firstPartOfDesc = new JLabel("Rute fra " + fromAddress + " til " + toAddress);
        firstPartOfDesc.setToolTipText("Rute fra " + fromAddress + " til " + toAddress);
        JLabel secondPartOfDesc = new JLabel(text);
        secondPartOfDesc.setToolTipText(text);
        firstPartOfDesc.setHorizontalAlignment(JLabel.CENTER);
        secondPartOfDesc.setHorizontalAlignment(JLabel.CENTER);
        secondPartOfDesc.setFont(nonHeadLineFont);

        System.out.println("Rute fra " + fromAddress + " til " + toAddress + ". " + text);
        panel.setLayout(new GridLayout(2, 1));
        panel.add(firstPartOfDesc);
        panel.add(secondPartOfDesc);
        return panel;
    }

    //Returns an String array with the desciption of the route.
    //last entry is the Distance and driving time description.
    private List<String> descriptionOfRoute(List<Data> route) {

        ArrayList<String> tempList = new ArrayList<>();

        String startText = " Start på ";
        String leftText = " ↰ på ";
        String straightTxt = " ↑ af ";
        String rightTxt = " ↱ på ";
        String ferryStartTxt = " Tag færgen ";
        String ferryWhileTxt = " Nyd turen og husk madpakke!";
        String ferryEndTxt = " Fortsæt til lands ad ";
        String roundaboutTxt = " ⟲ ";
        String roundaboutCheck = "Rundkørsel";

        String endTxt = " ankomst på ";

        double lengthOfRoute = 0;
        //in hours:
        double timeOfRoute = 0;

        int j = 0;
        while (j < route.size()) {
            Data e = route.get(j);
            double lengthOfRoad = e.getLength();
            //if first
            if (j == 0) {
                tempList.add(startText + e.getRoadName());
            } //if last
            else if (j == route.size() - 1) {
                tempList.add(endTxt + e.getRoadName());
            } else {
                Data previous = route.get(j - 1);
                Data next = e;

                if (previous instanceof Road) {
                    previous = previous.getEdges().get(previous.getEdges().size() - 1);
                }
                if (next instanceof Road) {
                    next = next.getEdges().get(0);
                }

                RoadType prevType = previous.getType();
                RoadType nextType = next.getType();

                if (nextType == RoadType.FERRYROUTE || prevType == RoadType.FERRYROUTE) {
                    if (prevType != RoadType.FERRYROUTE) {
                        tempList.add(ferryStartTxt + next.getRoadName());
                    } else if (nextType != RoadType.FERRYROUTE) {
                        tempList.add(ferryEndTxt + next.getRoadName());
                    } else { // out the the ferry route - turn.
                        tempList.add(ferryWhileTxt);
                    }
                } else if (next.getRoadName().equals(roundaboutCheck)) {
                    if (previous.getRoadName().equals(roundaboutCheck)) {
                        tempList.add(null);
                    } else {
                        tempList.add(roundaboutTxt);
                    }
                } else {
                    double angle = GeometricCalculator.getAngle(previous, next);
                    if (angle < 130) {

                        tempList.add(rightTxt + next.getRoadName() + ", kør " + writeDistance(lengthOfRoad));
                    } else if (angle > 230) {
                        tempList.add(leftText + next.getRoadName() + ", kør " + writeDistance(lengthOfRoad));
                    } else {
                        tempList.add(straightTxt + next.getRoadName() + ", kør " + writeDistance(lengthOfRoad));
                    }
                }
            }
            lengthOfRoute += lengthOfRoad;
            if (!isWalking) {
                timeOfRoute += e.weight();
            } else {
                timeOfRoute += e.getLength() / 1000 / 7;
            }
            j++;
        }
        tempList.add("Afstand: " + writeDistance(lengthOfRoute)
                + "  Køretid ca: " + writeTime(timeOfRoute));
        return tempList;
    }

    private String writeDistance(double dist) {
        if (dist < 950) {
            return (int) dist + " m.";
        }
        return (format.format((dist / 1000))) + " km.";
    }

    //Takes the time in hours as a parameter.
    private String writeTime(double timeOfRoute) {
        if (timeOfRoute >= 1) {
            return (int) timeOfRoute + " t. " + (int) ((timeOfRoute % 1) * 60) + " min.";
        }
        return (int) ((timeOfRoute % 1) * 60) + " min.";
    }

    @Override
    public Dimension getPreferredSize() {
        return panelDimension;
    }

    public int getSelectedItem() {
        return list.getSelectedIndex();
    }
}
