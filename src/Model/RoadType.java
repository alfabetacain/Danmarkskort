package Model;

import java.io.Serializable;

public enum RoadType implements Serializable {
    REGULARROAD(50), SUBROAD(40), HIGHWAY(130), EXPRESSWAY(80), PATH(7), COAST(0), MAINROAD(50), UNKNOWN(0),
    RIVER(0), POLITICALBORDER(0), FERRYROUTE(45);
    
    private final int speedLimit;
    
    RoadType(int speedLimit)
    {
        this.speedLimit = (int) (speedLimit * 1.15);
    }
    boolean isNotRoad(RoadType type)
    {
        return (type == RoadType.RIVER || type == RoadType.POLITICALBORDER || type == RoadType.COAST);
    }
    public int getSpeedLimit()
    {
        return speedLimit;
    }
}
