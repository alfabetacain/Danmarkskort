package Model;

import View.LoadBar;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parse Krak data files (kdv_node_unload.txt, kdv_unload.txt).
 *
 * Customize to your needs by overriding processNode and processEdge. See
 * example in main.
 *
 * Original author Peter Tiedemann petert@itu.dk; updates (2014) by Søren
 * Debois, debois@itu.dk
 */
public final class KrakLoader
{

    public static final HashMap<Integer, INode> nodes = new HashMap<>();
    public final Map<RoadType, List<Data>> edges;
   // public final ArrayList<Data> edges = new ArrayList<>();
    private final String dir;

    public KrakLoader()
    {
        dir = "data/";
        edges = new EnumMap<>(RoadType.class);
        for (RoadType type : RoadType.values())
            edges.put(type, new ArrayList<Data>());
        try
        {
            
            load(dir + "kdv_node_unload.txt",
                    dir + "kdv_unload.txt");

            // Check the results.
            System.out.printf("Loaded %d nodes, %d edges\n",
                    nodes.size(), edges.size());
            MemoryMXBean mxbean = ManagementFactory.getMemoryMXBean();
            System.out.printf("Heap memory usage: %d MB%n",
                    mxbean.getHeapMemoryUsage().getUsed() / (1000000));
        }
        catch (IOException ex)
        {
            Logger.getLogger(KrakLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void processNode(Node nd)
    {
        nodes.put(nd.getID(), nd);
    }

    public void processEdge(Edge ed)
    {
        edges.get(ed.getType()).add(ed);
    }

    /**
     * Load krak-data from given files, invoking processNode and processEdge
     * once for each node- and edge- specification in the input file,
     * respectively.
     *
     * @param nodeFile
     * @param edgeFile
     * @return
     * @throws IOException if there is a problem reading data or the files dont
     * exist
     */
    public void load(String nodeFile, String edgeFile) throws IOException
    {
        /* Nodes. */
        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(new FileInputStream(nodeFile), "UTF-8"), 128*1024);
        br.readLine(); // First line is column names, not data.
        
        
        LoadBar loadingProgress = LoadBar.getInstanceOf();
        
        String line;
        
        int i = 0;
        //the total number of lines
        int s = 1536716;
        while ((line = br.readLine()) != null)
        {
            loadingProgress.setValue(i++*100/s);
            DataLine dl = new DataLine(line);
            dl.getInt();
            dl.getInt();
            int id = dl.getInt();
            double x = dl.getDouble();
            double y = dl.getDouble();
            processNode(new Node(id, x, y));
        }
        br.close();

        /* Edges. */
        br = new BufferedReader(new InputStreamReader(new FileInputStream(edgeFile), "UTF-8"), 128*1024);
        br.readLine(); // Again, first line is column names, not data.

        while ((line = br.readLine()) != null)
        {
            loadingProgress.setValue(i++*100/s);
            processEdge(new Edge(line));
        }
        br.close();

        //Deletes the nodes after use
        //nodes.clear();

        DataLine.resetInterner();
        System.gc();

        //loads the coast lines
        BufferedReader reader = new BufferedReader(new FileReader(dir + "correctedCoastLine.txt"));

        String[] array;

        while ((line = reader.readLine()) != null)
        {
            loadingProgress.setValue(i++*100/s);
            array = line.split(",");
            edges.get(RoadType.COAST).add(new CoastData(Double.parseDouble(array[0]),
                    Double.parseDouble(array[1]),
                    Double.parseDouble(array[2]),
                    Double.parseDouble(array[3]))
            );
        }
        
        //loads political borders
        reader = new BufferedReader(new FileReader(dir + "correctedPoliticalBorder.txt"));

        while ((line = reader.readLine()) != null)
        {
            loadingProgress.setValue(i++*100/s);
            array = line.split(",");
            edges.get(RoadType.POLITICALBORDER).add(new PoliticalData(Double.parseDouble(array[0]),
                    Double.parseDouble(array[1]),
                    Double.parseDouble(array[2]),
                    Double.parseDouble(array[3]))
            );
        }

        reader.close();
        
        //loads rivers
        reader = new BufferedReader(new FileReader(dir + "correctedRiverLine.txt"));

        while ((line = reader.readLine()) != null)
        {
            loadingProgress.setValue(i++*100/s);
            array = line.split(",");
            edges.get(RoadType.RIVER).add(new RiverData(Double.parseDouble(array[0]),
                    Double.parseDouble(array[1]),
                    Double.parseDouble(array[2]),
                    Double.parseDouble(array[3]))
            );
        }

        reader.close();
    }
    
    public Collection<INode> getNodes()
    {
        return nodes.values();
    }

}
