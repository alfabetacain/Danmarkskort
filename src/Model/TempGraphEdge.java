/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.util.List;

/**
 * used when the Graph needs an edge that doesn't exist, e.g. when calculating
 * a route from or to a point on an Edge, then the old edge is split up into a
 * temporary Edge. 
 */
public class TempGraphEdge implements Data {

    private final INode from;
    private final INode to;
    private final String roadName;
    private final int cityName;
    private final double length;
    private RoadType type;
    private final double centerX;
    private final double centerY;
    
    public TempGraphEdge(INode from, INode to, Data edge)
    {
        this.from = from;
        this.to = to;
        this.roadName = edge.getRoadName();
        this.cityName = edge.getZipCode();
        length = GeometricCalculator.calculateDistanceBetweenCoordinates(
                from.getCoordinate(), to.getCoordinate());
        type = edge.getType();
        
            if (from.getCoordinate().x > to.getCoordinate().x) {
                centerX = (from.getCoordinate().x - to.getCoordinate().x) * 0.5 + to.getCoordinate().x;
            } else {
                centerX = (to.getCoordinate().x - from.getCoordinate().x) * 0.5 + from.getCoordinate().x;
            }
            if (from.getCoordinate().y > to.getCoordinate().y) {
                centerY = (from.getCoordinate().y - to.getCoordinate().y) * 0.5 + to.getCoordinate().y;
            } else {
                centerY = (to.getCoordinate().y - from.getCoordinate().y) * 0.5 + from.getCoordinate().y;
            }
    }
    @Override
    public Coordinate getFrom() {
        return from.getCoordinate();
        
    }

    @Override
    public Coordinate getTo() {
        return to.getCoordinate();
    }

    @Override
    public Coordinate getCenter() {
    
        return new Coordinate(centerX, centerY);
    }

    @Override
    public RoadType getType() {
        return type;
    }

    @Override
    public String getRoadName() {
        return roadName;
    }

    @Override
    public double weight() {
        return getLength() / 1000 / type.getSpeedLimit();
    }

    @Override
    public INode getFromNode() {
        return from;
    }

    @Override
    public INode getToNode() {
        return to;
    }

    @Override
    public double getRotation() {
        return Math.toDegrees(Math.atan2(from.getCoordinate().x -to.getCoordinate().x, from.getCoordinate().y -to.getCoordinate().y));
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public int getZipCode() {
        return cityName;
    }

    @Override
    public Coordinate getCoordFromNumber(int fromNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasHouseNumber(int number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Node getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void add(Data d)
    {
        throw new UnsupportedOperationException("Not supported in TempGraphEdge");
    }

    @Override
    public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public int getCoordinateAsHouseNumber(Coordinate coord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
