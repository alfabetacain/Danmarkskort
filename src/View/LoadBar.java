package View;

import javax.swing.JProgressBar;

/**
 * A JProgressBar, but with a getInstanceOf() method incorporated.
 */
public class LoadBar extends JProgressBar {
    private static LoadBar instance;
    
    public static LoadBar getInstanceOf() {
        if (instance==null) {
            instance = new LoadBar();
        }
            return instance;
    }
}
