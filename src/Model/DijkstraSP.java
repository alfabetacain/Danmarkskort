package Model;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * ***********************************************************************
 * Compilation: javac DijkstraSP.java Execution: java DijkstraSP input.txt s
 * Dependencies: EdgeWeightedDigraph.java IndexMinPQ.java Stack.java
 * DirectedEdge.java Data files: http://algs4.cs.princeton.edu/44sp/tinyEWD.txt
 * http://algs4.cs.princeton.edu/44sp/mediumEWD.txt
 * http://algs4.cs.princeton.edu/44sp/largeEWD.txt
 *
 * Dijkstra's algorithm. Computes the shortest path tree. Assumes all weights
 * are nonnegative.
 *
 * % java DijkstraSP tinyEWD.txt 0 0 to 0 (0.00) 0 to 1 (1.05) 0->4 0.38 4->5
 * 0.35 5->1 0.32 0 to 2 (0.26) 0->2 0.26 0 to 3 (0.99) 0->2 0.26 2->7 0.34 7->3
 * 0.39 0 to 4 (0.38) 0->4 0.38 0 to 5 (0.73) 0->4 0.38 4->5 0.35 0 to 6 (1.51)
 * 0->2 0.26 2->7 0.34 7->3 0.39 3->6 0.52 0 to 7 (0.60) 0->2 0.26 2->7 0.34
 *
 * % java DijkstraSP mediumEWD.txt 0 0 to 0 (0.00) 0 to 1 (0.71) 0->44 0.06
 * 44->93 0.07 ... 107->1 0.07 0 to 2 (0.65) 0->44 0.06 44->231 0.10 ... 42->2
 * 0.11 0 to 3 (0.46) 0->97 0.08 97->248 0.09 ... 45->3 0.12 0 to 4 (0.42) 0->44
 * 0.06 44->93 0.07 ... 77->4 0.11 ...
 *
 ************************************************************************
 */
/**
 * The <tt>DijkstraSP</tt> class represents a data type for solving the
 * single-source shortest paths problem in edge-weighted digraphs where the edge
 * weights are nonnegative.
 * <p>
 * This implementation uses Dijkstra's algorithm with a binary heap. The
 * constructor takes time proportional to <em>E</em> log <em>V</em>, where
 * <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 * Afterwards, the <tt>distTo()</tt> and <tt>hasPathTo()</tt> methods take
 * constant time and the <tt>pathTo()</tt> method takes time proportional to the
 * number of edges in the shortest path returned.
 * <p>
 * For additional documentation, see <a href="/algs4/44sp">Section 4.4</a> of
 * <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 * @author Robert Sedgewick
 * @author Kevin Wayne
 */
public class DijkstraSP {

    private final double[] distTo;
    private final Data[] edgeTo;
    private IndexMinPQ<Double> pq;    // priority queue of vertices

    /**
     * Computes a shortest paths tree from <tt>s</tt> to every other vertex in
     * the edge-weighted digraph <tt>G</tt>.
     *
     * @param G the edge-weighted digraph
     * @param s the source vertex
     * @throws IllegalArgumentException if an edge weight is negative
     * @throws IllegalArgumentException unless 0 &le; <tt>s</tt> &le; <tt>V</tt>
     * - 1
     */
    public DijkstraSP(Graph G, int s, int t, boolean isWalking) {

        distTo = new double[G.V()];
        edgeTo = new Data[G.V()];
        for (int v = 0; v < G.V(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
//        distTo.put(s, 0.0);
        distTo[s] = 0.0;
        Set<RoadType> disallowedTypes = new HashSet<>();
        if (isWalking) {
            disallowedTypes.add(RoadType.HIGHWAY);
        } else {
            disallowedTypes.add(RoadType.PATH);
        }
        pq = new IndexMinPQ<>(G.V());
        pq.insert(s, distTo[s]);
//        System.out.println("After null");
        if (isWalking) {
            while (!pq.isEmpty()) {
                int v = pq.delMin();
                if (v == t) {
                    return;
                }
                for (Data data : G.adj(v)) {
                    if (!disallowedTypes.contains(data.getType())) {
                        relaxWalking(data, v, t);
                    }
                }
            }
        } else {
            while (!pq.isEmpty()) {
                int v = pq.delMin();
                if (v == t) {
                    System.out.println("Found!");
                    return;
                }
                for (Data data : G.adj(v)) {
                    if (!disallowedTypes.contains(data.getType())) {
                        relax(data, v, t);
                    }
                }
            }
        }
        pq = null;
    }

    // relax edge e and update pq if changed
    private void relax(Data e, int s, int t) {
        int v, w;
        v = e.getFromNode().getID();
        w = e.getToNode().getID();
        if (distTo[w] > distTo[v] + e.weight()) {
            distTo[w] = distTo[v] + e.weight(); //.put(w, distTo.get(v) + e.weight());
            edgeTo[w] = e; //.put(w, e);
            if (pq.contains(w)) {
                pq.decreaseKey((w), distTo[w] + h(w, t));
            } else {
                pq.insert(w, distTo[w] + h(w, t));
            }
        }
    }

    private void relaxWalking(Data e, int s, int t) {
        int v, w;
        v = e.getFromNode().getID();
        w = e.getToNode().getID();
        if (distTo[w] > distTo[v] + e.getLength()) {
            distTo[w] = distTo[v] + e.getLength(); //.put(w, distTo.get(v) + e.weight());
            edgeTo[w] = e; //.put(w, e);
            if (pq.contains(w)) {
                pq.decreaseKey((w), distTo[w] + h(w, t));
            } else {
                pq.insert(w, distTo[w] + h(w, t));
            }
        }
    }

    /**
     * Returns the length of a shortest path from the source vertex <tt>s</tt>
     * to vertex <tt>v</tt>.
     *
     * @param v the destination vertex
     * @return the length of a shortest path from the source vertex <tt>s</tt>
     * to vertex <tt>v</tt>;
     * <tt>Double.POSITIVE_INFINITY</tt> if no such path
     */
    public double distTo(int v) {
        return distTo[v];
    }

    /**
     * Is there a path from the source vertex <tt>s</tt> to vertex <tt>v</tt>?
     *
     * @param v the destination vertex
     * @return <tt>true</tt> if there is a path from the source vertex
     * <tt>s</tt> to vertex <tt>v</tt>, and <tt>false</tt> otherwise
     */
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * Returns a shortest path from the source vertex <tt>s</tt> to vertex
     * <tt>v</tt>.
     *
     * @param v the destination vertex
     * @return a shortest path from the source vertex <tt>s</tt> to vertex
     * <tt>v</tt>
     * as an iterable of edges, and <tt>null</tt> if no such path
     */
    public Stack<Data> pathTo(int v) {
        if (!hasPathTo(v)) {
            return null;
        }
        Stack<Data> path = new Stack<>();
        for (Data e = edgeTo[v]; e != null; e = edgeTo[(e.getFromNode().getID())]) {
            path.push(e);
        }
        return path;
    }

    private double h(int i, int t) {
        Coordinate start = NodeCollection.getNode(i).getCoordinate();
        Coordinate end = NodeCollection.getNode(t).getCoordinate();
        return (GeometricCalculator.
                calculateDistanceBetweenCoordinates(start, end) / 1000) / RoadType.HIGHWAY.getSpeedLimit();
    }
}
