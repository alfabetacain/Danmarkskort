package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Acts as the northern panel in the program GUI structure, containing a searching
 * panel or navigation panel structure.
 */
public class SearchAndNavigationPanel extends JPanel {
    private boolean atNavigationPage = false;
    private Dimension buttonDimension = new Dimension(180, 20);
    private Dimension textFieldDimension = new Dimension(300, 22);
    private Font textFont = new Font("Meiryo", 0, 12);
    private Color lightGray = new Color(245, 245, 245);
    private final ActionListener ListenerForConfirmButtonAndSearchFields;
    private final MouseListener changeToCarNav;
    private final MouseListener changeToOnFootNav;
    private JTextField navigateFrom;
    private JTextField navigateTo;
    private JTextField searchTextField;
    private boolean listenAddedSearch = false;
    private boolean listenAddedToNaviFields = false;
    File selectedCarImg = new File("data/car-20selected.png");
    File deselectedCarImg = new File("data/car-20.png");
    File selectedManImg = new File("data/oldman-20selected.png");
    File deselectedManImg = new File("data/oldman-20.png");
    private boolean carNavSelected = true;
    private boolean footNavSelected = false;
    private ImageIcon manImageIcon = null;
    private ImageIcon carImageIcon = null;
    
    public SearchAndNavigationPanel(ActionListener ListenerForConfirmButtonAndSearchFields, MouseListener changeToCarNav,
            MouseListener changeToOnFootNav)
    {
        this.ListenerForConfirmButtonAndSearchFields = ListenerForConfirmButtonAndSearchFields;
        this.changeToCarNav = changeToCarNav;
        this.changeToOnFootNav = changeToOnFootNav;
        
        setBackground(lightGray);
        
        setUpSearchPanel();
    }
    
    public boolean atNavigationPage()
    {
        return atNavigationPage;
    }
    
    public boolean textInSearchTextField()
    {
        if(searchTextField.getText().equals("Søg...") || searchTextField.getText().equals(""))
            return false;
        else return true;
    }
    
    public boolean textInNavigateFrom()
    {
        if(navigateFrom.getText().equals("Navigér fra...") || navigateFrom.getText().equals(""))
            return false;
        else return true;
    }
    
    public boolean textInNavigateTo()
    {
        if(navigateTo.getText().equals("Navigér til...") || navigateTo.getText().equals(""))
            return false;
        else return true;
    }
    
    private void setUpNavigationPanel()
    {
        atNavigationPage = true;
        setLayout(new GridLayout(3,1,2,0));
        
        //Button for switching between search modes
        JPanel switchButtonPanel = createButtonsInPanel();
        switchButtonPanel.setBackground(lightGray);
        add(switchButtonPanel);
        
        
        JPanel navigationPanel = new JPanel();
        navigationPanel.setBackground(lightGray);
        //Navigation destination
        navigateFrom = createTextField("Navigér fra...", "Skriv start lokationen på din rejse");
        navigationPanel.add(navigateFrom);
        //Navigation destination - set equal to searchTextField if it contains user input
        if(searchTextField.getText().equals("Søg...") || searchTextField.getText().equals("")) {
            listenAddedToNaviFields = false;
            navigateTo = createTextField("Navigér til...", "Skriv destinationen på din rejse");
        } else {
            navigateTo = searchTextField;
            //navigateTo.setText(searchTextField.getText());
        }
        if(!listenAddedToNaviFields) {
            listenAddedToNaviFields = true;
            navigateTo.addActionListener(ListenerForConfirmButtonAndSearchFields);
            navigateFrom.addActionListener(ListenerForConfirmButtonAndSearchFields);
        }
        navigationPanel.add(navigateTo);
        add(navigationPanel);
        
        //Button for confirmation of search
        JPanel confirmButtonPanel = new JPanel();
        confirmButtonPanel.setBackground(lightGray);
        confirmButtonPanel.add(createConfirmButton("Søg"));
        add(confirmButtonPanel);
    }
    
    private void setUpSearchPanel()
    {
        atNavigationPage = false;
        setLayout(new GridLayout(3,1,2,0));
        
        //Button for switching between search modes
        JPanel switchButtonPanel = createButtonsInPanel();
        switchButtonPanel.setBackground(lightGray);
        add(switchButtonPanel);
        
        JPanel searchPanel = new JPanel();
        searchPanel.setBackground(lightGray);
        //Text field for searching - set equal to navigateTo if it contains user input
        if(searchTextField == null || navigateTo.getText().equals("Navigér til...")) {
            listenAddedSearch = false;
            searchTextField = createTextField("Søg...", "Type the address that you'd like to find");
        } else {
            searchTextField = navigateTo;
            //searchTextField.setText(navigateTo.getText());
        }
        if(!listenAddedSearch) {
            searchTextField.addActionListener(ListenerForConfirmButtonAndSearchFields);
            listenAddedSearch = true;
        }
        searchPanel.add(searchTextField);
        add(searchPanel);
        
        //Button for confirmation of search
        JPanel confirmButtonPanel = new JPanel();
        confirmButtonPanel.setBackground(lightGray);
        confirmButtonPanel.add(createConfirmButton("Søg"));
        add(confirmButtonPanel);
    }
    
    public void goToNavigationPanel()
    {
        removeAll();
        setUpNavigationPanel();
        revalidate();
    }
    
    private void goToSearchPanel()
    {
        removeAll();
        setUpSearchPanel();
        revalidate();
    }
    
    private JTextField createTextField(String ghostText, String tooltip)
    {
        JTextField textField = new JTextField();
        textField.setPreferredSize(textFieldDimension);
        textField.setHorizontalAlignment(JTextField.LEFT);
        textField.setFont(textFont);
        new GhostText(textField, ghostText);
        textField.setToolTipText(tooltip);
        return textField;
    }
    
    private JPanel createButtonsInPanel()
    {
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(createButton("Adressesøgning", new ListenForAddressSearchButton()));
        JLabel[] picLabels = createPictureLabels();
        buttonPanel.add(picLabels[0]);
        buttonPanel.add(picLabels[1]);
        buttonPanel.add(createButton("Rutevejledning", new ListenForNavigationButton()));
        return buttonPanel;
    }
    
    private JLabel[] createPictureLabels()
    {
        JLabel[] picArr = new JLabel[2];
        picArr[0] = createPicLabel(true);
        picArr[1] = createPicLabel(false);
        return picArr;
    }
    
    private JLabel createPicLabel(boolean picWithMan)
    {
        JLabel picLabel;
        if(picWithMan) {
            BufferedImage myPicture = null;
            try {
                if(footNavSelected) {
                    myPicture = ImageIO.read(selectedManImg);
                } else {
                    myPicture = ImageIO.read(deselectedManImg);
                }
            } catch(IOException ex) {
                //Do nothing
            }
            manImageIcon = new ImageIcon(myPicture);
            picLabel = new JLabel(manImageIcon);
            picLabel.addMouseListener(changeToOnFootNav);
        } else {
            BufferedImage myPicture = null;
            try {
                if(carNavSelected) {
                    myPicture = ImageIO.read(selectedCarImg);
                } else {
                    myPicture = ImageIO.read(deselectedCarImg);
                }
            } catch(IOException ex) {
                //Do nothing
            }
            carImageIcon = new ImageIcon(myPicture);
            picLabel = new JLabel(carImageIcon);
            picLabel.addMouseListener(changeToCarNav);
        }
        return picLabel;
    }
    
    public void selectCarNav()
    {
        carNavSelected = true;
        footNavSelected = false;
        try {
            carImageIcon.setImage(ImageIO.read(selectedCarImg));
            manImageIcon.setImage(ImageIO.read(deselectedManImg));
            repaint();
        } catch(IOException ex) {
            //Do nothing
        }
    }
    
    public void selectFootNav()
    {
        carNavSelected = false;
        footNavSelected = true;
        try {
            carImageIcon.setImage(ImageIO.read(deselectedCarImg));
            manImageIcon.setImage(ImageIO.read(selectedManImg));
            repaint();
        } catch(IOException ex) {
            //Do nothing
        }
    }
    
    public boolean isCarNavSelected()
    {
        return carNavSelected;
    }
    
    public boolean isFootNavSelected()
    {
        return footNavSelected;
    }
    
    private JButton createButton(String buttonName, ActionListener listener)
    {
        JButton button = new JButton(buttonName);
        button.setBorder(BorderFactory.createEtchedBorder());
        button.setBorder(BorderFactory.createLineBorder(lightGray, 1));
        button.setFont(textFont);
        button.setPreferredSize(buttonDimension);
        button.addActionListener(listener);
        return button;
    }
    
    private JButton createConfirmButton(String buttonName)
    {
        JButton button = new JButton(buttonName);
        button.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
        button.setFont(textFont);
        button.setPreferredSize(buttonDimension);
        button.addActionListener(ListenerForConfirmButtonAndSearchFields);
        return button;
    }
    
    @Override
    public Dimension getPreferredSize()
    {
        //Pixels chosen as per frame dimension definition
        return new Dimension(634, 85);
    }
    
    //Listener for button activating the singular search panel
    public class ListenForAddressSearchButton implements ActionListener {
        //goes to edit booking window
        @Override
        public void actionPerformed(ActionEvent e) {
            goToSearchPanel();
        }
    }
    
    //Listener for button activating the navigation panel
    public class ListenForNavigationButton implements ActionListener {
        //goes to edit booking window
        @Override
        public void actionPerformed(ActionEvent e) {
            goToNavigationPanel();
        }
    }
    
    public void setSearchTextField(String correctAddress) {
        searchTextField.setText(correctAddress);
    }
    
    public void setFromTextField(String correctAddress) {
        navigateFrom.setForeground(Color.BLACK);
        navigateFrom.setText(correctAddress);
    }
    
    public void setToTextField(String correctAddress) {
        
        navigateTo.setForeground(Color.BLACK);
        navigateTo.setText(correctAddress);
    }
    
    public String getSearchTextField() {
        return searchTextField.getText();
    }

    public String getFromTextField()
    {
        return navigateFrom.getText();
    }
    
    public String getToTextField()
    {
        return navigateTo.getText();
    }
}
