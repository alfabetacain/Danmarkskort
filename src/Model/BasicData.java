package Model;

/**
 * Class for containing basic information needed to implement the data interface
 *
 */
public abstract class BasicData implements Data
{
    private final INode from;
    private final INode to;
    private final double centerX, centerY;

    /**
     * Initializes the BasicData object. Takes the start point and end point of
     * the line/edge
     *
     * @param startX the x coordinate of the starting point
     * @param startY the y coordinate of the starting point
     * @param endX the x coordinate of the ending point
     * @param endY the y coordinate of the ending point
     */
    public BasicData(double startX, double startY, double endX, double endY)
    {
        this(new Node(-1,startX, startY), new Node(-1,endX, endY));
    }
    
    public BasicData(Coordinate start, Coordinate end)
    {
        this(new Node(-1, start), new Node(-1, end));
    }
    
    public BasicData(INode start, INode end)
    {
        from = start;
        to = end;
        
        Coordinate from = start.getCoordinate();
        Coordinate to = end.getCoordinate();
        
        double lowestX, lowestY, highestX, highestY;

        if (from.x < to.x)
        {
            lowestX = from.x;
            highestX = to.x;
        }
        else
        {
            lowestX = to.x;
            highestX = from.x;
        }

        if (from.y < to.y)
        {
            lowestY = from.y;
            highestY = to.y;
        }
        else
        {
            lowestY = to.y;
            highestY = from.y;
        }

        centerX = (highestX - lowestX) / 2 + lowestX;
        centerY = (highestY - lowestY) / 2 + lowestY;
    }

    @Override
    public Coordinate getFrom()
    {
        return from.getCoordinate();
    }

    @Override
    public Coordinate getTo()
    {
        return to.getCoordinate();
    }

    @Override
    public Coordinate getCenter()
    {
        return new Coordinate(centerX, centerY);
    }

    @Override
    public abstract RoadType getType();

    @Override
    public abstract String getRoadName();

    @Override
    public String toString()
    {
        return "Line: from (" + from.getCoordinate().x + "," + 
                from.getCoordinate().y + ") to ("
                + to.getCoordinate().x + "," + to.getCoordinate().y + ")";
    }
    
    @Override
    public abstract double weight();
    
    @Override
    public INode getFromNode()
    {
        return from;
    }
    
    @Override
    public INode getToNode()
    {
        return to;
    }
    
    @Override
    public double getLength()
    {
        return GeometricCalculator.
                calculateDistanceBetweenCoordinates(getFrom(), getTo());
    }
    
    @Override
    public void add(Data d)
    {
        
    }
}
