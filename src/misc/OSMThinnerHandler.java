/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import Model.CoordinateConversion;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import javanet.staxutils.IndentingXMLStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author christian
 */
public class OSMThinnerHandler extends DefaultHandler {

    private Document document;
    private IndentingXMLStreamWriter writer;
    private boolean parsingNode;
    private boolean parsingWay;
    private HashSet<String> nodes;
    private HashMap<String, String> realNodeId;
    private HashSet<String> ways;
    private CoordinateConversion converter;

    public void setNodes(HashSet<String> nodes)
    {
        this.nodes = nodes;
        realNodeId = new HashMap<>();
        Integer count = 0;
        for (String oldID : nodes)
        {
            realNodeId.put(oldID, count.toString());
            count++;
        }
    }
    
    public void setWays(HashSet<String> ways)
    {
        this.ways = ways;
    }
    
    @Override
    public void startDocument() throws SAXException {
        try
        {
            
            XMLOutputFactory writerFactory = XMLOutputFactory.newFactory();
            writer = new IndentingXMLStreamWriter(writerFactory.createXMLStreamWriter(new FileWriter("data/OSMUTM-Address.osm")));
            writer.writeStartDocument("1.0");
            writer.writeStartElement("OpenStreetMaps");
            parsingNode = false;
            parsingWay = false;
            converter = new CoordinateConversion();
        } catch (XMLStreamException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public void endDocument() throws SAXException {
        try
        {
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
            writer.close();
        } catch (XMLStreamException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        try
        {
            if (qName.equalsIgnoreCase("node"))
            {
                if (nodes.contains(attributes.getValue("id")))
                {
                    parsingNode = true;
                    writer.writeStartElement("node");
                    double lon = Double.parseDouble(attributes.getValue("lon"));
                    double lat = Double.parseDouble(attributes.getValue("lat"));
                    String utmFormat = converter.latLon2UTM(lat, lon);
                    String[] array = utmFormat.split("\\s");
                    writer.writeAttribute("id", realNodeId.get(attributes.getValue("id")));
                    writer.writeAttribute("lat", array[3]);
                    writer.writeAttribute("lon", array[2]);
                }
            }
            else if (qName.equalsIgnoreCase("way"))
            {
                if (ways.contains(attributes.getValue("id")))
                {
                    parsingWay = true;
                    writer.writeStartElement("way");
                }
            }
            else if (qName.equalsIgnoreCase("tag"))
            {
                if (parsingWay)
                {
                    if (isWantedWayTag(attributes))
                        writeTag(attributes);
                }
                else if (parsingNode)
                {
                    if (isWantedNodeTag(attributes))
                    {
                        writeTag(attributes);
                    }
                }
            }
            else if (qName.equalsIgnoreCase("nd"))
            {
                if (parsingWay)
                {
                    writer.writeStartElement("nd");
                    writer.writeAttribute("ref", realNodeId.get(attributes.getValue("ref")));
                    writer.writeEndElement();
                }
            }
        }
        catch (XMLStreamException e)
        {
            e.printStackTrace();
            throw new RuntimeException();
        }
//     
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {
        
        try
        {
            if (parsingNode || parsingWay)
                if (qName.equalsIgnoreCase("node") || qName.equalsIgnoreCase("way"))
                {
                    writer.writeEndElement();
                    writer.flush();
                    parsingNode = false;
                    parsingWay = false;
                }
        }
        catch (XMLStreamException e)
        {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    
    public boolean isWantedWayTag(Attributes attributes)
    {
        switch (attributes.getValue("k"))
        {
            case "highway":
                return true;
            case "name":
                return true;
            case "natural":
                if (attributes.getValue("v").equalsIgnoreCase("coastline"))
                    return true;
            default: 
                return false;
        }
    }
    
    public boolean isWantedNodeTag(Attributes attributes)
    {
        System.out.println("checking node tag");
        switch (attributes.getValue("k"))
        {
            case "addr:city":
                return true;
            case "addr:housenumber":
                return true;
            case "addr:postcode":
                return true;
            case "addr:street":
                return true;
            default:
                System.out.println("False");
                return false;
        }
    }
    
    public void writeTag(Attributes attributes)
    {
        try
        {
        writer.writeStartElement("tag");
        writer.writeAttribute("k", attributes.getValue("k"));
        writer.writeAttribute("v", attributes.getValue("v"));
        writer.writeEndElement();
        }
        catch (XMLStreamException e)
        {
            e.printStackTrace();
            throw new RuntimeException();
        }
        
    }
}
