package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * A quadtree is contains either an ArrayList of data or four other underlying
 * quadtrees.
 */
public class QuadTree {

    private final double x, y, length;
    private final QuadTree NW, NE, SE, SW;
    private final List<Data> data;

    /**
     * Constructs a QuadTree object and if necessary splits the data into 4 sub
     * QuadTrees Splits if there are more than 500 Data objects or splits the
     * data in type
     *
     * @param dataList the Data to be contained
     * @param x the x coordinate of the lower left corner of the area covered
     * @param y the y coordinate of the lower left corner of the area covered
     * @param length the length of the square of the area covered
     */
    public QuadTree(List<Data> dataList, double x, double y, double length) {
        this.x = x;
        this.y = y;
        this.length = length;
        if (dataList.size() > 500) {

            List<Data> subListNW = new ArrayList<>(dataList.size()/4);
            List<Data> subListNE = new ArrayList<>(dataList.size()/4);
            List<Data> subListSE = new ArrayList<>(dataList.size()/4);
            List<Data> subListSW = new ArrayList<>(dataList.size()/4);

            for (Data current : dataList) {
                Coordinate center = current.getCenter();
                if (center.x < x + length * 0.5) {
                    if (center.y < y + length * 0.5) {
                        subListSW.add(current);
                    } else {
                        subListNW.add(current);
                    }
                } else {
                    if (center.y > y + length * 0.5) {
                        subListNE.add(current);
                    } else {
                        subListSE.add(current);
                    }
                }
            }
            NW = new QuadTree(subListNW, x, y + length * 0.5, length * 0.5);
            NE = new QuadTree(subListNE, x + length * 0.5, y + length * 0.5, length * 0.5);
            SW = new QuadTree(subListSW, x, y, length * 0.5);
            SE = new QuadTree(subListSE, x + length * 0.5, y, length * 0.5);
            data = null;
        } else {
            data = dataList;

            NW = null;
            NE = null;
            SW = null;
            SE = null;
        }
    }

    /**
     * Returns a list of Data objects which fits within the given area Note that
     * the area is thought to be a square with the lower left corner having the
     * x,y of the given coordinate
     *
     * @param startingPoint coordinates of the lower left corner of the wanted
     * square
     * @param length the length of the square
     * @return a list containing all Data within the square
     */
    public void getAreaData(Coordinate startingPoint, double width, double height, List<Data> list) {

        //if the wanted area is not outside the bounds of this object
        if (isWithinBounds(startingPoint, width, height, false)) {
            if (data == null) {

                SW.getAreaData(startingPoint, width, height, list);
                SE.getAreaData(startingPoint, width, height, list);
                NE.getAreaData(startingPoint, width, height, list);
                NW.getAreaData(startingPoint, width, height, list);

            } else {
                list.addAll(data);
            }
        }
    }

    /**
     * Find the nearest road/line segment and returns it. If this object does
     * not contain any data, the call is passed on to the sub QuadTrees
     *
     * @param coordinates the coordinates of the point from which to find the
     * nearest line segment
     * @return the nearest line segment
     */
    public Data getClosestRoad(Coordinate coordinates, double length) {
        //System.out.println("get closest road");
        Data closestRoad = new EmptyData();
        if (isWithinBounds(coordinates, length, length, true)) {
            if (data == null) {
                Data[] closestRoads = new Data[]{
                    SW.getClosestRoad(coordinates, length),
                    SE.getClosestRoad(coordinates, length),
                    NW.getClosestRoad(coordinates, length),
                    NE.getClosestRoad(coordinates, length)
                };
                double shortestDistance = Double.MAX_VALUE;
//                closestRoad = closestRoads[0];
//                shortestDistance
//                        = GeometricCalculator.calculateDistance(
//                                closestRoads[0],
//                                coordinates);
                for (Data currentRoad : closestRoads) {
                    double currentDistance = GeometricCalculator.calculateDistance(currentRoad, coordinates);
                    if (currentDistance < shortestDistance) {
                        closestRoad = currentRoad;
                        shortestDistance = currentDistance;
                    }
                }
            } else {
                double shortestDistance = Double.MAX_VALUE;
                for (Data current : data) {
                    if (isWithinArea(current, coordinates, length)) {
                        double currentDistance
                                = GeometricCalculator.calculateDistance(
                                        current,
                                        coordinates);
                        if (currentDistance < shortestDistance) {
                            shortestDistance = currentDistance;
                            closestRoad = current;
                        }
                    }
                }
            }
        }
        return closestRoad;
    }

    //error here, this method is excluding areas that should not be excluded!!!
    private boolean isWithinBounds(Coordinate center, double width, double height, boolean isCenter) {
        if (isCenter) {
            if (center.x - width > this.x + this.length) {
                return false;
            }
            if (center.x + width < this.x) {
                return false;
            }
            if (center.y - height > this.y + this.length) {
                return false;
            }
            if (center.y + height < this.y) {
                return false;
            }
        } else {
            if (center.x > this.x + this.length) {
                return false;
            }
            if (center.x + width < this.x) {
                return false;
            }
            if (center.y > this.y + this.length) {
                return false;
            }
            if (center.y + height < this.y) {
                return false;
            }
        }
        return true;
    }
    
    private boolean isWithinArea(Data data, Coordinate center, double length)
    {
        Coordinate lineStart = 
                new Coordinate(center.x - length, center.y - length);
        Coordinate lineEnd = 
                new Coordinate(center.x - length, center.y + length);
        if (GeometricCalculator.calculateDistance(
                lineStart, lineEnd, data.getCenter()) < length*2 ||
                GeometricCalculator.calculateDistance(
                        lineStart, lineEnd, data.getFrom()) < length*2 ||
                GeometricCalculator.calculateDistance(
                        lineStart, lineEnd, data.getTo()) < length*2)
            return true;
        return false;
    }
    
    /**
     * Gets a list of sub quad trees, if they exist. If this quad tree contains
     * only data, returns null. If there is no data and no sub quad trees, returns
     * an empty list.
     * @return list of quad trees, null or empty list.
     */
    public List<QuadTree> getQuadTreesUnlessContainsData()
    {
        if(NW == null && NE == null && SE == null && SW == null) {
            if(data != null)
                return null; //Contains only data, and no other quad trees
        }
        List<QuadTree> quadTrees = new ArrayList<>();
        /*Check which sub-quadtrees exist, containing something, and add them to
        the list to be returned*/
        if(NW != null)
            quadTrees.add(NW);
        if(NE != null)
            quadTrees.add(NE);
        if(SE != null)
            quadTrees.add(SE);
        if(SW != null)
            quadTrees.add(SW);
        return quadTrees;
    }
}
