﻿Danmarkskort
============

Project Part I: Visualisation

make af map of danmark's roads by visualizing each road by itself.

Requirements:

Draw the map
  - Make an application that draws all the road segments in the dataset.
 
Differentiate between roads
  - Draw different segment types with different colours, e.g.
    • Highways: red
    • Main roads: blue
    • Paths: green
    • Other: black
    
Adjust size
  - It must be possible to adjust the size of the map drawing, i.e. 
    when resizing the panel with the mouse or maximizing the window.
    
Zoom in
  - You must be able to focus on a specific area of the map by dragging a rectangle with the mouse. 
  - Zoom means that not all road segments are shown and that pixel coordinates change.
  
Display closest road name
  - Either continuously or when a user hovers over the map, 
    unobtrusively display the name of the road closest to the mouse, e.g., in a status bar.
