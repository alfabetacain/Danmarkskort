package Model;

import Model.OSMEdgeData.OSMEdgeDataBuilder;
import View.LoadBar;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OSMLoader extends DefaultHandler implements IMapData {

    private List<Data> roads;
    private final Map<RoadType, List<Data>> map;
    private HashMap<Integer, NodeWithAddress> nodes;
    private boolean parsingWay;
    private boolean parsingNode;
    private List<Integer> ndList;
    private boolean isHighway;
    private String roadName;
    private RoadType type;
    private int loadProgress;
    private final LoadBar loadBar;
    private final int numberOfHighways;
    
    //node
    private double lat;
    private double lon;
    private int nodeZip;
    private List<String> houseNumber;
    private String nodeWay;
    private int nodeID;

    public OSMLoader() {

        loadBar = LoadBar.getInstanceOf();

        loadProgress = 0;
        numberOfHighways = 525083;
        map = new EnumMap<>(RoadType.class);
        for (RoadType type : RoadType.values())
            map.put(type, new ArrayList<Data>());

        try {
            
//            FileInputStream fileIn = new FileInputStream("data/serialNodes.ser");
//            ObjectInputStream in = new ObjectInputStream(fileIn);
//            NodeWithAddress node;
//            while ((node = (NodeWithAddress) in.readObject()) != null)
//                nodes.put(node.getID(), node);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();

            DefaultHandler handler = this;
            BufferedInputStream in = new BufferedInputStream(new FileInputStream("data/OSMUTM-Final.osm"), 64 * 1024);
            parser.parse(in, handler);

        } catch (SAXException | IOException | ParserConfigurationException e) {

            e.printStackTrace();
            throw new RuntimeException("Failure to read");
        }
    }

    @Override
    public Map<RoadType, List<Data>> getData() {
        return map;
    }

    @Override
    public void startDocument() throws SAXException {
        roads = new ArrayList<>();
        nodes = new HashMap<>();
        parsingWay = false;
        parsingNode = false;
        ndList = new ArrayList<>();
        nodeZip = -1;
        nodeWay = null;
        houseNumber = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("node")) {
            parsingNode = true;
            nodeID = Integer.parseInt(attributes.getValue("id"));
            lat = Double.parseDouble(attributes.getValue("lat"));
            lon = Double.parseDouble(attributes.getValue("lon"));
//            nodes.put(Integer.parseInt(attributes.getValue("id")),
//                    new NodeWithAddress(Integer.parseInt(attributes.getValue("id")),
//                            Double.parseDouble(attributes.getValue("lon")),
//                            Double.parseDouble(attributes.getValue("lat")),
//                            attributes.getValue("addr:street"),
//                            Integer.parseInt(attributes.getValue("addr:housenumber")),
//                            Integer.parseInt(attributes.getValue("addr:postcode"))));
        } else if (qName.equalsIgnoreCase("way")) {
            parsingWay = true;
        } else if (qName.equalsIgnoreCase("nd")) {
            if (parsingWay) {
                ndList.add(Integer.parseInt(attributes.getValue("ref")));
            }
        } else if (qName.equalsIgnoreCase("tag")) {
            if (parsingWay) {
//                if (attributes.getValue("k") != null && attributes.getValue("k").equalsIgnoreCase("highway"))
//                {
//                    isHighway = true;
//                    type = attributes.getValue("v");
//                }
//                if (attributes.getValue("k") != null && attributes.getValue("k").equalsIgnoreCase("name"))
//                    roadName = attributes.getValue("v");
//                if (attributes.getValue("natural") != null)
//                    isHighway = false;
                switch (attributes.getValue("k")) {
                    case "highway":
                        isHighway = true;
                        type = RoadType.valueOf(attributes.getValue("v"));
                        break;
                    case "natural":
                        isHighway = false;
                        break;
                    case "name":
                        roadName = attributes.getValue("v");
                        break;
                }
            }
            else if (parsingNode)
            {
////                if (attributes.getValue("addr:street") != null) nodeWay = attributes.getValue("addr:street");
////                //if (attributes.getValue("addr:postcode") != null && !attributes.getValue("addr:postcode").trim().isEmpty()) nodeZip = Integer.parseInt(attributes.getValue("addr:postcode").replaceAll("\\D", ""));
////                if (attributes.getValue("addr:housenumber") != null) houseNumber.add(attributes.getValue("addr:housenumber"));
//                
                switch (attributes.getValue("k"))
                {
                    case "addr:street": 
                        nodeWay = attributes.getValue("v");
                        break;
                    case "addr:postcode":
                        nodeZip = Integer.parseInt(attributes.getValue("v"));
                        break;
                    case "addr:housenumber":
                        houseNumber.add(attributes.getValue("v"));// = attributes.getValue("v");
                        break;                        
                }
            }
        }
    }

    @Override
    public void endElement(String uri,
            String localName,
            String qName)
            throws SAXException {

        if (qName.equalsIgnoreCase("way") && parsingWay) {
            if (isHighway) {

                loadBar.setValue(loadProgress++ * 100 / numberOfHighways);
                for (int i = 1; i < ndList.size(); i++) {
                    NodeWithAddress previous = nodes.get(ndList.get(i - 1));
                    NodeWithAddress current = nodes.get(ndList.get(i));
                    OSMEdgeDataBuilder builder = new OSMEdgeData.OSMEdgeDataBuilder(
                            previous, current
                    );
                    Data road = builder.roadName(roadName).type(type).build();
                    map.get(road.getType()).add(road);
                }
                isHighway = true;
                roadName = "";
                ndList.clear();
            } else {
                for (int i = 1; i < ndList.size(); i++) {
                    INode previous = nodes.get(ndList.get(i - 1));
                    INode current = nodes.get(ndList.get(i));
                    map.get(RoadType.COAST).add(new CoastData(previous, current));
//                    roads.add(new CoastData(previous,
//                            current));
                }
                ndList.clear();
            }
            parsingWay = false;
        }
        else if (qName.equalsIgnoreCase("node") && parsingNode)
        {
            nodes.put(nodeID, new NodeWithAddress(nodeID, lon, lat,
                    nodeWay, houseNumber, nodeZip));
            parsingNode = false;
            houseNumber.clear();
            nodeID = -1;
            lon = -1;
            lat = -1;
            nodeWay = null;
            nodeZip = -1;
        }
    }

    @Override
    public void endDocument() throws SAXException {
        ndList = null;
        //nodes = null;
        System.gc();
    }
    
    public Collection<NodeWithAddress> getSerialNodes()
    {
        return nodes.values();
    }
}
