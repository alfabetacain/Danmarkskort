

package Model;

public interface INode {
    public int getID();
    public Coordinate getCoordinate();
}
