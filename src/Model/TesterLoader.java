/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class TesterLoader implements IMapData {

    @Override
    public Map<RoadType, List<Data>> getData() {
        List<Data> data = new ArrayList<>();
        //input data set A
        INode from = new Node(1, 2.0, 2.0);
        INode to = new Node(2, 3.0, 3.0);
        data.add(new Edge(from, to, 0, 0, RoadType.REGULARROAD, "Vej1"));
        from = new Node(3, 4.0, 4.0);
        to = new Node(4, 5.0,5.0);
        data.add(new Edge(from, to, 0, 0, RoadType.REGULARROAD, "Vej2"));
        //input data set B
        from = new Node(5, 6.0,6.0);
        to = new Node(6, 7.0, 7.0);
        data.add(new Edge(from, to, 0, 10, RoadType.REGULARROAD, "Vej3"));
        from = new Node(7, 8.0, 8.0);
        data.add(new Edge(to, from, 0, 10, RoadType.REGULARROAD, "Vej4"));
        //input data set C
        from = new Node(8, 9.0, 9.0);
        to = new Node(9, 100.0, 100.0);
        data.add(new Edge(from, to, 0, 10, RoadType.REGULARROAD, "Vej5"));
        from = new Node(10, 1100.0, 1100.0);
        data.add(new Edge(to, from, 0, 10, RoadType.REGULARROAD, "Vej6"));
        Map<RoadType, List<Data>> map = new EnumMap<>(RoadType.class);
        for (RoadType type : RoadType.values())
            map.put(type, new ArrayList<Data>());
        map.put(RoadType.REGULARROAD, data);
        return map;
    }
    
}
