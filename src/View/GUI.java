package View;

import Controller.ArrowKeyDirection;
import Controller.Controller;
import Model.Coordinate;
import Model.Data;
import Model.IModel;
import Model.LoaderType;
import Model.Square;
import Model.SpellChecker;
import java.util.Timer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.TimerTask;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * The class responsible for managing and controlling the GUI of the program,
 * entirely. Controls which frames are active at which times, along with having
 * a reference to the main frame of the program, whilst also managing certain
 * listening tasks.
 */
public class GUI implements IGUI {

    private JFrame frame;
    private JFrame iss;
    private MapView mapView;
    private IModel model;
    private StatusBar statusBar;
    private Controller con;
    private SearchAndNavigationPanel srchAndNavi;
    private boolean isWalking = false;
    private List<Data> route;
    private SearchSuggestionFrame srchSuggFrame;
    private int singleSearchHouseNumber = -1;
    private int navFromHouseNumber = -1;
    private int navToHouseNumber = -1;
    private SpellChecker spellChecker;
    private RoadDescriptionPanel roadDesc;
    private boolean roadDescriptionVisible = false;
    private String searchFrom;          //Last 'successful' search input in from field
    private String searchTo;            //Last 'successful' search input in to field
    private boolean CSIZoom;

    /**
     * Initializes the GUI class by opening an initial selection screen,
     * offering the user the choice between using Krak or OSM data.
     */
    public GUI() {
        iss = new InitialSelectionScreen(new KrakButtonListener(), new OSMButtonListener());
    }

    @Override
    public void setupLoadingScreen() {
        iss.removeAll();
        iss.dispose();
        frame = new JFrame();

        // Add an indeterminate progress bar, as the program loads its data
        JProgressBar loadBar = LoadBar.getInstanceOf();
        loadBar.setValue(0);
        frame.add(loadBar, BorderLayout.CENTER);

        //Initial statusbar
        statusBar = new StatusBar(false);
        statusBar.setText("Indlæser, vent venligst...");
        frame.add(statusBar, BorderLayout.SOUTH);

        Dimension prefDim = new Dimension(250, 78); // 38 if we want to revert back to awesome loading frame
        frame.setPreferredSize(prefDim);
        frame.setSize(prefDim);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Sets up the main GUI-setup of the program, including the usage panels in
     * the top and bottom of the designed GUI.
     *
     * @param model The single model instance to be used by the program from
     * here on out.
     */
    @Override
    public void setupMainGUI(IModel model) {
        frame.dispose();
        frame = new JFrame("Danmarkskort");

        //default settings
        int frameInitDimensionX = 850;
        int frameInitDimensionY = 689;
        frame.setPreferredSize(new Dimension(frameInitDimensionX, frameInitDimensionY));
        frame.setSize(frameInitDimensionX, frameInitDimensionY);
        frame.setLocationRelativeTo(null);
        frame.setBackground(new Color(250, 250, 250));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        frame.setLayout(new BorderLayout());

        this.model = model;
        mapView = new MapView(model);
        mapView.setBorder(BorderFactory.createLineBorder(Color.black));

        frame.add(mapView, BorderLayout.CENTER);

        //Status bar
        statusBar = new StatusBar(true);
        frame.add(statusBar, BorderLayout.SOUTH);

        spellChecker = new SpellChecker(2, model.getAllRoadNames());

        //Search Panel
        srchAndNavi = new SearchAndNavigationPanel(new ToOrSearch_TextFieldListener(), new ByCarNavigationButtonListener(),
                new ByFootNavigationButtonListener());
        //srchAndNavi.addListener(new ToOrSearch_TextFieldListener());
        frame.add(srchAndNavi, BorderLayout.NORTH);

        //Resizability
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                frame.setSize(new Dimension(frame.getWidth(), frame.getHeight()));
                mapView.updateDimension();
                System.out.println("mapView.updateDimension");
                frame.repaint();
            }
        });
    }

    /**
     * Updates fields in this GUI class, and calls MapView's own setFromAddress
     * method.
     *
     * @param c The coordinate of the address location
     * @param s The road's full name, excluding the house number, if any
     * @param number The house number of the road.
     */
    @Override
    public void setFromAddress(Coordinate c, String s, int number) {
        mapView.setFromAddress(c, s, number);
        searchFrom = s;
        srchAndNavi.goToNavigationPanel();
        srchAndNavi.setFromTextField(s);
        navFromHouseNumber = number;
        if (mapView.hasTo() && searchTo != null) {
            showAddressOnMap();
        }
        frame.repaint();
    }

    /**
     * Updates fields in this GUI class, and calls MapView's own setToAddress
     * method.
     *
     * @param c The coordinate of the address location
     * @param s The road's full name, excluding the house number, if any
     * @param number The house number of the road.
     */
    @Override
    public void setToAddress(Coordinate c, String s, int number) {
        mapView.setToAddress(c, s, number);
        searchTo = s;
        srchAndNavi.goToNavigationPanel();
        srchAndNavi.setToTextField(s);
        navToHouseNumber = number;
        if (mapView.hasFrom() && searchFrom != null) {
            showAddressOnMap();
        }
        frame.repaint();
    }

    @Override
    public void toggleCSIZoom() {
        CSIZoom = mapView.toggleCSIZoom();
    }

    @Override
    public boolean usesCSIZoom() {
        return CSIZoom;
    }

    /**
     * A combined listener, listening for all action calls which occur on either
     * the single search text field, the from or to navigation text fields or
     * the "Search" button in the SearchAndNavigationPanel. Contains an
     * if-construction, relating to which page the srchAndNavi-field is
     * currently at (thus knowing if a single search or a navigation search has
     * been called for).
     */
    public class ToOrSearch_TextFieldListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            disableGUI();
            System.out.println("disable");
            if (route != null) {
                route = null;
            }
            if (srchAndNavi.atNavigationPage()) { //Navigation search
                if (srchAndNavi.textInNavigateFrom() && srchAndNavi.textInNavigateTo()) {
                    searchFrom = srchAndNavi.getFromTextField();
                    searchTo = srchAndNavi.getToTextField();
                    //Retrieve house numbers from the inputs - returns -1 if there was no house number input
                    navFromHouseNumber = getHouseNumberFromInput(searchFrom);
                    navToHouseNumber = getHouseNumberFromInput(searchTo);
                    if (navFromHouseNumber != -1) {
                        searchFrom = searchFrom.replace(navFromHouseNumber + "", "");
                    }
                    if (navToHouseNumber != -1) {
                        searchTo = searchTo.replace(navToHouseNumber + "", "");
                    }
                    //Retrieve zip code, if any, from the inputs - returns -1 if no zip code
                    int navFromZip = getZipCodeIfAny(searchFrom);
                    int navToZip = getZipCodeIfAny(searchTo);
                    //replace zip code with city name, and remove zip code from input if city name is already present
                    if (navFromZip != -1) {
                        searchFrom = updateInputString(searchFrom, navFromZip);
                    }
                    if (navToZip != -1) {
                        searchTo = updateInputString(searchTo, navToZip);
                    }
                    //Get suggestions: returns only 1 address if correct address was inputtet
                    System.out.println("searchInputFrom: " + searchFrom + ", searchInputTo: " + searchTo);
                    String[] suggestionsForInputFrom = spellChecker.findClosest(searchFrom);
                    String[] suggestionsForInputTo = spellChecker.findClosest(searchTo);
                    initializeSuggestionWindow(suggestionsForInputFrom, suggestionsForInputTo,
                            new ConfirmSuggestionChoiceButtonListener());
                } else {
                    JOptionPane.showMessageDialog(frame, "Indtast venligst en adresse i både til og fra feltet.");
                }
            } else if (!srchAndNavi.atNavigationPage()) { //Single address search
                if (srchAndNavi.textInSearchTextField()) {
                    searchTo = srchAndNavi.getSearchTextField();
                    //Alter searchInputSingleSearch if it contains numbers
                    singleSearchHouseNumber = getHouseNumberFromInput(searchTo);
                    System.out.println("House number: " + singleSearchHouseNumber);
                    if (singleSearchHouseNumber != -1) {
                        searchTo = searchTo.replace(singleSearchHouseNumber + "", "");
                    }
                    //Zip code
                    int singleSearchZip = getZipCodeIfAny(searchTo);
                    if (singleSearchZip != -1) {
                        searchTo = updateInputString(searchTo, singleSearchZip);
                    }

                    String[] suggestionsForInput = spellChecker.findClosest(searchTo);
                    initializeSuggestionWindow("Mente du:", suggestionsForInput,
                            new ConfirmSuggestionChoiceButtonListener(), SuggestionFrameType.SEARCH);
                } else {
                    JOptionPane.showMessageDialog(frame, "Indtast venligst en adresse i søgefeltet");
                }
            }
            enableGUI();
        }
    }

    /**
     * Updates the input String, so that if it contains the name related to the
     * zip code already, deletes the zip code from the search, and if only the
     * zip code is there along with the road name, adds the city name related to
     * the zip code.
     *
     * @param input The input String.
     * @param zip The zip code found in the input String.
     * @return The updated String.
     */
    private String updateInputString(String input, int zip) {
        System.out.println("nav input: " + input);
        if (input.contains(model.getCityFromZip(zip))) {
            input = input.replace("" + zip, "");
        } else if (!input.contains(model.getCityFromZip(zip))) {
            input = input.replace("" + zip, model.getCityFromZip(zip));
        }
        System.out.println("nav input, post-replacing: " + input);
        return input;
    }

    /**
     * Retrieves the house number from the given String, if such a number
     * exists.
     *
     * @param str the input String
     * @return -1 if no house number was found, otherwise returns the house
     * number
     */
    private int getHouseNumberFromInput(String str) {
        str = str.replaceAll("[^0-9]+", " ");
        String[] strArr = str.trim().split(" ");
        int houseNumber = -1;
        if (strArr[0].equals("")) { //there are no numbers in the input String
            return -1;
        } else {
            for (String s : strArr) {
                System.out.println("Number in strArr: " + s);
            }
            for (int i = 0; i < strArr.length; i++) {
                int number = Integer.parseInt(strArr[i]);
                if (number <= 999) {
                    houseNumber = number;
                }
            }
        }
        return houseNumber;
    }

    /**
     * Retrieves the zip code from the input String if any can be found.
     *
     * @param str the input String
     * @return -1 if no zip code was found, otherwise returns the zip code
     */
    private int getZipCodeIfAny(String str) {
        str = str.replaceAll("[^0-9]+", " ");
        String[] strArr = str.trim().split(" ");
        int zipCode = -1;
        if (strArr[0].equals("")) { //there are no numbers in the input String
            return -1;
        } else {
            for (int i = 0; i < strArr.length; i++) {
                int number = Integer.parseInt(strArr[i]);
                if (number > 999) {
                    if (model.getCityFromZip(number) != null) {
                        zipCode = number;
                    }
                }
            }
        }
        return zipCode;
    }

    /**
     * Initializes a suggestion window for a single search for an address.
     *
     * @param headline The headline to be given to this suggestion frame.
     * @param suggestions The suggestions returned by the spellchecker.
     * @param confirmButtonListener The listener for the suggestion frame's
     * confirm button.
     * @param frameType The type of the frame to be made.
     */
    private void initializeSuggestionWindow(String headline, String[] suggestions, ActionListener confirmButtonListener,
            SuggestionFrameType frameType) {
        if (suggestions.length > 1) {
            srchSuggFrame = new SearchSuggestionFrame(headline, suggestions, confirmButtonListener,
                    new SuggestionFrameHandler(), frameType);
            disableGUI();
        } else if (suggestions.length == 1) {
            showAddressOnMap(suggestions[0], singleSearchHouseNumber);
        } else {
            System.out.println("no suggestions for searched text!");
        }
    }

    /**
     * Initializes a suggestion window for if there are several suggestions for
     * both the navigateFrom and navigateTo textfields, returned by the
     * spellchecker.
     *
     * @param navFromSugg Suggestions for the navigate from field.
     * @param navToSugg Suggestions for the navigate to field.
     * @param confirmButtonListener The listener for the suggestion frame's
     * confirm button.
     */
    private void initializeSuggestionWindow(String[] navFromSugg, String[] navToSugg, ActionListener confirmButtonListener) {
        if (navFromSugg.length > 1 && navToSugg.length > 1) {
            srchSuggFrame = new SearchSuggestionFrame(navFromSugg, navToSugg, confirmButtonListener,
                    new SuggestionFrameHandler(), SuggestionFrameType.NAVIGATEFROMANDTO);
            disableGUI();
        } else if (navFromSugg.length > 1) {
            searchTo = navToSugg[0];
            if (navToHouseNumber != -1) {
                srchAndNavi.setToTextField(navToSugg[0] + " " + navToHouseNumber);
            } else {
                srchAndNavi.setToTextField(navToSugg[0]);
            }
            srchSuggFrame = new SearchSuggestionFrame("Hvor ville du lige rejse fra?", navFromSugg,
                    confirmButtonListener, new SuggestionFrameHandler(), SuggestionFrameType.NAVIGATEFROM);
            disableGUI();
        } else if (navToSugg.length > 1) {
            searchFrom = navFromSugg[0];
            if (navFromHouseNumber != -1) {
                srchAndNavi.setFromTextField(navFromSugg[0] + " " + navFromHouseNumber);
            } else {
                srchAndNavi.setFromTextField(navFromSugg[0]);
            }
            srchSuggFrame = new SearchSuggestionFrame("Hvor mente du, du ville hen til?", navToSugg,
                    confirmButtonListener, new SuggestionFrameHandler(), SuggestionFrameType.NAVIGATETO);
            disableGUI();
        } else if (navToSugg.length == 1 && navFromSugg.length == 1) {
            searchFrom = navFromSugg[0];
            searchTo = navToSugg[0];
            if (navFromHouseNumber != -1) {
                srchAndNavi.setFromTextField(navFromHouseNumber + " " + navFromSugg[0]);
            } else {
                srchAndNavi.setFromTextField(navFromSugg[0]);// NOT DONE WITH THIS - FAILURE. FAILURE EVERYWHERE
            }
            if (navToHouseNumber != -1) {
                srchAndNavi.setToTextField(navToHouseNumber + " " + navToSugg[0]);
            } else {
                srchAndNavi.setToTextField(navToSugg[0]);
            }
            showAddressOnMap();
        }
    }

    private void changeFocusToMainFrame() {
        enableGUI();
        frame.setFocusable(true);
        frame.toFront();
        frame.requestFocus();
    }

    /**
     * Listener for the confirmbutton of a suggestion frame. Acts according to
     * which type of suggestion frame is active.
     */
    public class ConfirmSuggestionChoiceButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            srchSuggFrame.tearDown();
            if (!srchAndNavi.atNavigationPage()) {
                //Searching for single address - confirming suggestion choice
                if (srchSuggFrame.getFrameType() == SuggestionFrameType.SEARCH) {
                    String chosenSuggestion = srchSuggFrame.getSingleSearchSuggestion();
                    if (chosenSuggestion != null) {
                        if (singleSearchHouseNumber != -1) {
                            srchAndNavi.setSearchTextField(chosenSuggestion + " " + singleSearchHouseNumber);
                        } else {
                            srchAndNavi.setSearchTextField(chosenSuggestion);
                        }
                        searchTo = chosenSuggestion;
                        showAddressOnMap(chosenSuggestion, singleSearchHouseNumber);
                    }
                }
            } else if (srchAndNavi.atNavigationPage()) {
                if (srchSuggFrame.getFrameType() == SuggestionFrameType.NAVIGATETO) {
                    //If there's a spelling mistake in the 'navigate to' field
                    String chosenSuggestion = srchSuggFrame.getSuggestionTo();
                    if (chosenSuggestion != null) {
                        if (navToHouseNumber != -1) {
                            srchAndNavi.setToTextField(chosenSuggestion + " " + navToHouseNumber);
                        } else {
                            srchAndNavi.setToTextField(chosenSuggestion);
                        }
                        System.out.println("searchInputFrom: " + searchFrom);
                        searchTo = chosenSuggestion;
                        showAddressOnMap();
                    }
                } else if (srchSuggFrame.getFrameType() == SuggestionFrameType.NAVIGATEFROM) {
                    //If there's a spelling mistake in the 'navigate from' field
                    String chosenSuggestion = srchSuggFrame.getSuggestionFrom();
                    if (chosenSuggestion != null) {
                        if (navFromHouseNumber != -1) {
                            srchAndNavi.setFromTextField(chosenSuggestion + " " + navFromHouseNumber);
                        } else {
                            srchAndNavi.setFromTextField(chosenSuggestion);
                        }
                        searchFrom = chosenSuggestion;
                        showAddressOnMap();
                    }
                } else if (srchSuggFrame.getFrameType() == SuggestionFrameType.NAVIGATEFROMANDTO) {
                    //Searching for route navigation - confirm suggestion choices (multiple)
                    String chosenSuggestion = srchSuggFrame.getSuggestionFrom();
                    String chosenSuggestion2 = srchSuggFrame.getSuggestionTo();
                    if (chosenSuggestion != null && chosenSuggestion2 != null) {
                        if (navFromHouseNumber != -1) {
                            srchAndNavi.setFromTextField(chosenSuggestion + " " + navFromHouseNumber);
                        } else {
                            srchAndNavi.setFromTextField(chosenSuggestion);
                        }
                        if (navToHouseNumber != -1) {
                            srchAndNavi.setToTextField(chosenSuggestion2 + " " + navToHouseNumber);
                        } else {
                            srchAndNavi.setToTextField(chosenSuggestion2);
                        }
                        searchFrom = chosenSuggestion;
                        searchTo = chosenSuggestion2;
                        showAddressOnMap();
                    }
                }
            }
        }
    }

    @Override
    public void setController(Controller con) {
        this.con = con;
    }

    public class KrakButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            setupLoadingScreen();
            Timer delay = new Timer("", false);
            delay.schedule(new TimerTask() {
                @Override
                public void run() {
                    con.chosenData(LoaderType.KRAKLOADER);
                }
            }, 1);
        }
    }

    public class OSMButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            setupLoadingScreen();
            Timer delay = new Timer("", false);
            delay.schedule(new TimerTask() {
                @Override
                public void run() {
                    con.chosenData(LoaderType.OSMLOADER);
                }
            }, 1);
        }
    }

    public class ByCarNavigationButtonListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!srchAndNavi.isCarNavSelected()) {
                isWalking = false;
                srchAndNavi.selectCarNav();
                System.out.println("isWalking: " + isWalking);
                if (roadDescriptionVisible) {
                    updateRouteWithoutZoomEffect();
                }
            }
        }
    }

    public class ByFootNavigationButtonListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (!srchAndNavi.isFootNavSelected()) {
                isWalking = true;
                srchAndNavi.selectFootNav();
                System.out.println("isWalking: " + isWalking);
                if (roadDescriptionVisible) {
                    updateRouteWithoutZoomEffect();
                }
            }
        }
    }

    private void removeRoadDescPanel() {
        if (roadDescriptionVisible) {
            frame.remove(roadDesc);
            frame.revalidate();
            roadDescriptionVisible = false;
        }
    }

    private void updateRouteWithoutZoomEffect() {
        removeRoadDescPanel();

        route = model.getPathBetweenPoints(searchFrom, searchTo, isWalking, navFromHouseNumber, navToHouseNumber);

        roadDesc = new RoadDescriptionPanel(searchFrom, searchTo, route, new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int select = roadDesc.getSelectedItem();
                if (select == 0) {
                    mapView.highlightTurn(route.get(select), true);
                } else if (select == route.size() - 1) {
                    mapView.highlightTurn(route.get(select), false);
                } else {
                    mapView.highlightTurn(route.get(select), route.get(select - 1));
                }
                frame.repaint();
            }
        }, isWalking);
        frame.add(roadDesc, BorderLayout.EAST);
        roadDescriptionVisible = true;
        frame.pack();

        mapView.changeRoute(route);

        frame.repaint();
    }

    @Override
    public void zoomIn() {
        mapView.zoomIn();
        frame.repaint();
    }

    @Override
    public void zoomOut() {
        mapView.zoomOut();
        frame.repaint();
    }

    /**
     * Zooms the Mapview to the square defined by a coordinate (x,y) and a
     * length. The methods also disables the user interface, while the map is 
     * zooming, so to not get any new inputs, during a possible zoom animation.
     * 
     * @param x of the coordinate
     * @param y point of the coordinate
     * @param sideLength the length of the square.
     */
    @Override
    public void zoomToSquare(double x, double y, double sideLength) {
        disableGUI();
        mapView.zoomToSquare(x, y, sideLength);
        enableGUI();
    }

    private class SuggestionFrameHandler extends WindowAdapter {

        @Override
        public void windowLostFocus(WindowEvent we) {
            srchSuggFrame.requestFocus();
        }

        @Override
        public void windowClosing(WindowEvent we) {
            changeFocusToMainFrame();
        }

        @Override
        public void windowClosed(WindowEvent we) {
            changeFocusToMainFrame();
        }
    }

    //Enables the GUI for userinput, typically called some time after disableGUI
    @Override
    public void enableGUI() {
        frame.setEnabled(true);
        frame.requestFocus();
        mapView.setEnabled(true);
        mapView.requestFocusInWindow();
        frame.setEnabled(true);
        frame.repaint();
    }
    
    /**
     * Disables the frames, so that the user isn't able to access the frame.
     * Typically used for when the a user started process is being processed.
     * The GUI is once again enabled by calling enableGUI().
     */
    @Override
    public void disableGUI() {
        frame.setEnabled(false);
    }

    @Override
    public void moveMap(ArrowKeyDirection direction) {
        mapView.moveMap(direction);
        frame.repaint();
    }

    /**
     * this method is continiously called when making a zoom square. It
     * updates the length and coordinate of the square.
     */
    @Override
    public void updateZoomSquare(Square squ) {
        mapView.updateSquare(squ);
        frame.repaint();
    }

    // the text of the status bar is set as String text.
    @Override
    public void setStatusText(String text) {
        statusBar.setText(text);
    }

    @Override
    public void addTheKeyListener(KeyListener l) {
        mapView.addKeyListener(l);
    }

    @Override
    public void addMouseListeners(MouseListener ml, MouseMotionListener mml, MouseWheelListener mwl) {
        mapView.addMouseListener(ml);
        mapView.addMouseMotionListener(mml);
        mapView.addMouseWheelListener(mwl);
    }

    /**
     * used for changing the shown map in mapview, by continuosly calling this
     * method when the mouse is moved. The map is then moved with a vector
     * equal to the new coor - the last.
     */
    @Override
    public void mouseDrag(Coordinate coor) {
        mapView.mouseDrag(coor);
        frame.repaint();
    }
    
    // sets the mapview to show the entire map.
    @Override
    public void resetView() {
        disableGUI();
        mapView.resetView();
        enableGUI();
    }

    //Changes the cursor to 'cursor'.
    @Override
    public void setCursor(Cursor cursor) {
        frame.setCursor(cursor);
        mapView.setCursor(cursor);
    }

    @Override
    public void focusMapView() {
        mapView.requestFocus();
    }

    @Override
    public void start() {
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public Coordinate pointToCoordinate(Point point) {
        return mapView.pointToCoordinate(point);
    }

    private void showAddressOnMap(String s, int number) {
        removeRoadDescPanel();
        Coordinate c = model.getAddressCoordinate(s);
        mapView.clearAddresses();
        mapView.setToAddress(c, s, number);
        mapView.centerOnHighlightedAddress();
    }

    /**
     * Shows the adress on the map, using the latest updated searchFrom and
     * searchTo values.
     */
    private void showAddressOnMap() {
        removeRoadDescPanel();

        route = model.getPathBetweenPoints(searchFrom, searchTo, isWalking, navFromHouseNumber, navToHouseNumber);

        roadDesc = new RoadDescriptionPanel(searchFrom, searchTo, route, new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int select = roadDesc.getSelectedItem();
                if (select == 0) {
                    mapView.highlightTurn(route.get(select), true);
                } else if (select == route.size() - 1) {
                    mapView.highlightTurn(route.get(select), false);
                } else {
                    mapView.highlightTurn(route.get(select), route.get(select - 1));
                }
                frame.repaint();
            }
        }, isWalking);
        frame.add(roadDesc, BorderLayout.EAST);
        roadDescriptionVisible = true;
        frame.pack();

        mapView.changeRoute(route);

        Data fromEdge = model.getEdgeFromAddress(searchFrom);
        Data toEdge = model.getEdgeFromAddress(searchTo);

        mapView.setFromAddress(fromEdge.getCoordFromNumber(navFromHouseNumber), searchFrom, navFromHouseNumber);
        mapView.setToAddress(toEdge.getCoordFromNumber(navToHouseNumber), searchTo, navToHouseNumber);

        mapView.centerOnHighlightedAddress();
        frame.repaint();
    }

    @Override
    public void changeWalkingStatus() {
        isWalking = !isWalking;
        System.out.println("Status: " + isWalking);
    }

    @Override
    public boolean isWalking() {
        return isWalking;
    }

    @Override
    public boolean hasTo() {
        return mapView.hasTo();
    }

    @Override
    public boolean hasFrom() {
        return mapView.hasFrom();
    }

    @Override
    public void clearFromAndTo() {
        mapView.clearAddresses();
        removeRoadDescPanel();
    }
}