package Model;

import java.util.List;

/**
 * Represents the raw data from a line in kdv_unload.tyt.
 */
public final class Edge implements Comparable<Edge>, Data {
//        public final double fromX, fromY, toX, toY;

    public final INode from, to;
    public final double centerX, centerY;
    public final RoadType roadType;
    public final double LENGTH;
    public final int TYP;
    public final String VEJNAVN;
    public final int FROMLEFT;
    public final int TOLEFT;
    public final int FROMRIGHT;
    public final int TORIGHT;
    public final String FROMLEFT_BOGSTAV;
    public final String TOLEFT_BOGSTAV;
    public final String FROMRIGHT_BOGSTAV;
    public final String TORIGHT_BOGSTAV;
    public final int V_POSTNR;
    public final int H_POSTNR;
    public final String RUTENR;
    public final int FRAKOERSEL;
    public final int ZONE;
    public final int SPEED;
    public final double DRIVETIME;
    //Consider making it an ENUM.
    public final String ONE_WAY;
    public final String F_TURN;
    public final String T_TURN;
    public final int VEJNR;
    private final double weight;

    public Edge(String line) {
        try {
            DataLine dl = new DataLine(line);
            //tilsvarende key for the from NODe
            int FNODE = dl.getInt();
            //The key for the to Node
            int TNODE = dl.getInt();

//                fromX = KrakLoader.nodes.get(FNODE).X_COORD;
//                fromY = KrakLoader.nodes.get(FNODE).Y_COORD; 
//                toX = KrakLoader.nodes.get(TNODE).X_COORD;
//                toY = KrakLoader.nodes.get(TNODE).Y_COORD;
            from = KrakLoader.nodes.get(FNODE);
            to = KrakLoader.nodes.get(TNODE);
            if (from.getCoordinate().x > to.getCoordinate().x) {
                centerX = (from.getCoordinate().x - to.getCoordinate().x) * 0.5 + to.getCoordinate().x;
            } else {
                centerX = (to.getCoordinate().x - from.getCoordinate().x) * 0.5 + from.getCoordinate().x;
            }
            if (from.getCoordinate().y > to.getCoordinate().y) {
                centerY = (from.getCoordinate().y - to.getCoordinate().y) * 0.5 + to.getCoordinate().y;
            } else {
                centerY = (to.getCoordinate().y - from.getCoordinate().y) * 0.5 + from.getCoordinate().y;
            }

            /**
             * Lav FNODE og TNODE til koordinater, der bliver til et centerpunkt
             * og en længde. initialiser og tilføj de felter, der ikke bruges
             * endnu, men som skal bruges af openmaps
             */
            LENGTH = dl.getDouble();
            int DAV_DK = dl.getInt();
            int DAV_DK_ID = dl.getInt();
            TYP = dl.getInt();

            switch (TYP) {
                case 1:
                    roadType = RoadType.HIGHWAY; //Highway
                    break;
                case 2:
                    roadType = RoadType.EXPRESSWAY; //Eypressway
                    break;
                case 3:
                    roadType = RoadType.MAINROAD; //Main road (>6m)
                    break;
                case 4:
                    roadType = RoadType.SUBROAD; //Secondary road (>6m)
                    break;
                case 5:
                    roadType = RoadType.REGULARROAD; //Road (3-6m)
                    break;
                case 6:
                    roadType = RoadType.SUBROAD; //"Other road"
                    break;
                case 8:
                    roadType = RoadType.PATH; //Path
                    break;
                case 10:
                    roadType = RoadType.SUBROAD; //Road in between fields
                    break;
                case 11:
                    roadType = RoadType.PATH; //Walking streets
                    break;
                case 21:
                    roadType = RoadType.HIGHWAY; //Projected highway
                    break;
                case 22:
                    roadType = RoadType.EXPRESSWAY; //Proj. eypressway
                    break;
                case 23:
                    roadType = RoadType.REGULARROAD; //Proj. main road
                    break;
                case 24:
                    roadType = RoadType.SUBROAD; //Proj. secondary road
                    break;
                case 25:
                    roadType = RoadType.REGULARROAD; //Proj. medium road(3-6m)
                    break;
                case 26:
                    roadType = RoadType.REGULARROAD; //Proj. small road(<3m)
                    break;
                case 28:
                    roadType = RoadType.PATH; //Proj. path
                    break;
                case 31:
                    roadType = RoadType.HIGHWAY; //Highway eyit
                    break;
                case 32:
                    roadType = RoadType.EXPRESSWAY; //Eypressway eyit
                    break;
                case 33:
                    roadType = RoadType.REGULARROAD; //Main road eyit  
                    break;
                case 34:
                    roadType = RoadType.SUBROAD; //Secondary road eyit
                    break;
                case 35:
                    roadType = RoadType.SUBROAD; //"Other road" eyit
                    break;
                case 41:
                    roadType = RoadType.HIGHWAY; //Highway tunnel
                    break;
                case 42:
                    roadType = RoadType.EXPRESSWAY; //Eypressway tunnel
                    break;
                case 43:
                    roadType = RoadType.REGULARROAD; //Main road tunnel
                    break;
                case 44:
                    roadType = RoadType.SUBROAD; //Secondary road tunnel
                    break;
                case 45:
                    roadType = RoadType.SUBROAD; //"Other road" tunnel
                    break;
                case 46:
                    roadType = RoadType.SUBROAD; //"Smaller tunnel"
                    break;
                case 48:
                    roadType = RoadType.PATH; //Path tunnel
                    break;
                case 80:
                    roadType = RoadType.FERRYROUTE; // Route for ferries
                    break;
                default:
                    roadType = RoadType.REGULARROAD;
            }

            VEJNAVN = dl.getString();
            FROMLEFT = dl.getInt();
            TOLEFT = dl.getInt();
            FROMRIGHT = dl.getInt();
            TORIGHT = dl.getInt();
            FROMLEFT_BOGSTAV = dl.getString();
            TOLEFT_BOGSTAV = dl.getString();
            FROMRIGHT_BOGSTAV = dl.getString();
            TORIGHT_BOGSTAV = dl.getString();
            int V_SOGNENR = dl.getInt();
            int H_SOGNENR = dl.getInt();
            V_POSTNR = dl.getInt();
            H_POSTNR = dl.getInt();
            int KOMMUNENR = dl.getInt();
            int VEJKODE = dl.getInt();
            int SUBNET = dl.getInt();
            RUTENR = dl.getString();
            FRAKOERSEL = dl.getInt();
            ZONE = dl.getInt();
            SPEED = dl.getInt();
            DRIVETIME = dl.getDouble();
            ONE_WAY = dl.getString();
            F_TURN = dl.getString();
            T_TURN = dl.getString();
            VEJNR = dl.getInt();
            String AENDR_DATO = dl.getString();
            int TJEK_ID = dl.getInt();
        } catch (IllegalStateException e) {
            throw new IllegalStateException();

        }

        //might be moved to weight() for performance increase during start-up
        weight = (LENGTH / 1000) / roadType.getSpeedLimit();
    }

    public Edge(INode from, INode to, int lowestHouse, int highestHouse, RoadType type, String name) {
    this.from = from;
    this.to = to;
    Coordinate cFrom = from.getCoordinate();
    Coordinate cTo = to.getCoordinate();
    centerX = cFrom.x > cTo.x ? (cFrom.x - cTo.x) * 0.5 + cTo.x : (cTo.x - cFrom.x) * 0.5 + cFrom.x;
    centerY = cFrom.y > cTo.y ? (cFrom.y - cTo.y) * 0.5 + cTo.y : (cTo.y - cFrom.y) * 0.5 + cFrom.y;
    this.roadType = type;
    LENGTH = GeometricCalculator.calculateDistanceBetweenCoordinates(cFrom, cTo);
    
    TYP = 0;
    VEJNAVN = name;
    FROMLEFT = lowestHouse;
    TOLEFT = 0;
    FROMRIGHT = 0;
    TORIGHT = highestHouse;
    FROMLEFT_BOGSTAV = "A";
    TOLEFT_BOGSTAV = "A";
    FROMRIGHT_BOGSTAV = "A";
    TORIGHT_BOGSTAV = "A";
    V_POSTNR = 2500;
    H_POSTNR = 2500;
    RUTENR = "Bla";
    FRAKOERSEL = -1;
    ZONE = 0;
    SPEED = 0;
    DRIVETIME = 0;
    ONE_WAY = "Si";
    F_TURN = "Who cares?";
    T_TURN = "Really?";
    VEJNR = 0;
    weight  = (LENGTH / 1000) / roadType.getSpeedLimit();
        System.out.println(VEJNAVN + " has weight: " + weight);
}

@Override
        public String toString() {
        return centerX + ","
                + centerY + ","
                + //FNODE + "," +
                //TNODE + "," +
                //String.format("%.5f,", LENGTH) +
                //DAV_DK + "," +
                //DAV_DK_ID + "," +
                //TYP + "," +
                "'" + VEJNAVN + "',"
                + FROMLEFT + ","
                + TOLEFT + ","
                + FROMRIGHT + ","
                + TORIGHT + ","
                + "'" + FROMLEFT_BOGSTAV + "',"
                + "'" + TOLEFT_BOGSTAV + "',"
                + "'" + FROMRIGHT_BOGSTAV + "',"
                + "'" + TORIGHT_BOGSTAV + "',"
                + //V_SOGNENR + "," +
                //H_SOGNENR + "," +
                V_POSTNR + ","
                + H_POSTNR + ","
                + //KOMMUNENR + "," +
                //VEJKODE + "," +
                //SUBNET + "," +
                "'" + RUTENR + "',"
                + FRAKOERSEL + ","
                + ZONE + ","
                + SPEED + ","
                + String.format("%.3f,", DRIVETIME)
                + "'" + ONE_WAY + "',"
                + "'" + F_TURN + "',"
                + "'" + T_TURN + "',"
                + (VEJNR == -1 ? "**********," : VEJNR + ",");
        //"'" + AENDR_DATO + "'," +
        //TJEK_ID;
    }

    @Override
        public int compareTo(Edge another) {
        if (another.centerX > centerX) {
            return 1;
        } else if (another.centerX < centerX) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
        public Coordinate getFrom() {
        return from.getCoordinate();
    }

    @Override
        public Coordinate getTo() {
        return to.getCoordinate();
    }

    @Override
        public Coordinate getCenter() {
        return new Coordinate(centerX, centerY);
    }

    @Override
        public RoadType getType() {
        return roadType;
    }

    @Override
        public String getRoadName() {
        return VEJNAVN;
    }
        @Override
        public double weight()
    {
        return weight;
    }

    @Override
        public INode getFromNode() {
        return from;
    }

    @Override
        public INode getToNode() {
        return to;
    }

    @Override
        public double getLength() {
        return LENGTH;
    }

    @Override
        public double getRotation() {

        return Math.toDegrees(Math.atan2(from.getCoordinate().y - to.getCoordinate().y, from.getCoordinate().y - to.getCoordinate().y));

    }

    @Override
    public int getZipCode() {
        return H_POSTNR;
    }

    @Override
        public Coordinate getCoordFromNumber(int fromNumber) {
        return getCenter();
    }

    @Override
        public boolean hasHouseNumber(int number) {
        if (number >= FROMLEFT && number <= TOLEFT) {
            return true;
        }
        if (number >= FROMRIGHT && number <= TORIGHT) {
            return true;
        }
        return false;
    }
    
    private int lowestHouseNumber() {
        int lowest = FROMLEFT < FROMRIGHT ? FROMLEFT : FROMRIGHT;
        return lowest;
    }

    private int highestHouseNumber() {
        int highest = TOLEFT < TORIGHT ? TOLEFT : TORIGHT;
        return highest;
    }

    @Override
        public INode getAddressAsNode(int houseNumber, int possibleNodeNumber) {
        if (!hasHouseNumber(houseNumber)) {
            throw new IllegalArgumentException("No such house number: " + houseNumber
            + " - it must be between " + lowestHouseNumber() + " and " + highestHouseNumber());
        }
        int lowest = lowestHouseNumber();
        if (lowest == houseNumber) return from;
        int highest = highestHouseNumber();
        if (highest == houseNumber) return to;
        double slope = (getTo().y - getFrom().y) / (getTo().x - getFrom().x);
        double ydif = (to.getCoordinate().y - from.getCoordinate().y) / (highest - lowest) * (houseNumber - lowest);
        Node node = new Node(possibleNodeNumber, 
                getFrom().x + ydif, getFrom().y + slope * ydif);
        return node;
    }
    
    @Override
        public void add(Data d)
    {
        
    }

    @Override
        public List<Data> getEdges() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public boolean isNode(int houseNumber) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
        public int getCoordinateAsHouseNumber(Coordinate coord) {
        int lowest = lowestHouseNumber();
        int highest = highestHouseNumber();
        double slope = (getTo().y - getFrom().y) / (getTo().x - getFrom().x);
        double ydif = (Math.abs(to.getCoordinate().y - from.getCoordinate().y)) / slope;
        double lengthOfHouseNumber = LENGTH / (highest - lowest);
        double closestDistance = Double.MAX_VALUE;
        int housenumber = lowest;
        for (int i = 0; i < (highest-lowest); i++)
        {
            if (Math.abs(lengthOfHouseNumber - ydif) < closestDistance)
            {
                housenumber = i + lowest;
                closestDistance = Math.abs(lengthOfHouseNumber - ydif);
            }
        }
        return housenumber;
    }
}
