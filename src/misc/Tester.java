/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author christian
 */
public class Tester {

    public static void main(String[] args) {
        try
        {
            XMLOutputFactory writerFactory = XMLOutputFactory.newFactory();
            XMLStreamWriter writer = writerFactory.createXMLStreamWriter(new FileWriter("data/testOSM.xml"));
            writer.writeStartDocument();
            writer.writeStartElement("OpenStreetMaps");
            for (int i = 0; i < 10000000; i++)
            {
                writer.writeStartElement("node");
                writer.writeAttribute("attribute1", "value1");
                //writer.flush();
                //writer.writeAttribute("attribute2", "value2");
                writer.writeEndElement();
            writer.flush();
            }

            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
            writer.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
